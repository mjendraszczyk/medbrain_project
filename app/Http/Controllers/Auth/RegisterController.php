<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profil;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Validate;
///use Validate;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App\Ustawienia;

use App\Podmiot;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/konto/dane';
    protected $redirectTo = '/ogloszenie/dodaj/krok2';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        if($request->query('proces') == 'ogloszenie') {
             $this->redirectTo = '/ogloszenie/dodaj/krok2';
        } else {
            $this->redirectTo = '/konto/dane';
        }
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // return "TE";
        //dd($data);
       // $getUser = User::where('email',$data['email_register'])->count();

        
            return Validator::make($data, [
            'email_register' => ['required', 'string', 'email', 'max:255','unique:users,email'],
            'password_register' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        //     if ($getUser == 0) {
                
        //         return $validator;
        //     }
        // } catch(\Exception $e) {
        //     return redirect()->back();
        // }
   

     
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => '',
            'email' => $data['email_register'],
            'password' => Hash::make($data['password_register']),
            'stan' => '0',
            'id_rola' => '1'
        ]);

        Profil::create([
            'o_mnie' => null,
            'id_specjalizacje' => null,
            'id_user' => $user->id,
            'szukam' => 0,
        ]);

        $email = trim($data['email_register']);
        $konto = array(
                'name' =>  $email,
                'email' => $email,
                'stan' => '0'
            );
            
        $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
            $getUstawienia = Ustawienia::where('instancja','mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
                    $message->from(env('MAIL_USERNAME'), $temat);
                    $message->to($email)->subject($temat);
                });
                } catch(\Exception $e) {
                    return false;
                }
            }

        return $user;
    }
}
