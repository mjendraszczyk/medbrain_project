<?php

namespace App\Http\Controllers;

use App\Specjalizacja;
use App\WymiarPracy;
use App\Poziom;
use App\Wynagrodzenie;
use App\Doswiadczenie;
use App\Wojewodztwo;
use App\Miasto;
use App\Kraj;
use App\User;
use App\Podmiot;
use App\Ogloszenie;
use App\RodzajPlacowki;
use App\TypOgloszenia;
use App\RodzajUmowy;
use App\DodatkoweAdresy;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Tytul;
use App\Rola;
use Image;
use App\Ustawienia;
use Illuminate\Http\Response;
use App\Lokalizacja;

use Illuminate\Http\Request;


use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $limit;
    // public  $min_price;
    //     public  $max_price;
    public function __construct() {
 
        Session::put('filter_wynagrodzenie_max',Ogloszenie::select('wynagrodzenie_do')->max('wynagrodzenie_do'));
        
          if (Session::get('filter_wynagrodzenie_do') == null) {
            Session::put('filter_wynagrodzenie_do', Ogloszenie::select('wynagrodzenie_do')->max('wynagrodzenie_do'));
        } else{ 
            Session::put('filter_wynagrodzenie_do', Session::get('filter_wynagrodzenie_do'));
        }

        if (Session::get('filter_wynagrodzenie_od') == null) {
             Session::put('filter_wynagrodzenie_od', Ogloszenie::select('wynagrodzenie_od')->min('wynagrodzenie_od'));
        } else{ 
             Session::put('filter_wynagrodzenie_od', Session::get('filter_wynagrodzenie_od'));
        }

        $this->limit = 25;
    }
    public static function getSpecjalizacje($id) { 
        if ($id == null) {
            $specjalizacje = (new Specjalizacja())->orderBy('nazwa','asc')->get();
            return $specjalizacje;
        } elseif($id == 0) {
            return "wszystkie";
        }  else {
            $specjalizacje = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
            return $specjalizacje->nazwa;
        }
    }
    public static function getSpecjalizacjeColor($id) {
        if ($id == null) {
            $specjalizacje = (new Specjalizacja())->get();
            return $specjalizacje;
        } elseif($id == 0) {
            return "04c785";
        } else {
            $specjalizacje = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
            return $specjalizacje->kolor;
        }
    }
    public static function getSpecjalizacjeSymbol($id) {
        if ($id == null) {
            $specjalizacje = (new Specjalizacja())->get();
            return $specjalizacje;
        } else {
            $specjalizacje = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
            return $specjalizacje->symbol;
        }
    }
        public static function getRole($id) { 
        if ($id == null) {
            $role = (new Rola())->get();
        } else {
            $role = (new Rola())->where('id_rola', $id)->first();
        }
        return $role;
    }
    public static function getSpecjalizacjaName($id) {
        $specjalizacjaName = (new Specjalizacja())->where('id_specjalizacje', $id)->first();
        return $specjalizacjaName;
    }
    public static function getWymiarPracy($id) { 
        if ($id == null) {
            $WymiarPracy = (new WymiarPracy())->get();
        } else {
            $WymiarPracy = WymiarPracy::where('id_wymiar_pracy', $id)->get();
        }
        return $WymiarPracy;
    }
    public static function getDoswiadczenie($id) { 
        if ($id == null) {
            $Doswiadczenie = (new Doswiadczenie())->get();
        } else {
            $Doswiadczenie = Doswiadczenie::where('id_doswiadczenie', $id)->get();
        }
        return $Doswiadczenie;
    }
    public static function getPoziom($id) { 
        if ($id == null) {
        $Poziom = (new Poziom())->get();
        } else {
            $Poziom = Poziom::where('id_poziom', $id)->first();
        }
        return $Poziom;
    }
    public static function getPodmiot($id) { 
        if ($id == null) {
        $Podmiot = (new Podmiot())->get();

        return $Podmiot;
        } else {
            $Podmiot = Podmiot::find($id);
            if($Podmiot == null) {
                return "Brak";
            } else{
                $Podmiot = Podmiot::where('id_podmiot', $id)->first();
                
                return $Podmiot->nazwa;
            }
        }
    }
    public static function getPodmiotMail($id) {
            $Podmiot = Podmiot::find($id);
            if($Podmiot == null) {
                return "Brak";
            } else{
                $Podmiot = Podmiot::where('id_podmiot', $id)->first();
                
                return $Podmiot->podmiot_email;
            }
    }
    public static function getPodmiotPhone($id) {
        $Podmiot = Podmiot::find($id);
            if($Podmiot == null) {
                return "Brak";
            } else{
                $Podmiot = Podmiot::where('id_podmiot', $id)->first();
                
                return $Podmiot->podmiot_telefon;
            }
    }

     public static function getPodmiotAddress($id) {
        $Podmiot = Podmiot::find($id);
            if($Podmiot == null) {
                return "Brak";
            } else{
                $Podmiot = Podmiot::where('id_podmiot', $id)->first();
                
                return $Podmiot->ulica.', '.Controller::getMiasto($Podmiot->id_miasta).' ('.$Podmiot->lat.', '.$Podmiot->lon.')';
            }
    }

    public static function getWynagrodzenie($id) { 
        if($id == null) {
            $Wynagrodzenie = (new Wynagrodzenie())->get();
        } else {
            $Wynagrodzenie = (new Wynagrodzenie())->where('id_wynagrodzenie', $id)->get();
        }
        
        return $Wynagrodzenie;
    }
    public static function getOgloszenie($id_ogloszenia) {
        return Ogloszenie::where('id_ogloszenia',$id_ogloszenia)->first();
         
    }
    public static function parseMoneyFormat($money, $id_ogloszenia) {
        
        if ($id_ogloszenia != '') {
            $getOgloszenie = Ogloszenie::where('id_ogloszenia', $id_ogloszenia)->first();

                if ($getOgloszenie->waluta_wynagrodzenia != '') {
                    return number_format($money, 2, '.', ' ').' '.$getOgloszenie->waluta_wynagrodzenia;
                } else {
                    return number_format($money, 2, '.', ' ').' PLN';
                }
            } else {
                return number_format($money, 2, '.', ' ');
            }
    }
    public static function getAsyncMiastaByWojewodztwo($id) { 
        $getMiasta = Miasto::where('id_wojewodztwa', $id)->get();

        return $getMiasta;
    }
    public static function getMiastaByWojewodztwo($id) { 
        $getMiasta = Miasto::where('id_wojewodztwa', $id)->get();

        return $getMiasta;
    }
    public static function getRodzajPlacowki($id) {
        if($id == null){
            $getRodzajPlacowki = RodzajPlacowki::get();
        } else {
            $getRodzajPlacowki = RodzajPlacowki::where('id_rodzaj_placowki', $id)->first();
        }
        return $getRodzajPlacowki;
    }
    public static function getTypOgloszenia($id) {
        if ($id == null) {
            $getRodzajOgloszenia = (new TypOgloszenia())->get();
        } else {
            $getRodzajOgloszenia = TypOgloszenia::where('id_typ_ogloszenia', $id)->get();
        }
        return $getRodzajOgloszenia;
    }
    public static function getRodzajUmowy($id) {
        if ($id == null) {
            $getRodzajUmowy = (new RodzajUmowy())->get();
        } else {
            $getRodzajUmowy = RodzajUmowy::where('id_rodzaj_umowy', $id)->get();
        }
        return $getRodzajUmowy;
    }
    public static function getWojewodztwoIdByMiasto($id) {
        
        if($id == null) { 
            $getWojewodztwo = Wojewodztwo::first();
            return $getWojewodztwo->id_wojewodztwa;
        } else {
            $getMiasto = Miasto::where('id_miasta', $id)->first();
            $getWojewodztwo = Wojewodztwo::find($getMiasto->id_wojewodztwa);
            if ($getWojewodztwo == null) {
                return "Brak";
            } else {
                $getWojewodztwo = Wojewodztwo::where('id_wojewodztwa', $getMiasto->id_wojewodztwa)->first();
                return $getWojewodztwo->id_wojewodztwa;
            }
        }
    }
    public static function getWojewodztwoByMiasto($id) {
        
        if($id == null) { 
            $getWojewodztwo = Wojewodztwo::get();
            return $getWojewodztwo;
        } else {
            $getMiasto = Miasto::where('id_miasta', $id)->first();
            $getWojewodztwo = Wojewodztwo::find($getMiasto->id_wojewodztwa);
            if ($getWojewodztwo == null) {
                return "Brak";
            } else {
                $getWojewodztwo = Wojewodztwo::where('id_wojewodztwa', $getMiasto->id_wojewodztwa)->first();
                return $getWojewodztwo->nazwa;
            }
        }
    }
    public static function getMiasto($id) {
        if($id == null) { 
            $getMiasto = Miasto::get();
            return "Brak";
        } else {
            $getMiasto = Miasto::find($id);
            if ($getMiasto == null) {
                return "Brak";
            } else {
                $getMiasto = Miasto::where('id_miasta', $id)->first();
                return $getMiasto->nazwa;
            }
        }
    }
    public static function getUzytkownik($id) { 
        if ($id == null) {
            $getUzytkownik = User::get();
        } else{
            $getUzytkownik = User::where('id', $id)->first();
        }
        return $getUzytkownik;
    }
    public static function getUzytkownikName($id) { 
    if ($id == null) {
        return "Brak";
    } else {
        $getUzytkownik = User::find($id);
        if($getUzytkownik == null) {
            return "Brak";
        } else {
            $getUzytkownik = User::where('id', $id)->first();
            return $getUzytkownik->name;
        }
    }
    }
    public static function getTytuly($id) {
        if ($id == null) {
            $getTytuly = (new Tytul())->get();
        } else {
            $getTytuly = Tytul::where('id_tytuly', $id)->first();
        }
        return $getTytuly;
    }
    public static function getKraj($id) {
        if($id == null) {
            $getKraj = Kraj::get();
        } else {
            $getKraj = Kraj::where('id_kraje', $id)->first();
        }
        return $getKraj;
    }
    public static function getWojewodztwo($id) {
        if(($id == null)) {
            $getWojewodztwo = Wojewodztwo::get();
            return $getWojewodztwo;
        } else if($id == '0') {
            return 'wszystkie';
        } else {
            $getWojewodztwo = Wojewodztwo::where('id_wojewodztwa', $id)->first();
            return $getWojewodztwo->nazwa;
        }
        
    }

    public static function checkLokalizacjaFromOgloszenie($id_ogloszenia) {

          $getOgloszenie = Ogloszenie::where('id_ogloszenia',$id_ogloszenia)->first();
        //Sprawdz lokalizacje 
        if ($getOgloszenie->id_lokalizacja != '') {
            // Wybierz dane z lokalizacji do widoku
            
            $getLokalizacja = Lokalizacja::where('id_lokalizacja', $getOgloszenie->id_lokalizacja)->first();
        } else {
            $getLokalizacja = null;
        }
        return $getLokalizacja;
    }
    public static function getKrajByIdMiasto($id) {
        $getMiasto = Miasto::find($id);

        if($getMiasto == null) {
            return "Brak";
        } else {
            $getMiasto->first();
            $getWojewodztwo = Wojewodztwo::findOrFail($getMiasto->id_wojewodztwa);
            if ($getWojewodztwo == null) {
                return  'Brak';
            } else {
                $getKraj = Kraj::find($getWojewodztwo->id_kraje);
 
                if ($getKraj == null) {
                    return  'Brak';
                } else {
                    $getKraj->first();
                    return $getKraj->nazwa;
                }
            }
        }
    }

    public static function getUstawienia() {
        $ustawienia = Ustawienia::pluck('wartosc', 'instancja');

        return $ustawienia;
    }

    public function filterCore($request, $object) {
        $primaryKey = ($object)->getKeyName();
        $table = ($object)->getTable();
         
            $columns = ($object)->getFillable();
            // dd($request);
        
            $items = $object::where($primaryKey, '>', 0);
            foreach ($columns as $column) {
                if (($request->get($column) != '') || (Session::get($table.'_'.$column) != '')) {
                    // echo Session::get($table.'_'.$column);
                    // echo $table.'_'.$column;
                    // dd($request);

                    // exit();
                    if (Session::get($table.'_'.$column) == '') {
                        // echo "Test";
                        // exit();
                        Session::put($table.'_'.$column, $request->get($column));
                        $items->where($column, 'LIKE', '%'.$request->get($column).'%');
                    } else{
                        if($request->get($column) != '') {
                            Session::put($table.'_'.$column, $request->get($column));
                        }
                        $items->where($column, 'LIKE', '%'.Session::get($table.'_'.$column).'%');
                    }
                }
            }
            if ($table == 'users') {
                    $table = 'uzytkownicy';
            }

            if ($request->has('reset_filter')) {
                foreach ($columns as $column) {
                    Session::put($table.'_'.$column, '');
                }
                return redirect()->route('backend_'.$table.'_index');
            } else {
                return view('backend.'.$table.'.index')->with('items', $items->paginate($this->limit));
            }
    }

     public function upload($input, $extensions, $path, $tytul, $request, $sizeX, $sizeY)
    {
        if ($extensions == 'jpg,jpeg,png') {
            $this->validate($request, [
            $input => 'image|mimes:'.$extensions.'|max:2048',
        ]);
        } else {
            $this->validate($request, [
            $input => 'file|mimes:'.$extensions.'|max:2048',
            ]);
        }


        $file = $request->file($input);
        // dd($file);
        $this->FileName = md5(str_slug($tytul.time())).'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path($path);

        if ($extensions == 'jpg,jpeg,png') {
            $img = Image::make($file->path());
            $img->resize($sizeX, $sizeY, function ($constraint) {//200,200
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$this->FileName);
        } else {
            $filePath = $destinationPath.'/'.$this->FileName;
            $file->move($destinationPath, $this->FileName);
        }
        // $imagePath = $destinationPath.'/'.$this->FileName;
        // $image->move($destinationPath, $this->FileName);
    }

    public function getFileName()
    {
        return $this->FileName;
    }
    public static function getOgloszeniaDependsPodmiot($id_podmiot)  {
        
        $getSpecjalizacja = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->where('ogloszenia.id_podmiot',$id_podmiot)->get('specjalizacje.nazwa as specjalizacja_nazwa');

        // $getOgloszeniaDependsPodmiot = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_podmiot',$id_podmiot)->get('specjalizacje.nazwa as specjalizacja_nazwa','*.*');
         
        //->leftJoin('miasta','podmiot.id_miasta','miasta.id_miasta')
         $getOgloszeniaDependsPodmiot = (new Ogloszenie())->leftJoin('poziom','ogloszenia.id_poziom','poziom.id_poziom')->leftJoin('lokalizacja','ogloszenia.id_lokalizacja','lokalizacja.id_lokalizacja')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_podmiot',$id_podmiot)->get(['*','specjalizacje.nazwa as specjalizacja_nazwa','poziom.nazwa as poziom_nazwa', 'lokalizacja.ulica as lokalizacja_ulica', 'lokalizacja.alias as lokalizacja_alias']);
       
         return $getOgloszeniaDependsPodmiot;
    }

    public static function getOgloszeniaDependsMiasto($id_miasta)  {
         $getOgloszeniaDependsMiasto = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('podmiot.id_miasta',$id_miasta)->get();

         return $getOgloszeniaDependsMiasto;
    }

     public static function getOgloszeniaDependsSpecjalizacja($id_specjalizacje, $id_podmiot)  {
         $getOgloszeniaDependsSpecjalizacja = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_podmiot',$id_podmiot)->where('ogloszenia.id_specjalizacje',$id_specjalizacje)->get();

         return $getOgloszeniaDependsSpecjalizacja;
    }
    public static function getOsobyDependsMiasto($id_miasto) {
        $getOsobyDependsMiasto = (new User())->join('profil','profil.id_user', '=', 'users.id')->leftJoin('specjalizacje','specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->where('profil.id_miasta',$id_miasto)->get();

         return $getOsobyDependsMiasto;
    }
    public static function getOsobyDependsSpecjalizacja($id_specjalizacje) {
        $getOsobyDependsSpecjalizacja = (new User())->join('profil','profil.id_user', '=', 'users.id')->leftJoin('specjalizacje','specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->where('profil.id_specjalizacje',$id_specjalizacje)->get();

         return $getOsobyDependsSpecjalizacja;
    }
    // public static function getOsobyDependsS
    public function getOgloszeniaFilter(Request $request) {
         
            $wynagrodzenie_od = $request->input('wynagrodzenie_od');
            $wynagrodzenie_do = $request->input('wynagrodzenie_do');
            
            
            Session::put('filter_wynagrodzenie_od', $wynagrodzenie_od);
            Session::put('filter_wynagrodzenie_do', $wynagrodzenie_do);

            $request->session()->put('filter_wynagrodzenie_od', $wynagrodzenie_od);
            $request->session()->put('filter_wynagrodzenie_do', $wynagrodzenie_do);

            
         return redirect()->route('ogloszenia_wynagrodzenie_index',['wynagrodzenie_od'=>$wynagrodzenie_od, 'wynagrodzenie_do'=>$wynagrodzenie_do]);
    }
    public static function getHomeSpecjalizacje() {
        return (new Specjalizacja())->orderBy('nazwa','asc')->get();
    }

    public static function getWalutaWynagrodzenia($id_waluta_wynagrodzenia) {
         $waluta_wynagrodzenia = array(
                array(
                    'id_waluta_wynagrodzenia' => 'PLN',
                    'nazwa' => 'PLN',
                ),
                array(
                    'id_waluta_wynagrodzenia' => 'EUR',
                    'nazwa' => "EUR",
                ),
                array(
                    'id_waluta_wynagrodzenia' => 'GBP',
                    'nazwa' => "GBP",
                ),
                array(
                    'id_waluta_wynagrodzenia' => 'USD',
                    'nazwa' => "USD",
                ),
                array(
                    'id_waluta_wynagrodzenia' => 'CHF',
                    'nazwa' => "CHF",
                )
            );

        if($id_waluta_wynagrodzenia == null) {
            return  json_decode (json_encode ($waluta_wynagrodzenia), FALSE);
        }
        else {
            foreach ($id_waluta_wynagrodzenia as $waluta) {
                if($waluta['id_waluta_wynagrodzenia'] == $id_waluta_wynagrodzenia) {
                    return $waluta['nazwa'];
                }
            }
        }
    }
    public static function getTypWynagrodzenia($id_typ_wynagrodzenia) {
        
        $typ_wynagrodzenia = array(
                array(
                    'id_typ_wynagrodzenia' => '1',
                    'nazwa' => 'za miesiąc',
                ),
                array(
                    'id_typ_wynagrodzenia' => '2',
                    'nazwa' => 'roboczogodzina',
                )
            );

        if($id_typ_wynagrodzenia == null) {
            return  json_decode (json_encode ($typ_wynagrodzenia), FALSE);
        }
        else {
            foreach ($typ_wynagrodzenia as $typ) {
                if($typ['id_typ_wynagrodzenia'] == $id_typ_wynagrodzenia) {
                    return $typ['nazwa'];
                }
            }
        }
    }
    public static function checkDodatkoweAdresy($id_ogloszenia) {
        return DodatkoweAdresy::where('id_ogloszenia', $id_ogloszenia)->count();
    }
    public static function getDodatkoweAdresy($id_ogloszenia) {
        return DodatkoweAdresy::where('id_ogloszenia', $id_ogloszenia)->get();
    }
    public function getApiOgloszenie($id_ogloszenia)
    {
        return (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('id_ogloszenia',$id_ogloszenia)->orderBy('id_ogloszenia','desc')->first();
        //exit();
    }
    public function resetUser($id_user) { 
        $getUser = User::where('id',$id_user)->first();
        $getUser->update([
            'password' => bcrypt('1')
        ]);

    }
}
