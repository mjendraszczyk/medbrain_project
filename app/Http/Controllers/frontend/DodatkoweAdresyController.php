<?php

namespace App\Http\Controllers\frontend;

use App\DodatkoweAdresy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\frontend\DodatkoweAdresyRequest;

class DodatkoweAdresyController extends Controller
{
    public function store_dodatkowe_lokalizacje($id_ogloszenia, DodatkoweAdresyRequest $request) {
        //$getDodatkoweAdresy = DodatkoweAdresy::where('')->get();
        $nowyAdres = new DodatkoweAdresy();
        $nowyAdres->nazwa = $request->input('ulica');
        $nowyAdres->lat_dodatkowy_adres = $request->input('lat');
        $nowyAdres->lon_dodatkowy_adres = $request->input('lon');
        $nowyAdres->id_ogloszenia = $id_ogloszenia;
        $nowyAdres->save();
        return redirect()->back();
    } 

       public function delete_dodatkowe_lokalizacje($id_dodatkowy_adres) {
        $dodatkowyAdres = DodatkoweAdresy::where('id_dodatkowe_adresy',$id_dodatkowy_adres)->delete();
        return redirect()->back();
    } 
}
