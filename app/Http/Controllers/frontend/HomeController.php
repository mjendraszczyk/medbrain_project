<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Wojewodztwo;
use App\Specjalizacja;
use App\Ogloszenie;
use App\Podmiot;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index() {
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOglosznia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->orderBy('ogloszenia.id_ogloszenia', 'DESC')->paginate(20);//->orderByRaw('RAND()')
        $countOgloszenia = Ogloszenie::count();
        return view('frontend.homepage.index')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOglosznia)->with('countOgloszenia',$countOgloszenia);
    }
    public function findFirmaPost(Request $request) {
        return redirect()->route('frontend_get_find_firma',['nazwa'=>$request->input('firma')]);
    }
    public function findFirmaGet($firma) {

        $getSpecjalizacje = Controller::getHomeSpecjalizacje();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOglosznia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('podmiot.nazwa',$firma)->groupBy('podmiot.id_podmiot')->orderBy('podmiot.nazwa','desc')->paginate(15);//

        //$firma = Podmiot::where('nazwa',$firma)->get();
        // dd($firma);
        return view('frontend.search.index')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOglosznia);
    }
}
