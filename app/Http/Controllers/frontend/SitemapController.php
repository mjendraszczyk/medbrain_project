<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Blog;
use App\Cms;
use App\Miasto;
use App\Ogloszenie;
use App\Wojewodztwo;
use App\Specjalizacja;

class SitemapController extends Controller
{
    public function index() {
        $routes_array = [route('home'),route('kontakt'),route('faq'),route('polityka_prywatnosci'),route('regulamin'),route('odstapienie_od_umowy'), route('login'), route('onas'), route('konto'), route('blog_index')];

        $getBlog = Blog::get();

        $getCms = Cms::get();

        $getOgloszenia = Ogloszenie::join('podmiot', 'podmiot.id_podmiot', 'ogloszenia.id_podmiot')->get();

        $getSpecjalizacje = Specjalizacja::get();

        $getWojewodztwa = Wojewodztwo::get();
        $getMiasta = Miasto::get();

        return response()->view('frontend.sitemap.index',compact('getBlog','routes_array', 'getCms','getMiasta', 'getOgloszenia', 'getWojewodztwa', 'getSpecjalizacje'))->header('Content-Type', 'text/xml');
    }
}
