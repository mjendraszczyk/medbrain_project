<?php

namespace App\Http\Controllers\frontend;

use App\Miasto;
use App\Poziom;
use App\User;
use App\Specjalizacja;
use App\Doswiadczenie;
use App\Wynagrodzenie;
use App\WymiarPracy;
use App\Wojewodztwo;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\Ogloszenie;
use App\TypOgloszenia;
use App\Podmiot;
use App\Profil;
use App\Aplikacja;
use App\Lokalizacja;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

use Image;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Response;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\frontend\AplikacjeRequest;

use App\Http\Controllers\Controller;
use App\Http\Requests\frontend\OgloszeniaKrok1Request;
use App\Http\Requests\frontend\OgloszeniaKrok2Request;
use App\Http\Requests\frontend\OgloszeniaKrok3Request;
use App\Http\Requests\frontend\OgloszeniaKrok4Request;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;
use App\Ustawienia;

class OgloszeniaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.ogloszenia.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function krok1()
    {
        if(!Auth::check()) {
            return view('frontend.ogloszenia.krok1');
        } else {
            return redirect()->route('oglosznia_dodaj_krok2');
        }
    }
    public function krok1Request(OgloszeniaKrok1Request $request) { 

        $getRequestKrok1 = $request->input('specjalizacja');
         
        $request->session()->put('form_specjalizacja', $getRequestKrok1);
        return redirect()->route('oglosznia_dodaj_krok2');
    }
    public function krok2()
    {
        return view('frontend.ogloszenia.krok2');
    }

       public function krok2Request(OgloszeniaKrok2Request $request) { 
        // Specjalizacja + Rodzaj umowy + Wymiar pracy + Wynagrodzenie od + Wynagrodzenie do + Typ wynagrodzenia + Waluta
        $getRequestKrok2Specjalizacja = $request->input('specjalizacja');
        $getRequestKrok2RodzajUmowy = $request->input('typ_umowy');
        $getRequestKrok2WymiarPracy = $request->input('wymiar_pracy');
        $getRequestKrok2WynagrodzenieOd = $request->input('wynagrodzenie_od');
        $getRequestKrok2WynagrodzenieDo = $request->input('wynagrodzenie_do');
        $getRequestKrok2TypWynagrodzenia = $request->input('typ_wynagrodzenia');
        $getRequestKrok2WalutaWynagrodzenia = $request->input('waluta_wynagrodzenia');

        /**
         * Ukryte pola
         */
        $getRequestKrok2DoswiadczenieOd = $request->input('doswiadczenie_od');
        $getRequestKrok2DoswiadczenieDo = $request->input('doswiadczenie_do');
   
        /**
         * Przeniesione do kroku 3
         */
        // $getRequestKrok2Poziom = $request->input('poziom');
        // $getRequestKrok2Tresc = $request->input('tresc');
        // $getRequestKrok2Szukam = $request->input('szukam');
         
        $request->session()->put('form_specjalizacja', $getRequestKrok2Specjalizacja);
        $request->session()->put('form_wymiar_pracy', $getRequestKrok2WymiarPracy);
        $request->session()->put('form_typ_umowy', $getRequestKrok2RodzajUmowy);
        $request->session()->put('form_wynagrodzenie_od', $getRequestKrok2WynagrodzenieOd);
        $request->session()->put('form_wynagrodzenie_do', $getRequestKrok2WynagrodzenieDo);
        $request->session()->put('form_typ_wynagrodzenia', $getRequestKrok2TypWynagrodzenia);
        $request->session()->put('form_waluta_wynagrodzenia', $getRequestKrok2WalutaWynagrodzenia);

        
        $request->session()->put('form_doswiadczenie_od', $getRequestKrok2DoswiadczenieOd);
        $request->session()->put('form_doswiadczenie_do', $getRequestKrok2DoswiadczenieDo);
        // $request->session()->put('form_poziom', $getRequestKrok2Poziom);
        // $request->session()->put('form_tresc', $getRequestKrok2Tresc);
        // $request->session()->put('form_szukam', $getRequestKrok2Szukam);

        return redirect()->route('oglosznia_dodaj_krok3');
    }
    public function krok3() {
        return view('frontend.ogloszenia.krok3');
    }

     public function krok3Request(OgloszeniaKrok3Request $request) {
        // Poziom + opis
        $getRequestKrok3Tresc = $request->input('tresc');
        $getRequestKrok3Poziom = $request->input('poziom');
        $getRequestKrok3Rekruter = $request->input('rekruter');
        
        $request->session()->put('form_tresc', $getRequestKrok3Tresc);
        $request->session()->put('form_poziom', $getRequestKrok3Poziom);
        $request->session()->put('form_rekruter', $getRequestKrok3Rekruter);
        return redirect()->route('ogloszenia_dodaj_krok4');
    }
    public function krok4()
    {
        // Sprawdzamy czy user jest zalogowany czy nie
        /**
         * Dla niezalogowanego wyswietl formularz dodania danych firmy i lokalizacji,
         * dla zalogowanego wyswietl lokalizacje do wyboru
         */

        // Dla niezalogowanego standardowo tworzymy ogloszenie
        if (!Auth::check() == true) {
            $getWojewodztwo = Wojewodztwo::get();
            $getRodzajPlacowki = RodzajPlacowki::get();

            return view('frontend.ogloszenia.krok4')->with('wojewodztwa', $getWojewodztwo)->with('rodzaje_placowki', $getRodzajPlacowki);
        } else {
            $user = User::where('id',Auth::user()->id)->first();
            // echo "GG";
            // exit();
            //Gdy user jest zalogowany oraz ma dodany conajmnie jeden adres listujemy mu adresy
            $getLokalizacje = Lokalizacja::where('id_user',Auth::user()->id)->count();

            if($getLokalizacje > 0) {
                $lokalizacje = Lokalizacja::where('id_user',Auth::user()->id)->get();
                $getWojewodztwo = Wojewodztwo::get();
                    $getRodzajPlacowki = RodzajPlacowki::get();

                    return view('frontend.ogloszenia.krok4b')->with('wojewodztwa', $getWojewodztwo)->with('rodzaje_placowki', $getRodzajPlacowki)->with('lokalizacje',$lokalizacje);
            } else{
                    $getWojewodztwo = Wojewodztwo::get();
                    $getRodzajPlacowki = RodzajPlacowki::get();

                    return view('frontend.ogloszenia.krok4')->with('wojewodztwa', $getWojewodztwo)->with('rodzaje_placowki', $getRodzajPlacowki);
            }
            // if (($user->id_podmiot != '')) {
                
            //     $getUser = (new User())->leftJoin('profil','profil.id_user','=','users.id')->leftJoin('podmiot','podmiot.id_podmiot','=','users.id_podmiot')->where('users.id',Auth::user()->id)->first();

            //     if($getUser->id_miasta != '') {
            //         Session::put('form_imie_nazwisko', $getUser->name);
            //         Session::put('form_podmiot', $getUser->nazwa);
            //         Session::put('form_adres_email', $getUser->email);
            //         Session::put('form_miasto', $getUser->id_miasta);
            //         Session::put('form_telefon', $getUser->podmiot_telefon);
            //         Session::put('form_ulica', $getUser->ulica);
            //         Session::put('form_rodzaj_placowki', $getUser->id_rodzaj_placowki);
            //         Session::put('form_lat', $getUser->lat);
            //         Session::put('form_lon', $getUser->lon);
            //         Session::put('form_nip', $getUser->nip);

                

            //         return redirect()->route('ogloszenia_dodaj_krok5');
            //     }
            // } 
           
            //     Session::put('form_id_user', Auth::user()->id);
            //     $getWojewodztwo = Wojewodztwo::get();
            //     $getRodzajPlacowki = RodzajPlacowki::get();
            //     return view('frontend.ogloszenia.krok3')->with('wojewodztwa', $getWojewodztwo)->with('rodzaje_placowki', $getRodzajPlacowki);
            
        }
    }

       public function krok4Request(OgloszeniaKrok4Request $request) { 

        $getRequestKrok3ImieNazwisko = $request->input('imie_nazwisko');
        //$getRequestKrok3Email = $request->input('adres_email');

        $getRequestKrok3Wojewodztwo = $request->input('id_wojewodztwa');

        $getRequestKrok3Miasto = $request->input('miasto');
        $getRequestKrok3RodzajPlacowki = $request->input('id_rodzaj_placowki');
        $getRequestKrok3Ulica = $request->input('ulica');
        $getRequestKrok3Telefon = $request->input('telefon');
        $getRequestKrok3Podmiot = $request->input('podmiot');

        $getRequestKrok3Lat = $request->input('lat');
        $getRequestKrok3Lon = $request->input('lon');
        $getRequestKrok3Nip = $request->input('nip');
        
         
         $uzytkownicy = User::where('id', Auth::user()->id);
         $getUzytkownicy = $uzytkownicy->first();
         $podmiot = Podmiot::where('id_podmiot', $getUzytkownicy->id_podmiot);
         
        //  print_r((Input::file('avatar_upload')));

          if ((Input::file('avatar_upload'))) {
              $upload = new Controller();
            $upload->upload('avatar_upload', 'jpg,jpeg,png', '/img/avatar/', $request->get('avatar_upload'), $request, 200, 200);
            $uzytkownicy->update([
                'avatar_upload' => $upload->getFileName()
            ]);

              $upload->upload('avatar_upload', 'jpg,jpeg,png', '/img/logo/', $request->get('avatar_upload'), $request, 200, 200);
            $podmiot->update([
                'logo_upload' => $upload->getFileName()
            ]);
        }
        
        // echo "end";
        // exit();

        $request->session()->put('form_imie_nazwisko', $getRequestKrok3ImieNazwisko);
        $request->session()->put('form_podmiot', $getRequestKrok3Podmiot);
        $request->session()->put('form_wojewodztwo', $getRequestKrok3Wojewodztwo);
        //$request->session()->put('form_adres_contact_email', $getRequestKrok3Email);
        $request->session()->put('form_miasto', $getRequestKrok3Miasto);
        $request->session()->put('form_telefon', $getRequestKrok3Telefon);
        $request->session()->put('form_ulica', $getRequestKrok3Ulica);
        $request->session()->put('form_rodzaj_placowki', $getRequestKrok3RodzajPlacowki);
        $request->session()->put('form_lat', $getRequestKrok3Lat);
        $request->session()->put('form_lon', $getRequestKrok3Lon);
        $request->session()->put('form_nip', $getRequestKrok3Nip);
        
         // Dodawanie lokalizacji 
            $lokalizacja = new Lokalizacja();
            $lokalizacja->alias = Session::get('form_podmiot');
            $lokalizacja->lat = Session::get('form_lat');
            $lokalizacja->lon = Session::get('form_lon');
            $lokalizacja->ulica = Session::get('form_ulica');
            $lokalizacja->id_rodzaj_placowki = Session::get('form_rodzaj_placowki');
            $lokalizacja->id_miasta = Session::get('form_miasto');
            $lokalizacja->id_user = (Auth::check() == true) ? Auth::user()->id : $user->id;
            $lokalizacja->save();

            // Opcjonalnie twórz podmiot ?

            // zapisz lokalizacje w sesji 

            $getRequestKrok4id_lokalizacja = $lokalizacja->id_lokalizacja;
            $request->session()->put('form_id_lokalizacja', $lokalizacja->id_lokalizacja);
            $getLokalizacja = Lokalizacja::where('id_lokalizacja',$getRequestKrok4id_lokalizacja)->first();
            
        return redirect()->route('ogloszenia_dodaj_krok5');
    }

    public function krok4bRequest(Request $request) {
        
        $getRequestKrok4id_lokalizacja = $request->input('id_lokalizacja');
        $request->session()->put('form_id_lokalizacja', $getRequestKrok4id_lokalizacja);
        $getLokalizacja = Lokalizacja::where('id_lokalizacja',$getRequestKrok4id_lokalizacja)->first();

        //dd($getLokalizacja);
       
        $request->session()->put('form_podmiot',$getLokalizacja->alias);
         $request->session()->put('form_miasto', $getLokalizacja->id_miasta);
        $request->session()->put('form_ulica', $getLokalizacja->ulica);
        $request->session()->put('form_rodzaj_placowki', $getLokalizacja->id_rodzaj_placowki);
        //  print_r($getLokalizacja);
        // exit();
        $request->session()->put('form_lat', $getLokalizacja->lat);
        $request->session()->put('form_lon', $getLokalizacja->lon);

            return redirect()->route('ogloszenia_dodaj_krok5');
    }
    public function krok5()
    {

        $doswiadczenie_od =  Session::get('form_doswiadczenie_od');
        $doswiadczenie_do =  Session::get('form_doswiadczenie_do');
        if (Auth::check()) {
            $user = User::where('id', Auth::user()->id)->first();
        
            $podmiot = Podmiot::where('id_podmiot', $user->id_podmiot)->first();
            //$checkPodmiot = Podmiot::where('id_user',Auth::user()->id)->first();
        }
        
        // echo $podmiot->id_miasta;
        // exit();
        if((Auth::check()) && (isset($podmiot->id_miasta))) {
            $miasto = Miasto::where('id_miasta', $podmiot->id_miasta)->first();
        } else {
        $miasto = Miasto::where('id_miasta', Session::get('form_miasto'))->first();
        }
        // echo  Session::get('form_rodzaj_placowki');
        // exit();
        $rodzaj_placowki = RodzajPlacowki::where('id_rodzaj_placowki', Session::get('form_rodzaj_placowki'))->first();
        

        $szukam = TypOgloszenia::where('id_typ_ogloszenia', Session::get('form_szukam'))->first();
        $poziom = Poziom::where('id_poziom', Session::get('form_poziom'))->first();
        $wynagrodzenie_od = Session::get('form_wynagrodzenie_od');
        $wynagrodzenie_do = Session::get('form_wynagrodzenie_do');
        
        
        $specjalizacja = Specjalizacja::where('id_specjalizacje', Session::get('form_specjalizacja'))->first();
        $wymiar_pracy = WymiarPracy::where('id_wymiar_pracy', Session::get('form_wymiar_pracy'))->first();
        $typ_umowy = RodzajUmowy::where('id_rodzaj_umowy', Session::get('form_typ_umowy'))->first();

        $getLokalizacja = Lokalizacja::where('id_lokalizacja',Session::get('form_id_lokalizacja'))->first();

        $kontakt_email = '';
        $kontakt_telefon = '';
        if($getLokalizacja) {
            $kontakt_email = $getLokalizacja->kontakt_email;
            $kontakt_telefon = $getLokalizacja->kontakt_telefon;
        } else {
            $kontakt_email = Session::get('form_adres_email');
            $kontakt_telefon = Session::get('form_telefon');
        }
        return view('frontend.ogloszenia.krok5')
        ->with('doswiadczenie_od',$doswiadczenie_od)
        ->with('doswiadczenie_do',$doswiadczenie_do)
        ->with('imie_nazwisko',Session::get('form_podmiot'))
        ->with('miasto',$miasto->nazwa)
        ->with('rodzaj_placowki',$rodzaj_placowki->nazwa)//$rodzaj_placowki->nazwa 
        ->with('email',$kontakt_email)
        ->with('poziom',$poziom->nazwa)//
        ->with('wynagrodzenie_od',$wynagrodzenie_od)
        ->with('wynagrodzenie_do',$wynagrodzenie_do)
        ->with('specjalizacja',$specjalizacja->nazwa)
        ->with('wymiar_pracy',$wymiar_pracy->nazwa)
        ->with('tresc',Session::get('form_tresc'))
        ->with('szukam','Pracownika')//$szukam->nazwa
        ->with('typ_umowy',$typ_umowy->nazwa);//
    }
    public function krok5Request(Request $request) {
        //Jesli user jest zalogowany to pobierz obiekt usera
        if (Auth::check() == true) {
            $user = User::where('id', Auth::user()->id)->first();
        }
        //Pobierz obiekt usera po adresie email pobranego z sesji form_adres_email
        $getUser = User::where('email',Session::get('form_adres_email'))->count();
        //Jesli nie znajdzie takiego usera w bazie
        if (($getUser) == 0) {
            
            // Jesli user jest niezalogowany lub jesli bedąc zalogwany nie ma firmy
            if ((Auth::check() == false) || ($user->id_podmiot == '')) {
                $getWojewodztwo = Miasto::where('id_miasta', Session::get('form_miasto'))->first();

                // Dodawanie nowej firmy
                $podmiot = new Podmiot();
                $podmiot->nazwa = Session::get('form_podmiot');
                $podmiot->ulica = Session::get('form_ulica');
                $podmiot->id_miasta = Session::get('form_miasto');
                //$podmiot->id_wojewodztwo = $getWojewodztwo->id_wojewodztwo;
                //$podmiot->id_kraj = '1';
                $podmiot->id_rodzaj_placowki = Session::get('form_rodzaj_placowki');
                $podmiot->kod_pocztowy = '';
                $podmiot->lat = Session::get('form_lat');
                $podmiot->lon = Session::get('form_lon');
                $podmiot->nip = Session::get('form_nip');
                $podmiot->podmiot_telefon = Session::get('form_telefon');
                $podmiot->podmiot_email = Session::get('form_adres_email');
                $podmiot->save();

                // Jesli user jest zalogowany
                if (Auth::check() != false) {
                    User::where('id', Auth::user()->id)->update([
                        'id_podmiot' => $podmiot->id_podmiot
                    ]);
                }
            }
            
            // Jesli user jest niezalogowany
            if (Auth::check() == false) {
                //Dodawanie nowego uzytkownika
                $user = User::create([
                    'name' => Session::get('form_podmiot'),
                    'email' => Session::get('form_adres_email'),
                    'password' => Hash::make(time()),
                    'stan' => '0'
                ]);
                
                // Dodawanie nowego profilu
                // -- obecnie nieuzywane
                Profil::create([
                    'o_mnie' => null,
                    'id_specjalizacje' => null,
                    'id_user' => $user->id,
                    'szukam' => 0
                ]);

                $konto = array(
                    'name' =>  Session::get('form_podmiot'),
                    'email' => Session::get('form_adres_email'),
                    'stan' => '0'
                );

                // Dane do wysylki maila z informacja o utworzeniu konta
                $email = trim($user->email);
                $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
                $getUstawienia = Ustawienia::where('instancja', 'mails')->first();
                if ($getUstawienia->wartosc == '1') {
                    try {
                        Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
                            $message->from(env('MAIL_USERNAME'), $temat);
                            $message->to($email)->subject($temat);
                        });
                    } catch (\Exception $e) {
                        return false;
                    }
                }
            }
            // Wybierz lokalizację
            if (Session::get('form_id_lokalizacja') != '') {
                $getLokalizacja = Lokalizacja::where('id_lokalizacja', Session::get('form_id_lokalizacja'))->first();
            } else {
                $getLokalizacja = Lokalizacja::where('id_user', Auth::user()->id)->first();
            }
            // Sprobuj znalezc podmiot o podanym id w uzytkowniku
            try {
                $podmiot = Podmiot::findOrFail($user->id_podmiot);
            } catch(\Exception $e) {
                // Dodawanie nowej firmy
                //if (empty(Session::get('form_id_lokalizacja'))) {
                    $getLokalizacja = Lokalizacja::where('id_user', Auth::user()->id)->first();
                //}
                $podmiot = new Podmiot();
                $podmiot->nazwa = $getLokalizacja->alias;
                $podmiot->ulica = $getLokalizacja->ulica;
                $podmiot->id_miasta = $getLokalizacja->id_miasta;
                $podmiot->id_rodzaj_placowki = $getLokalizacja->id_rodzaj_placowki;
                $podmiot->kod_pocztowy = '';
                $podmiot->lat = $getLokalizacja->lat;
                $podmiot->lon = $getLokalizacja->lon;
                $podmiot->nip = '';
                $podmiot->podmiot_telefon = $getLokalizacja->kontakt_telefon;
                $podmiot->podmiot_email = $getLokalizacja->kontakt_email;
                $podmiot->save();

                // Zaktualizuj informacje dla uzytkownika o firmie
                User::where('id', Auth::user()->id)->update([
                    'id_podmiot' => $podmiot->id_podmiot
                ]);
            }

            // Dodawanie nowego ogloszenia
            $ogloszenie = new Ogloszenie();
            $ogloszenie->email = Session::get('form_adres_email');
            $ogloszenie->imie_nazwisko = Session::get('form_podmiot');
            $ogloszenie->id_user = (Auth::check() == true) ? Auth::user()->id : $user->id;
            $ogloszenie->id_wymiar_pracy = Session::get('form_wymiar_pracy');
            $ogloszenie->id_specjalizacje = Session::get('form_specjalizacja');
            $ogloszenie->doswiadczenie_od = Session::get('form_doswiadczenie_od');
            $ogloszenie->doswiadczenie_do = Session::get('form_doswiadczenie_do');
            $ogloszenie->id_poziom = Session::get('form_poziom');
            $ogloszenie->wynagrodzenie_od = Session::get('form_wynagrodzenie_od');
            $ogloszenie->typ_wynagrodzenia = Session::get('form_typ_wynagrodzenia');
            $ogloszenie->waluta_wynagrodzenia = Session::get('form_waluta_wynagrodzenia');
            $ogloszenie->wynagrodzenie_do = Session::get('form_wynagrodzenie_do');
            //$ogloszenie->id_typ_ogloszenia = Session::get('form_szukam');
            $ogloszenie->id_rodzaj_umowy = Session::get('form_wymiar_pracy');
            $ogloszenie->tresc = Session::get('form_tresc');
            $ogloszenie->rekruter = Session::get('form_rekruter');
            

            // Jesli jest brak podmiotu w uzytkowniku lub user jest niezalogowany, dolacz do ogloszenia id podmiotu bezposrednio z obiektu podmiot
            if ((Auth::check() == false) || ($user->id_podmiot == '')) {
                $ogloszenie->id_podmiot = $podmiot->id_podmiot;
            } elseif(Auth::check() == true) {
                // Jesli user jest zalogowany dolacz id podmiotu dla ogloszenia z obiektu user
                $user = User::where('id',Auth::user()->id)->first();
                $ogloszenie->id_podmiot = $user->id_podmiot;
            } else {
                // W kazdym innym przypadku dolacz id podmiotu z obiektu podmiot
                $ogloszenie->id_podmiot = $podmiot->id_podmiot;
            }
            // Dodanie id lokalizacji do ogłoszenia
            if($getLokalizacja) {
                $ogloszenie->id_lokalizacja = $getLokalizacja->id_lokalizacja;
            }
            $ogloszenie->save();

            // Wysyłka maila z informacjami o ogłoszeniu
            $kontakt = $user->email;//$ogloszenie->email;
            $temat = 'Dodałes ogłoszenie w serwisie '.env('APP_NAME');

            $ogloszenie_mail = array(
                    'id_ogloszenie' => $ogloszenie->id_ogloszenia,
                    'id_user ' => $ogloszenie->id_user,
                    'id_wymiar_pracy' => $ogloszenie->id_wymiar_pracy,
                    'id_specjalizacje' => $ogloszenie->id_specjalizacje,
                    'doswiadczenie_od' => $ogloszenie->doswiadczenie_od,
                    'doswiadczenie_do' => $ogloszenie->doswiadczenie_do,
                    'id_poziom' => $ogloszenie->id_poziom,
                    'wynagrodzenie_od' => $ogloszenie->wynagrodzenie_od,
                    'wynagrodzenie_do' => $ogloszenie->wynagrodzenie_do,
                    'id_rodzaj_umowy' => $ogloszenie->id_rodzaj_umowy,
                    'tresc' => $ogloszenie->tresc,
                    'id_podmiot' => $ogloszenie->id_podmiot
                );

            $data = array('temat'=>$temat, 'email'=>$kontakt, 'ogloszenie_mail'=>$ogloszenie_mail);
            $getUstawienia = Ustawienia::where('instancja', 'mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                    Mail::send('mails.ogloszenie', $data, function ($message) use ($kontakt, $temat) {
                        $message->from(env('MAIL_USERNAME'), $kontakt);
                        $message->to($kontakt)->subject($temat);
                    }); 
                } catch (\Exception $e) {
                        return false;
                    }
            }

            // Czyszczenie sesji
            $request->session()->put('form_specjalizacja', '');
            $request->session()->put('form_wynagrodzenie_od', '');
            $request->session()->put('form_wynagrodzenie_do', '');
            $request->session()->put('form_doswiadczenie_od', '');
            $request->session()->put('form_doswiadczenie_do', '');
            $request->session()->put('form_poziom', '');
            $request->session()->put('form_wymiar_pracy', '');
            $request->session()->put('form_typ_umowy', '');
            $request->session()->put('form_tresc', '');
            $request->session()->put('form_szukam', '');
            $request->session()->put('form_imie_nazwisko', '');
            $request->session()->put('form_podmiot', '');
            $request->session()->put('form_adres_email', '');
            $request->session()->put('form_miasto', '');
            $request->session()->put('form_telefon', '');
            $request->session()->put('form_ulica', '');
            $request->session()->put('form_rodzaj_placowki', '');
            $request->session()->put('form_lat', '');
            $request->session()->put('form_lon', '');
            $request->session()->put('form_nip', '');

            return redirect()->route('ogloszenia_dodaj_krok6',['id'=>$ogloszenie->id_ogloszenia, 'nazwa'=>str_slug($podmiot->nazwa)]);
        } else { 
            // Zwroc komunikat o isniejacym uzytkowniku w bazie i przekieruj routing
            Session::flash('status_fail', 'Istnieje już użytkownik o podanym adresie e-mail');
            return redirect()->back();
        }
    }

    public function krok6($id, $nazwa)
    {
        return view('frontend.ogloszenia.krok6')->with('id', $id)->with('nazwa', $nazwa);
    }
    public function getOgloszenia($id_wojewodztwo, $id_specjalizacje) {
        if (($id_specjalizacje == '0') && ($id_wojewodztwo == '0')){ 
            $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->get();
        } else {
            if ($id_specjalizacje == '0') {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('miasta','miasta.id_miasta','=','podmiot.id_miasta')->where('miasta.id_wojewodztwa', $id_wojewodztwo)->get();
            } 
            else if ($id_wojewodztwo == '0') {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
            } else {
                $ogloszenia = (new Ogloszenie())->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
            }
        }
        return $ogloszenia;
    }
    
    public function getPracownikDependsOnSpecjalizacja($id_specjalizacje) {
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getPracownicy = (new Profil())->join('users','profil.id_user', '=', 'users.id')->leftJoin('specjalizacje','specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->where('profil.id_specjalizacje', $id_specjalizacje)->where('profil.szukam','1')->get();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getPracownicy)->with('id_specjalizacje',$id_specjalizacje);
    }
    public function getPracownikDependsOnWojewodztwo($id_wojewodztwa) {
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        if ($id_wojewodztwa == 'wszystkie') {
            $getPracownicy = (new Profil())->join('users', 'profil.id_user', '=', 'users.id')->leftJoin('specjalizacje', 'specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->leftJoin('miasta', 'miasta.id_miasta', 'profil.id_miasta')->where('profil.id_specjalizacje','!=','')->where('profil.id_miasta','!=','')->where('profil.szukam','1')->get();
        } else { 
            $getPracownicy = (new Profil())->join('users', 'profil.id_user', '=', 'users.id')->leftJoin('specjalizacje', 'specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->leftJoin('miasta', 'miasta.id_miasta', 'profil.id_miasta')->where('miasta.id_wojewodztwa', $id_wojewodztwa)->where('profil.id_specjalizacje','!=','')->where('profil.id_miasta','!=','')->where('profil.szukam','1')->get();
        }
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getPracownicy);
    }
    public function getOgloszeniaDependsOnWojewodztwo($id_wojewodztwa) {
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->leftJoin('miasta','miasta.id_miasta','podmiot.id_miasta')->where('miasta.id_wojewodztwa', $id_wojewodztwa)->get();

        $wojewodztwo = Wojewodztwo::where('id_wojewodztwa', $id_wojewodztwa)->first();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia)->with('id_wojewodztwo', $id_wojewodztwa);
    }

    
    public function getOgloszeniaDependsOnWynagrodzenie($wynagrodzenie_od, $wynagrodzenie_do ){
        
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.wynagrodzenie_od', '>=', $wynagrodzenie_od)->where('ogloszenia.wynagrodzenie_do', '<=', $wynagrodzenie_do)->get();
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia);
    }
    public function getOgloszeniaDependsOnSpecjalizacja($id_specjalizacje) { 
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
        

        $specjalizacja = Specjalizacja::where('id_specjalizacje', $id_specjalizacje)->first();
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia)->with('id_specjalizacje',$id_specjalizacje)->with('specjalizacja', $specjalizacja);
    }

    public function storeOgloszeniaDependsOnSpecjalizacjaWojewodztwo(Request $request) {
      
        if ($request->input('id_wojewodztwa') != '0') {
            $wojewodztwo_alias = Wojewodztwo::where('id_wojewodztwa', $request->input('id_wojewodztwa'))->first()->nazwa;
        } else {
            $wojewodztwo_alias = 'wszystkie';
        }
       
       // dd($request);
       if (($request->input('specjalizacja') != '0') && ($request->input('specjalizacja') != '')) {
           $aliasSpecjalizacja = Specjalizacja::where('id_specjalizacje',$request->input('specjalizacja'))->first()->nazwa;
       } else {
        $aliasSpecjalizacja = 'wszystkie';
       }
  
        if($request->input('specjalizacja') == '') { 
             $id_specjalizacja =  '0';
        } else {
            $id_specjalizacja = $request->input('specjalizacja');
        }
        return redirect()->route('ogloszenia_specjalizacja_wojewodztwa_index',['id_specjalizacje'=>$id_specjalizacja,'name'=>str_slug($aliasSpecjalizacja),'id_wojewodztwa'=>$request->input('id_wojewodztwa'),'nazwa'=>str_slug($wojewodztwo_alias)]);
    }
    public function getOgloszeniaDependsOnSpecjalizacjaWojewodztwo($id_specjalizacje, $specjalizacja_name, $id_wojewodztwa, $wojewodztwo_name) { 
        $getSpecjalizacje = Controller::getHomeSpecjalizacje();//(new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();
        
        // Tylko specjalizacje
        if (($id_specjalizacje != '0') && ($id_wojewodztwa == '0')) {
          //  dd("A");
            $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('lokalizacja','ogloszenia.id_lokalizacja','lokalizacja.id_lokalizacja')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->get();
        } 
        // Tylko województwa
        else if(($id_wojewodztwa != '0') && ($id_specjalizacje == 0)) {
            $getMiastaFromWojewodztwo = Miasto::where('id_wojewodztwa', $id_wojewodztwa)->pluck('id_miasta');
            //dd($getMiastaFromWojewodztwo);
//dd("B");
        $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('lokalizacja','ogloszenia.id_lokalizacja','lokalizacja.id_lokalizacja')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->whereIn('lokalizacja.id_miasta', $getMiastaFromWojewodztwo)->OrWhereIn('podmiot.id_miasta', $getMiastaFromWojewodztwo)->get();
        } 
        else if(($id_wojewodztwa != '0') && ($id_specjalizacje != '0')) {
                 $getMiastaFromWojewodztwo = Miasto::where('id_wojewodztwa', $id_wojewodztwa)->pluck('id_miasta');
            //dd($getMiastaFromWojewodztwo);
//dd("C");
            $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('lokalizacja','ogloszenia.id_lokalizacja','lokalizacja.id_lokalizacja')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_specjalizacje', $id_specjalizacje)->whereIn('lokalizacja.id_miasta', $getMiastaFromWojewodztwo)->OrWhereIn('podmiot.id_miasta', $getMiastaFromWojewodztwo)->get();
        } else {
//dd("D");
            $getOgloszenia = (new Ogloszenie())->join('specjalizacje','ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('lokalizacja','ogloszenia.id_lokalizacja','lokalizacja.id_lokalizacja')->leftJoin('podmiot','ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->get();
        }
        
        return view('frontend.ogloszenia.show')->with('specjalizacje',$getSpecjalizacje)->with('wojewodztwa',$getWojewodztwa)->with('ogloszenia',$getOgloszenia)->with('id_specjalizacje',$id_specjalizacje)->with('id_wojewodztwa', $id_wojewodztwa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $getSpecjalizacje = Controller::getHomeSpecjalizacje();
            $getWojewodztwa = (new Wojewodztwo())->get();

            $getOgloszenia = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot', 'ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('id_ogloszenia', $id)->get();
            $singleOgloszenie = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot', 'ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_ogloszenia', $id)->get();

            $getOgloszenie = Ogloszenie::where('id_ogloszenia', $id)->first();
            //Sprawdz lokalizacje
            if ($getOgloszenie->id_lokalizacja != '') {
                // Wybierz dane z lokalizacji do widoku
            
                $getLokalizacja = Lokalizacja::where('id_lokalizacja', $getOgloszenie->id_lokalizacja)->first();
            } else {
                $getLokalizacja = null;
            }

            $showOgloszenia = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot', 'ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('podmiot.id_podmiot', $getOgloszenie->id_podmiot)->get();

           // dd($showOgloszenia);
            return view('frontend.ogloszenia.show')->with('specjalizacje', $getSpecjalizacje)->with('wojewodztwa', $getWojewodztwa)->with('showOgloszenia', $showOgloszenia)->with('ogloszenia', $getOgloszenia)->with('ogloszenie', $singleOgloszenie)->with('id_ogloszenia', $id)->with('lokalizacja', $getLokalizacja);
        } catch (\Exception $e) {
            // return false;
            return view('errors.404');
        }
    }

     public static function getOgloszeniaFromCompany($id) {
        try {
            $getOgloszenie = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot', 'ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('ogloszenia.id_ogloszenia', $id)->first();

            $showOglosznia = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('podmiot', 'ogloszenia.id_podmiot', '=', 'podmiot.id_podmiot')->where('podmiot.id_podmiot', $getOgloszenie->id_podmiot)->get();
            return $showOglosznia;
        } catch(\Exception $e) {
            return false;
        }
    }

    public function getMiasta() { // tesotwa metoda

    $oClient = new Client();
    $oClient->setConfig('mjendraszczyk','Di9!neTp78',true);
    $oNativeApi = NativeApi::create($oClient);

    var_dump($oNativeApi->CzyZalogowany());
    var_dump($oNativeApi->PobierzSlownikCechULIC());
    //var_dump($oNativeApi->WyszukajMiejscowosc('skiernie',null));

    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {
        //provinceId
 
        //var_dump($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));

        foreach($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
            //dd($powiat);
            //dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
            foreach($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
            if ($gmina->typeName == 'miasto') { 
                //print_r($gmina);
                dd($gmina);
            }
        }
        }
    }
    //var_dump($oNativeApi->PobierzListePowiatow());

    }
    public function storeAplikacje($id_ogloszenia, AplikacjeRequest $request)
    {
        $aplikacja = new Aplikacja();
        $upload = new Controller();
        $upload->upload('cv', 'pdf,doc,docx,odt', '/documents', $request->get('cv'), $request, 0, 0);
        // dd($request);
        $aplikacja->id_ogloszenia = $id_ogloszenia;
        $aplikacja->imie_nazwisko = $request->input('imie_nazwisko');
        $aplikacja->tresc = $request->input('tresc');
        $aplikacja->email = $request->input('email');
        $aplikacja->id_user = (Auth::user()) ? Auth::user()->id : null;
        $aplikacja->zalacznik = $upload->getFileName();
        $aplikacja->save();
        
        $temat = "Nowa wiadomosc w sprawie ogłoszenia na ".env('APP_NAME');
        $user_email = trim($aplikacja->email);
        
        $getPodmiot = Ogloszenie::where('id_ogloszenia', $id_ogloszenia)->join('users','users.id', '=','ogloszenia.id_user')->first();
        $email = $getPodmiot->email;
        $data = array('temat'=>$temat, 'email'=>$email, 'user_email'=>$user_email, 'imie' => $aplikacja->imie_nazwisko, 'zalacznik' => $aplikacja->zalacznik);

        $getUstawienia = Ustawienia::where('instancja', 'mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                    Mail::send('mails.nowa_aplikacja', $data, function ($message) use ($temat, $email) {
                        $message->from(env('MAIL_USERNAME'), $temat);
                        $message->to($email)->subject($temat);
                    });
                } catch (\Exception $e) {
                    return false;
                }
            }

        Session::flash('status', 'Zapisano pomyślnie.');
        // powiadomienie mailowe

        return redirect()->route('ogloszenia_zobacz',['id_ogloszenia'=>$id_ogloszenia]);
    }

}
