<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Faq;
use App\Cms;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
    public function onas() {
        $Serwis = Cms::where('id_cms', 1)->get();

        return view('frontend.cms.onas')->with('serwis',$Serwis);
    }
    public function odstapienie_od_umowy()
    {
        $Odstapienie = Cms::where('id_cms', 4)->get();

        return view('frontend.cms.odstapienie_od_umowy')->with('odstapienie',$Odstapienie);
    }
    public function polityka_prywatnosci()
    {
        $Polityka = Cms::where('id_cms', 3)->get();

        return view('frontend.cms.polityka_prywatnosci')->with('polityka',$Polityka);
    }
    public function faq() {
        $Faq = Faq::get();
        return view('frontend.cms.faq')->with('faq',$Faq);
    }
     public function kontakt() {
        return view('frontend.cms.kontakt');
    }
    public function regulamin() { 
        $Regulamin = Cms::where('id_cms',2)->get();
        return view('frontend.cms.regulamin')->with('regulamin', $Regulamin);
    }
}
