<?php

namespace App\Http\Controllers\frontend;

use App\Miasto;
use App\Poziom;
use App\User;
use App\Specjalizacja;
use App\Doswiadczenie;
use App\Wynagrodzenie;
use App\WymiarPracy;
use App\Wojewodztwo;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\Ogloszenie;
use App\TypOgloszenia;
use App\Profil;
use App\Lokalizacja;
use Image;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\backend\OgloszeniaRequest;
use App\Http\Requests\frontend\LokalizacjaRequest;
use Illuminate\Support\Facades\Mail;

use App\Edukacja;
use App\Aplikacja;

use App\Podmiot;
use Auth;
use Session;
use App\DodatkoweAdresy;
use App\Ustawienia;

use App\Http\Requests\frontend\UzytkownicyRequest;
use App\Http\Requests\frontend\ProfilLekarskiRequest;
use App\Http\Requests\frontend\AktywacjaRequest;


use App\Http\Requests\backend\PodmiotRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilController extends Controller
{
    public $getSpecjalizacje;
    public $getWojewodztwa;
    public $getOglosznia;
    public $getProfil;
    public $userID;
    public $user;

    public $doswiadczenie;
    public $edukacja;
    public $getDodatkoweAdresy;

    public function __construct()
    {
        $this->getSpecjalizacje = (new Specjalizacja())->get();
        $this->getWojewodztwa = (new Wojewodztwo())->get();
        $this->getRodzajePlacowki = (new RodzajPlacowki())->get();
        $this->getOglosznia = (new Ogloszenie())->join('specjalizacje', 'ogloszenia.id_specjalizacje', '=', 'specjalizacje.id_specjalizacje')->leftJoin('users', 'ogloszenia.id_user', '=', 'users.id')->leftJoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->get();
    }
    public function getAuth()
    {
        $this->user = Auth::user();
        return $this->user;
    }
    public function index()
    {
        return redirect()->route('login');
    }
    public function show($id)
    {
        $getProfil = (new User())->join('profil', 'profil.id_user', '=', 'users.id')->where('users.id', $id)->first();
        $this->edukacja = (new User())->leftjoin('edukacja', 'users.id', '=', 'edukacja.id_user')->where('users.id', $id)->get();

        $this->doswiadczenie =  User::leftjoin('doswiadczenie', 'users.id', '=', 'doswiadczenie.id_user')->where('users.id', $id)->get();

        $getPracownicy = (new Profil())->join('users','profil.id_user', '=', 'users.id')->leftJoin('specjalizacje','specjalizacje.id_specjalizacje', '=', 'profil.id_specjalizacje')->where('profil.id_specjalizacje', $getProfil->id_specjalizacje)->where('profil.id_miasta','!=','')->where('profil.szukam','1')->get();

        return view('frontend.profil.show')->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $getPracownicy)->with('profil', $getProfil)->with('edukacja',$this->edukacja)->with('doswiadczenie',$this->doswiadczenie);
    }
    public function dane()
    {
        $this->getProfil = (new User())->leftjoin('profil', 'profil.id_user', '=', 'users.id')->leftjoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->where('users.id', $this->getAuth()->id)->first();
 
        return view('frontend.profil.dane')->with('imie_nazwisko', $this->getAuth()->name)->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $this->getOglosznia)->with('profil', $this->getProfil)->with('rodzaje_placowki',$this->getRodzajePlacowki);
    }

    public function profil_lekarski()
    {
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        $this->edukacja = (new User())->leftjoin('edukacja', 'users.id', '=', 'edukacja.id_user')->where('users.id', $this->getAuth()->id)->first();

        $this->edukacja_others = (new User())->leftjoin('edukacja', 'users.id', '=', 'edukacja.id_user')->where('users.id', $this->getAuth()->id)->get();

        $this->doswiadczenie =  User::leftjoin('doswiadczenie', 'users.id', '=', 'doswiadczenie.id_user')->where('users.id', $this->getAuth()->id)->first();
        

        $this->doswiadczenie_others =  User::leftjoin('doswiadczenie', 'users.id', '=', 'doswiadczenie.id_user')->where('users.id', $this->getAuth()->id)->get();
        
        return view('frontend.profil.profil_lekarski')->with('imie_nazwisko', $this->getAuth()->name)->with('specjalizacje', $this->getSpecjalizacje)->with('wojewodztwa', $this->getWojewodztwa)->with('ogloszenia', $this->getOglosznia)->with('profil', $this->getProfil)->with('edukacja', $this->edukacja)->with('doswiadczenie', $this->doswiadczenie)->with('edukacja_others', $this->edukacja_others)->with('doswiadczenie_others', $this->doswiadczenie_others);
    }

    public function platnosci()
    {
         $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        return view('frontend.profil.platnosci')->with('imie_nazwisko', $this->getAuth()->name)->with('profil', $this->getProfil);
    }
    public function ogloszenia()
    {
        $id = Auth::user()->id;
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();

        $getProfilOgloszenia = Ogloszenie::where('id_user', $id)->get();
        return view('frontend.profil.ogloszenia')->with('imie_nazwisko', $this->getAuth()->name)->with('ogloszenia', $getProfilOgloszenia)->with('profil', $this->getProfil);
    }
    public function editOgloszenie($id) {
        $this->getProfil = User::leftjoin('profil', 'users.id', '=', 'profil.id_user')->where('users.id', $this->getAuth()->id)->first();
        $ogloszenia = Ogloszenie::where('id_ogloszenia', $id)->where('id_user', Auth::user()->id)->first();
        $getWojewodztwa = (new Wojewodztwo())->get();

        $this->getDodatkoweAdresy = DodatkoweAdresy::where('id_ogloszenia', $id)->get();

        return view('frontend.profil.edit')->with('ogloszenia', $ogloszenia)->with('wojewodztwa', $getWojewodztwa)->with('imie_nazwisko', $this->getAuth()->name)->with('profil', $this->getProfil)->with('dodatkowe_adresy',$this->getDodatkoweAdresy);
    }
    public function updateOgloszenie(OgloszeniaRequest $request,$id) {
         $ogloszenia = Ogloszenie::where('id_ogloszenia', $id)->where('id_user', Auth::user()->id);
        
        $ogloszenia->update([
        'imie_nazwisko' => $request->input('imie_nazwisko'),
        'email' => Auth::user()->email,
        'id_user' => Auth::user()->id,
        'id_specjalizacje' => $request->input('id_specjalizacje'),
        'id_wymiar_pracy' => $request->input('id_wymiar_pracy'),
        'doswiadczenie_od' => $request->input('doswiadczenie_od'),
        'doswiadczenie_do' => $request->input('doswiadczenie_do'),
        'id_poziom' => $request->input('id_poziom'),
        'id_rodzaj_umowy' => $request->input('id_rodzaj_umowy'),
        'wynagrodzenie_od' => $request->input('wynagrodzenie_od'),
        'wynagrodzenie_do' => $request->input('wynagrodzenie_do'),
        'typ_wynagrodzenia' => $request->input('typ_wynagrodzenia'),
        'waluta_wynagrodzenia' => $request->input('waluta_wynagrodzenia'),
        'tresc' => $request->input('tresc'),
        'id_podmiot' => Auth::user()->id_podmiot,
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }
    public function updateDane(UzytkownicyRequest $request)
    {
        $id = Auth::user()->id;
        $upload = new Controller();

        $uzytkownicy = User::where('id', $id);
// dd($request);

        if ((Input::file('avatar_upload'))) {
            $upload->upload('avatar_upload', 'jpg,jpeg,png', '/img/avatar/', $request->get('avatar_upload'), $request, 200, 200);
            $uzytkownicy->update([
                'avatar_upload' => $upload->getFileName()
            ]);
        }

        if (!empty($request->input('password'))) {
            if ($request->input('password') == $request->input('password_repeat')) {
                $uzytkownicy->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'telefon' => $request->input('telefon'),
            ]);
                Session::flash('status', 'Zapisano pomyślnie.');
            } else {
                Session::flash('status_fail', 'Hasła do siebie nie pasują');
            }
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'telefon' => $request->input('telefon'),
            ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        }
        return redirect()->back();
    }
    public function updateDaneFirma(PodmiotRequest $request)
    {
        //'nazwa', 'ulica', 'id_miasta', 'kod_pocztowy', 'lat', 'lon', 'nip','podmiot_telefon', 'podmiot_email'
        $id = Auth::user()->id_podmiot;
        $upload = new Controller();
        
        //dd($id);
        if ($id != null) {
            $podmiot = Podmiot::where('id_podmiot', $id);
            if ((Input::file('logo_upload'))) {
               
                $upload->upload('logo_upload', 'jpg,jpeg,png', '/img/logo/', $request->get('logo_upload'), $request, 200, 200);
                $podmiot->update([
                'logo_upload' => $upload->getFileName()
            ]);
            }
        }

        if ($id != null) {
            $podmiot = Podmiot::where('id_podmiot', $id);
            $podmiot->update([
                'nazwa' => $request->input('nazwa'),
                'ulica' => $request->input('ulica'),
                'id_miasta' => $request->input('id_miasta'),
                'kod_pocztowy' => $request->input('kod_pocztowy'),
                'lat' => $request->input('lat'),
                'lon' => $request->input('lon'),
                'nip' => $request->input('nip'),
                'podmiot_telefon' => $request->input('podmiot_telefon'),
                'podmiot_email' => $request->input('podmiot_email'),
                'id_rodzaj_placowki' => $request->input('id_rodzaj_placowki'),
            ]);
            $getLokalizacje = Lokalizacja::where('id_user', Auth::user()->id)->count();
            if($getLokalizacje == 0) {
                $lokalizacja = new Lokalizacja();
                $lokalizacja->alias = $request->input('nazwa');
                $lokalizacja->id_user = Auth::user()->id;
                $lokalizacja->lat = $request->input('lat');
                $lokalizacja->lon = $request->input('lon');
                $lokalizacja->ulica = $request->input('ulica');
                $lokalizacja->id_miasta = $request->input('id_miasta');
                $lokalizacja->id_rodzaj_placowki = $request->input('id_rodzaj_placowki');

                $lokalizacja->save();
            }
        } else { 
            $podmiot = new Podmiot();
            $podmiot->nazwa = $request->input('nazwa');
                $podmiot->ulica = $request->input('ulica');
                $podmiot->id_miasta = $request->input('id_miasta');
                $podmiot->kod_pocztowy = $request->input('kod_pocztowy');
                $podmiot->lat = $request->input('lat');
                $podmiot->lon = $request->input('lon');
                $podmiot->nip = $request->input('nip');
                $podmiot->podmiot_telefon = $request->input('podmiot_telefon');
                $podmiot->podmiot_email = $request->input('podmiot_email');
                $podmiot->id_rodzaj_placowki = $request->input('id_rodzaj_placowki');
            
            $podmiot->save();

                // Tworzenie nowej lokalizacji 
            $lokalizacja = new Lokalizacja();
            $lokalizacja->alias = $request->input('nazwa');
            $lokalizacja->id_user = Auth::user()->id;
            $lokalizacja->lat = $request->input('lat');
            $lokalizacja->lon = $request->input('lon');
            $lokalizacja->ulica = $request->input('ulica');
            $lokalizacja->id_miasta = $request->input('id_miasta');
            $lokalizacja->id_rodzaj_placowki = $request->input('id_rodzaj_placowki');

            $lokalizacja->save();

               $user = User::where('id', Auth::user()->id);
            $user->update([
                'id_podmiot' => $podmiot->id_podmiot
            ]);
        }
        if(Ogloszenie::where('id_user', Auth::user()->id)->count() == 0) {
            //Session::flash('status', 'Zapisano pomyślnie.');
            //return redirect()->back();
            return redirect()->route('oglosznia_dodaj_krok2');
        } else {
            Session::flash('status', 'Zapisano pomyślnie.');
            return redirect()->back();
        }
        
    }
    public function profil_user() {
        $this->getProfil = (new User())->join('profil', 'profil.id_user', '=', 'users.id')->where('users.id',Auth::user()->id)->first();
        return view('frontend.profil.konto')->with('profil',$this->getProfil)->with('imie_nazwisko', $this->getAuth()->name);
    }
    public function updateProfilLekarski(ProfilLekarskiRequest $request)
    {
    //  dd($request);

        $profil = Profil::where('id_user', Auth::user()->id);
        $profil->update([
            'id_specjalizacje' => $request->input('id_specjalizacje'),
            'o_mnie' => $request->input('tresc'),
            'id_miasta' => $request->input('id_miasta'),
            'szukam' => $request->input('szukam'),
            'lat' => $request->input('lat'),
            'lon' => $request->input('lon'),
        ]);
        
        if (Profil::where('id_user', Auth::user()->id)->exists() == false) {
            $profil->insert([
            'id_specjalizacje' => $request->input('id_specjalizacje'),
            'o_mnie' => $request->input('tresc'),
            'id_user' => Auth::user()->id,
            'id_miasta' => $request->input('id_miasta'),
            'lat' => $request->input('lat'),
            'lon' => $request->input('lon'),
            ]);
        }


        Edukacja::where('id_user', Auth::user()->id)->delete();

        foreach($request->input('edukacja_nazwa') as $key => $edukacja) { 
            Edukacja::insert([
            'nazwa' => $request->input('edukacja_nazwa')[$key],
            'edukacja_lata_od' => $request->input('edukacja_lata_od')[$key],
            'edukacja_lata_do' => $request->input('edukacja_lata_do')[$key],
            'id_tytul' => $request->input('id_tytul')[$key],
            'id_user' => Auth::user()->id,
        ]);
        }

        Doswiadczenie::where('id_user', Auth::user()->id)->delete();

        foreach ($request->input('doswiadczenie_nazwa') as $key => $edukacja) {
            Doswiadczenie::insert([
                'nazwa' => $request->input('doswiadczenie_nazwa')[$key],
                'doswiadczenie_lata_od' => $request->input('doswiadczenie_lata_od')[$key],
                'doswiadczenie_lata_do' =>  $request->input('doswiadczenie_lata_do')[$key],
                'id_user' => Auth::user()->id,
            ]);
        }

        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }
    public function usun_wiadomosci_ogloszenia($id_wiadomosci) {
        $getZalacznik = Aplikacja::where('id_aplikacje',$id_wiadomosci)->first();
        unlink(public_path('documents/'.$getZalacznik->zalacznik));
        $getWiadomosc = Aplikacja::where('id_aplikacje',$id_wiadomosci)->delete();
        return redirect()->back();
    }
    public function wiadomosci_ogloszenia($id_ogloszenia) { 
        $this->getProfil = (new User())->join('profil', 'profil.id_user', '=', 'users.id')->where('users.id',Auth::user()->id)->first();

        $wiadomosci = Aplikacja::where('id_ogloszenia', $id_ogloszenia)->get();

        return view('frontend.profil.wiadomosci',compact('wiadomosci'))->with('imie_nazwisko', $this->getAuth()->name)->with('profil', $this->getProfil);
    }
    public function aktywacja($email)
    {
        $user = User::where('email',$email)->first();
        return view('frontend.profil.aktywacja')->with('user_id',$user->id)->with('user',$user)->with('wojewodztwa', Controller::getWojewodztwoByMiasto(null));
        
    }
    public function aktywacja_proces(AktywacjaRequest $request,$id) {
        $user = User::where('id',$id)->first();
        if ($request->input('password') == $request->input('password_repeat')) {
            $user->update([
            'password' => bcrypt($request->input('password')),
            'stan'=>'1',
            'id_rola' => '1'
        ]);

        $profil = Profil::where('id_user',$id);
        $profil->update([
            'id_specjalizacje' => $request->input('id_specjalizacje'),
            'id_miasta' => $request->input('id_miasta'),
        ]);
               $email = $user->email;
               $kontakt = $email;

               $konto = array(
                'name' =>  $kontakt,
                'email' => $email,
                'stan' => '0'
            );
                $temat = 'Aktywacja konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
            
                  $getUstawienia = Ustawienia::where('instancja','mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                    Mail::send('mails.aktywacja', $data, function ($message) use ($email, $kontakt, $temat) {
                        $message->from(env('MAIL_USERNAME'), $kontakt);
                        $message->to($email, $kontakt)->subject($temat);
                    });
                } catch (\Exception $e) {
                    return false;
                }
            }

        return redirect()->route('home');
        } else {
            Session::flash('status_fail', 'Hasła do siebie nie pasują');
            return view('frontend.profil.aktywacja')->with('user_id',$user->id);
        }

    }
        public function profil_ogloszenia_delete($id_ogloszenia) {
        $getOgloszenie = Ogloszenie::where('id_ogloszenia', $id_ogloszenia)->delete();
        return redirect()->back();
    }
    public function profil_usun_konto() {
        $id = Auth::user()->id;
        $getDoswiadczenie = Doswiadczenie::where('id_user',$id)->delete();
        $getDoswiadczenie = Doswiadczenie::where('id_user',$id)->delete();
        $getEdukacja = Edukacja::where('id_user',$id)->delete();
        $getProfil = Profil::where('id_user',$id)->delete();
        $getUser = User::where('id',$id)->delete();
        $getOgloszenia = Ogloszenie::where('id_user',$id)->delete();
        //podmiot ??
        return redirect()->back();
    }
    public function lokalizacje_index() {
         $this->getProfil = (new User())->leftjoin('profil', 'profil.id_user', '=', 'users.id')->leftjoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->where('users.id', $this->getAuth()->id)->first();
         $getLokalizacje = Lokalizacja::where('id_user',Auth::user()->id)->get();

        return view('frontend.profil.lokalizacje')->with('profil',$this->getProfil)->with('imie_nazwisko', $this->getAuth()->name)->with('lokalizacje',$getLokalizacje);
    }
    public function lokalizacje_create() {
        $this->getProfil = (new User())->leftjoin('profil', 'profil.id_user', '=', 'users.id')->leftjoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->where('users.id', $this->getAuth()->id)->first();
         
        return view('frontend.profil.lokalizacje_create')->with('profil',$this->getProfil)->with('imie_nazwisko', $this->getAuth()->name)->with('rodzaje_placowki', $this->getRodzajePlacowki);
    }
    public function lokalizacje_edit($id) {
        $this->getProfil = (new User())->leftjoin('profil', 'profil.id_user', '=', 'users.id')->leftjoin('podmiot', 'users.id_podmiot', '=', 'podmiot.id_podmiot')->where('users.id', $this->getAuth()->id)->first();


        $getLokalizacja = Lokalizacja::where('id_lokalizacja',$id)->first();

        return view('frontend.profil.lokalizacje_edit')->with('profil',$this->getProfil)->with('imie_nazwisko', $this->getAuth()->name)->with('lokalizacja',$getLokalizacja)->with('rodzaje_placowki', $this->getRodzajePlacowki);
    }
    public function lokalizacje_store(LokalizacjaRequest $request) {
        $newLokalizacja = new Lokalizacja();
        $newLokalizacja->alias = $request->input('alias');
        $newLokalizacja->lat = $request->input('lat');
        $newLokalizacja->lon = $request->input('lon');
        $newLokalizacja->id_miasta = $request->input('id_miasta');
        $newLokalizacja->id_user = Auth::user()->id;
        $newLokalizacja->ulica = $request->input('ulica');
        $newLokalizacja->id_rodzaj_placowki = $request->input('id_rodzaj_placowki');
        $newLokalizacja->kontakt_email = $request->input('kontakt_email');
        $newLokalizacja->kontakt_telefon = $request->input('kontakt_telefon');
        $newLokalizacja->save();
//dd($request);
        if($request->query('status') == 'krok4') {
            return redirect()->route('ogloszenia_dodaj_krok4');
        } else {
            return redirect()->route('profil_lokalizacje_index');
        }
    }
    public function lokalizacje_update(LokalizacjaRequest $request, $id) {
        $getLokalizacja = Lokalizacja::where('id_lokalizacja',$id)->update([
            'alias' => $request->input('alias'),
            'lat' => $request->input('lat'),
            'lon' => $request->input('lon'),
            'id_miasta' => $request->input('id_miasta'),
            'id_user' => Auth::user()->id,
            'id_rodzaj_placowki' => $request->input('id_rodzaj_placowki'),
            'ulica' => $request->input('ulica'),
            'kontakt_email' => $request->input('kontakt_email'),
            'kontakt_telefon' => $request->input('kontakt_telefon'),
        ]);
 
        return redirect()->back();
    }
    public function lokalizacje_delete($id) {
        $getLokalizacja = Lokalizacja::where('id_lokalizacja', $id)->delete();
        return redirect()->route('profil_lokalizacje_index');
    }
}
