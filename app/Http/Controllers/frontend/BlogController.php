<?php
namespace App\Http\Controllers\frontend;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index() {
        $blog = Blog::get();

        return view('frontend.blog.index')->with('items',$blog);
    }
    public function show($id) {
        $blog = Blog::where('id_blog', $id)->get();
        return view('frontend.blog.show')->with('blog',$blog);
    }
}
