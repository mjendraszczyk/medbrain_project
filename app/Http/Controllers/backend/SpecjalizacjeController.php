<?php

namespace App\Http\Controllers\backend;

use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\SpecjalizacjeRequest;


class SpecjalizacjeController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specjalizacje = Specjalizacja::paginate($this->limit);
        return view('backend.specjalizacje.index')->with('items',$specjalizacje);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.specjalizacje.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpecjalizacjeRequest $request)
    {
        $specjalizacje = new Specjalizacja();
        $specjalizacje->nazwa = $request->input('nazwa');
        $specjalizacje->symbol = $request->input('symbol');
        $specjalizacje->kolor = substr($request->input('kolor'), 1);
        $specjalizacje->tresc = $request->input('tresc');
        $specjalizacje->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specjalizacje = Specjalizacja::where('id_specjalizacje', $id)->first();

        return view('backend.specjalizacje.edit')->with('specjalizacje', $specjalizacje);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SpecjalizacjeRequest $request, $id)
    {
        $specjalizacje = Specjalizacja::where('id_specjalizacje', $id);
        $specjalizacje->update([
        'nazwa' => $request->input('nazwa'),
        'symbol' => $request->input('symbol'),
        'kolor' => substr($request->input('kolor'), 1),
        'tresc' => $request->input('tresc'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specjalizacje = Specjalizacja::where('id_specjalizacje', $id);
        $specjalizacje->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Specjalizacja()));
    }
}
