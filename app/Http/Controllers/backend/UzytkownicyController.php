<?php

namespace App\Http\Controllers\backend;

use App\User;
use App\Edukacja;
use App\Doswiadczenie;
use App\Ogloszenie;
use App\Podmiot;
use App\Profil;

use App\Specjalizacja;
use App\Wojewodztwo;
use Session;
use Validator;

use Illuminate\Support\Facades\Mail;
use App\Ustawienia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\UzytkownicyRequest;
use Illuminate\Support\Facades\DB;


class UzytkownicyController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uzytkownicy = User::paginate($this->limit);
        return view('backend.uzytkownicy.index')->with('items',$uzytkownicy);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wojewodztwo = Wojewodztwo::get();
        $podmioty = Podmiot::get();
        return view('backend.uzytkownicy.create')->with('wojewodztwa',$wojewodztwo)->with('podmioty',$podmioty);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UzytkownicyRequest $request)
    {

        $getUser = User::where('email',$request->input('email'))->count();
        if($getUser == 0) {
            $uzytkownicy = new User();
            $uzytkownicy->name = $request->input('name');
            $uzytkownicy->email = $request->input('email');
            $uzytkownicy->stan = $request->input('stan');
            $uzytkownicy->password = bcrypt($request->input('password'));
            $uzytkownicy->telefon = $request->input('telefon');
            $uzytkownicy->id_rola = $request->input('id_rola');
            $uzytkownicy->id_podmiot = $request->input('id_podmiot');
        
            $uzytkownicy->save();
            Session::flash('status', 'Zapisano pomyślnie.');


            $profil = new Profil();
            $profil->id_specjalizacje = $request->input('id_specjalizacje');
            $profil->id_user = $uzytkownicy->id;
            $profil->id_miasta = $request->input('id_miasta');
            $profil->szukam = $request->input('szukam');
            $profil->lat = $request->input('lat');
            $profil->lon = $request->input('lon');
            $profil->o_mnie = '';
            $profil->save();

            // 'o_mnie','id_specjalizacje','id_user','lat','lon','szukam', 'id_miasta'
            $email = trim($uzytkownicy->email);
            $konto = array(
                'name' =>  $uzytkownicy->name,
                'email' => $email,
                'stan' => '0'
            );
            
            $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
            $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
            $getUstawienia = Ustawienia::where('instancja', 'mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
                    $message->from(env('MAIL_USERNAME'), $temat);
                    $message->to($email)->subject($temat);
                });
                } catch (\Exception $e) {
                    return false;
                }
            }

            return redirect()->back();
        } else {
            Session::flash('status_fail', 'Istnieje już użytkownik o podanym adresie e-mail');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wojewodztwo = Wojewodztwo::get();
        $uzytkownicy = User::where('id', $id)->join('profil','profil.id_user','=','users.id')->first();

        $podmioty = Podmiot::get();
        return view('backend.uzytkownicy.edit')->with('uzytkownicy', $uzytkownicy)->with('wojewodztwa',$wojewodztwo)->with('podmioty',$podmioty);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UzytkownicyRequest $request, $id)
    {
        $uzytkownicy = User::where('id', $id);
        if(!empty($request->input('password'))) {

            

            if($request->input('password') == $request->input('password_repeat')) {
                $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'stan' => $request->input('stan'),
            'telefon' => $request->input('telefon'),
            'id_rola' => $request->input('id_rola'),
            'id_podmiot' => $request->input('id_podmiot'),
            ]);
            Session::flash('status', 'Zapisano pomyślnie.');

                $profil = Profil::where('id_user',$id)->update([ 
        'id_specjalizacje' => $request->input('id_specjalizacje'),
        'id_user' => $id,
        'id_miasta' => $request->input('id_miasta'),
        'szukam' => $request->input('szukam'),
        'lat' => $request->input('lat'),
        'lon' => $request->input('lon'),
        'o_mnie' => '',
                ]);

            } else {
              Session::flash('status_fail', 'Hasła do siebie nie pasują');
            }
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'stan' => $request->input('stan'),
            'telefon' => $request->input('telefon'),
            'id_podmiot' => $request->input('id_podmiot'),
            'id_rola' => $request->input('id_rola'),
            ]);


                   $profil = Profil::where('id_user',$id)->update([ 
        'id_specjalizacje' => $request->input('id_specjalizacje'),
        'id_user' => $id,
        'id_miasta' => $request->input('id_miasta'),
        'szukam' => $request->input('szukam'),
        'lat' => $request->input('lat'),
        'lon' => $request->input('lon'),
        'o_mnie' => '',
                ]);
             Session::flash('status', 'Zapisano pomyślnie.');
        }
           

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $uzytkownicy = User::where('id', $id);
        $uzytkownicy->delete();

        $profil = Profil::where('id_user',$id);
        $profil->delete();

        $ogloszenia = Ogloszenie::where('id_user',$id);
        $ogloszenia->delete();

        $edukacja = Edukacja::where('id_user',$id);
        $edukacja->delete();

        $doswiadczenie = Doswiadczenie::where('id_user',$id);
        $doswiadczenie->delete();

        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new User()));
    }
}
