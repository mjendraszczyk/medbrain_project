<?php

namespace App\Http\Controllers\backend;

use App\Faq;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\FaqRequest;

use Illuminate\Support\Facades\DB;


class FaqController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Faq = Faq::paginate($this->limit);
        return view('backend.faq.index')->with('items',$Faq);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request)
    {

        $Faq = new Faq();
        $Faq->insert([
          'nazwa' => $request->get('nazwa'),
          'opis' => $request->get('opis'),
        ]);
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Faq = Faq::where('id_faq', $id)->first();

        return view('backend.faq.edit')->with('faq', $Faq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqRequest $request, $id)
    {
        $Faq = Faq::where('id_faq', $id);
        $Faq->update([
        'nazwa' => $request->get('nazwa'),
        'opis' => $request->get('opis'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Faq = Faq::where('id_faq', $id);
        $Faq->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Faq()));
    }
}
