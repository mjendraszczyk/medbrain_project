<?php

namespace App\Http\Controllers\backend;

use App\Tytul;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\TytulyRequest;
use Illuminate\Support\Facades\DB;


class TytulyController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tytuly = Tytul::paginate($this->limit);
        return view('backend.tytuly.index')->with('items',$tytuly);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.tytuly.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TytulyRequest $request)
    {
        $tytuly = new Tytul();
        $tytuly->nazwa = $request->input('nazwa');
        $tytuly->save();
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tytuly = Tytul::where('id_tytuly', $id)->first();

        return view('backend.tytuly.edit')->with('tytuly', $tytuly);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TytulyRequest $request, $id)
    {
        $tytuly = Tytul::where('id_tytuly', $id);
        $tytuly->update([
        'nazwa' => $request->input('nazwa'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tytuly = Tytul::where('id_tytuly', $id);
        $tytuly->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Tytul()));
    }
}
