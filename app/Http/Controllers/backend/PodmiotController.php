<?php

namespace App\Http\Controllers\backend;

use App\Podmiot;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\PodmiotRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Image;
use Illuminate\Support\Facades\Input;

use App\RodzajPlacowki;
use App\Profil;
use App\Ustawienia;
use Illuminate\Support\Facades\Mail;

class PodmiotController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podmioty = Podmiot::paginate($this->limit);
        return view('backend.podmiot.index')->with('items',$podmioty);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getSpecjalizacje = (new Specjalizacja())->get();
        $getWojewodztwa = (new Wojewodztwo())->get();
        $getRodzajePlacowki = (new RodzajPlacowki())->get();

        return view('backend.podmiot.create')->with('specjalizacje', $getSpecjalizacje)->with('wojewodztwa', $getWojewodztwa)->with('rodzaje_placowki',$getRodzajePlacowki);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PodmiotRequest $request)
    {
        $podmiot =  Podmiot::create([
        'nazwa' => $request->input('nazwa'),
        'ulica' => $request->input('ulica'),
        'id_miasta' => $request->input('id_miasta'),
        'kod_pocztowy' => $request->input('kod_pocztowy'),
        'lat' => $request->input('lat'),
        'lon' => $request->input('lon'),
        'nip' => $request->input('nip'),
        'podmiot_telefon' => $request->input('podmiot_telefon'),
        'podmiot_email' => $request->input('podmiot_email'),
        'id_rodzaj_placowki' => $request->input('id_rodzaj_placowki'),
        // $podmiot->save();
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');


          //if(Auth::check() == false) {
                $user = User::create([
                'name' => $request->input('nazwa'),
                'email' => $request->input('podmiot_email'),
                'password' => Hash::make(time()),
                'stan' => '0',
                'id_rola' => 1,
                'id_podmiot' => $podmiot->id_podmiot
            ]);

            Profil::create([
                'o_mnie' => null,
                'id_specjalizacje' => null,
                'id_user' => $user->id,
                'szukam' => 0
            ]);

            $konto = array(
                'name' =>  $request->input('nazwa'),
                'email' => $request->input('podmiot_email'),
                'stan' => '0'
            );
                $email = trim($user->email);
                // echo "mail".$email;
                // exit();
                $temat = 'Utworzenie konta w serwisie '.env('APP_NAME');
                $data = array('temat'=>$temat, 'email'=>$email, 'konto'=>$konto);
                 
                  $getUstawienia = Ustawienia::where('instancja','mails')->first();
            if ($getUstawienia->wartosc == '1') {
                try {
                    Mail::send('mails.rejestracja', $data, function ($message) use ($temat, $email) {
                        $message->from(env('MAIL_USERNAME'), $temat);
                        $message->to($email)->subject($temat);
                    });
                } catch (\Exception $e) {
                    return false;
                }
            }
                 

            //}

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id)->first();
        $getWojewodztwa = (new Wojewodztwo())->get();
        $getRodzajePlacowki = (new RodzajPlacowki())->get();
        return view('backend.podmiot.edit')->with('podmiot', $podmiot)->with('wojewodztwa', $getWojewodztwa)->with('rodzaje_placowki',$getRodzajePlacowki);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PodmiotRequest $request, $id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id);
        $podmiot->update([
        'nazwa' => $request->input('nazwa'),
        'ulica' => $request->input('ulica'),
        'id_miasta' => $request->input('id_miasta'),
        'kod_pocztowy' => $request->input('kod_pocztowy'),
        'lat' => $request->input('lat'),
        'lon' => $request->input('lon'),
        'nip' => $request->input('nip'),
        'podmiot_telefon' => $request->input('podmiot_telefon'),
        'podmiot_email' => $request->input('podmiot_email'),
        'id_rodzaj_placowki' => $request->input('id_rodzaj_placowki'),
        ]);

            if ((Input::file('logo_upload'))) {
               $upload = new Controller();
                $upload->upload('logo_upload', 'jpg,jpeg,png', '/img/logo/', $request->get('logo_upload'), $request, 200, 200);
                $podmiot->update([
                'logo_upload' => $upload->getFileName()
            ]);
            }
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $podmiot = Podmiot::where('id_podmiot', $id);
        $podmiot->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
 
        return $this->filterCore($request, (new Podmiot()));
            // $columns = (new Podmiot())->getFillable();
            // $podmioty = Podmiot::where('id_podmiot', '>', 0);
            // foreach ($columns as $column) {
            //     // Session::put($column, '');
            //     if (($request->get($column) != '') || (Session::get($column) != '')) {
                    
            //         if (Session::get($column) == '') {
            //             Session::put($column, $request->get($column));
            //             $podmioty->where($column, 'LIKE', '%'.$request->get($column).'%');
            //         } else{
            //             $podmioty->where($column, 'LIKE', '%'.Session::get($column).'%');
            //         }
            //         // exit('f');
            //     }
            // }
            // if ($request->has('reset_filter')) {
            //     foreach ($columns as $column) {
            //         Session::put($column, '');
            //     }
            //     return redirect()->route('backend_podmiot_index');
            // } else {
            //     return view('backend.podmiot.index')->with('items', $podmioty->paginate($this->limit));
            // }
    }
}
