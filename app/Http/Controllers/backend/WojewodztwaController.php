<?php

namespace App\Http\Controllers\backend;

use App\Kraj;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests\backend\WojewodztwaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class WojewodztwaController extends Controller
{
    // public $limit;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wojewodztwa = Wojewodztwo::paginate($this->limit);
        return view('backend.wojewodztwa.index')->with('items',$wojewodztwa);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kraje = Kraj::get();

        return view('backend.wojewodztwa.create')->with('kraje',$kraje);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WojewodztwaRequest $request)
    {
        $wojewodztwa = new Wojewodztwo();
        $wojewodztwa->nazwa = $request->input('nazwa');
        $wojewodztwa->id_kraje = $request->input('id_kraje');
        $wojewodztwa->tresc = $request->input('tresc');
            Session::flash('status', 'Zapisano pomyślnie.');
        $wojewodztwa->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wojewodztwa = Wojewodztwo::where('id_wojewodztwa', $id)->first();

        return view('backend.wojewodztwa.edit')->with('wojewodztwa', $wojewodztwa)->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WojewodztwaRequest $request, $id)
    {
        $wojewodztwa = Wojewodztwo::where('id_wojewodztwa', $id);
        $wojewodztwa->update([
        'nazwa' => $request->input('nazwa'),
        'id_kraje' => $request->input('id_kraje'),
        'tresc' => $request->input('tresc'),
        ]);
            Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wojewodztwa = Wojewodztwo::where('id_wojewodztwa', $id);
        $wojewodztwa->delete();
        return redirect()->back();
    }
    public function filter(Request $request) {//Request $request
        
        return $this->filterCore($request, (new Wojewodztwo()));
    }
}
