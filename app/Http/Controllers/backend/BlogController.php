<?php

namespace App\Http\Controllers\backend;


use App\Blog;
use App\Specjalizacja;
use App\Wojewodztwo;
use Session;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\BlogRequest;

use Illuminate\Support\Facades\DB;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::paginate($this->limit);
        return view('backend.blog.index')->with('items', $blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        $blog = new Blog();

        $upload = new Controller();
        if ((Input::file('blog_upload'))) {
            $upload->upload('blog_upload', 'jpg,jpeg,png', '/img/blog/', $request->get('blog_upload'), $request, 1980, 720);
            $blog->insert([
                'tytul' => $request->get('tytul'),
                'tresc' => $request->get('tresc'),
                'image' => $upload->getFileName()
            ]);
            
        } else {
            $blog->insert([
                'tytul' => $request->get('tytul'),
                'tresc' => $request->get('tresc'),
            ]); 
        }

        $id_blog = DB::getPdo()->lastInsertId();;
        Session::flash('status', 'Dodano pomyślnie.');


        return redirect()->route('backend_blog_edit',['id'=>$id_blog]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::where('id_blog', $id)->first();

        return view('backend.blog.edit')->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::where('id_blog', $id);
        $upload = new Controller();
        if ((Input::file('blog_upload'))) {
            $upload->upload('blog_upload', 'jpg,jpeg,png', '/img/blog/', $request->get('blog_upload'), $request, 1980, 720);
            $blog->update([
                'tytul' => $request->get('tytul'),
                'tresc' => $request->get('tresc'),
                'image' => $upload->getFileName()
            ]);
        } else {
            $blog->update([
                'tytul' => $request->get('tytul'),
                'tresc' => $request->get('tresc'),
            ]); 
        }

        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::where('id_blog', $id);
        $blog->delete();
        return redirect()->back();
    }
    public function filter(Request $request)
    {
        return $this->filterCore($request, (new Blog()));
    }
}
