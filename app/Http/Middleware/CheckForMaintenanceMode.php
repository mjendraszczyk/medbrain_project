<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Route;

class CheckForMaintenanceMode extends Middleware
{
  
    protected $request;
    protected $app;

    public function __construct(Application $app, Request $request)
    {
        $this->app = $app;
        $this->request = $request;
    }

    public function handle($request, Closure $next)
    {
        //echo "dsf". $request->is('backoffice/*');
        if (($this->app->isDownForMaintenance())) {
        if($request->is('backoffice/*') || $request->is('backoffice') || $request->is('login/*') || $request->is('login') || $request->is('konto/*') || $request->is('logout'))  {
            
        } else {
            return response()->view('errors.maintenance',[], 503);
        }
        }
        return $next($request);
    }
}
