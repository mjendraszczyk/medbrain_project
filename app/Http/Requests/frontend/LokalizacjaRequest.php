<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class LokalizacjaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alias' => 'required',
            'ulica' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'id_rodzaj_placowki' => 'required',
            // 'id_user' => 'required',
            // 'id_miasta' => 'required',
        ];
    }
}
