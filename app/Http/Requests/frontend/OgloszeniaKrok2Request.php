<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class OgloszeniaKrok2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'wynagrodzenie_od' => 'required|numeric',
            //'wynagrodzenie_do' => 'required|numeric',
            'specjalizacja' => 'required',
            'typ_umowy' => 'required',
            'typ_wynagrodzenia' => 'required',
            'waluta_wynagrodzenia' => 'required',
            'doswiadczenie_od' => 'nullable:numeric',
            'doswiadczenie_do' => 'nullable:numeric',
            'wymiar_pracy' => 'required',
            // 'poziom' => 'required',
            // 'tresc' => 'required',
            // 'szukam' => 'required',
        ];
    }
}
