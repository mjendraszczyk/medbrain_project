<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class OgloszeniaKrok4Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'imie_nazwisko' => 'required|min:3|string',
            //'adres_email' => 'required|email',
            'miasto' => 'required',
            'id_rodzaj_placowki' => 'required',
            'ulica' => 'required|min:3',
            'telefon' => 'required|numeric',
            'podmiot' => 'required|min:3',
            'lat' => 'required',
            'lon' => 'required',
            //'avatar_upload' => 'file',
            //'nip' => 'required|numeric|',
        ];
    }
}
