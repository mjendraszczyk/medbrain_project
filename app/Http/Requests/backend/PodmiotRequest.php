<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class PodmiotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nazwa' => 'required',
            'ulica' => 'required',
            'id_miasta' => 'required',
            'kod_pocztowy' => 'required',
            'lat' => 'required',
            'lon' => 'required',
            'nip' => 'numeric|nullable',
            'podmiot_telefon' => 'required',
            'id_rodzaj_placowki' => 'required',
            'podmiot_email' => 'required|email',
        ];
 
    }
}
