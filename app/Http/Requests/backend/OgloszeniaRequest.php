<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class OgloszeniaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_specjalizacje' => 'required',
            'id_wymiar_pracy' => 'required',
            'id_poziom' => 'required',
            'doswiadczenie_od' => 'nullable:numeric',
            'doswiadczenie_do' => 'nullable:numeric',
            'id_podmiot' => 'required',
            'typ_wynagrodzenia' => 'required',
            'waluta_wynagrodzenia' => 'required',
            'id_rodzaj_umowy' => 'required',
        ];
    }
}
