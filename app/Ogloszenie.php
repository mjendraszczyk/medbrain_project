<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ogloszenie extends Model
{
    protected $primaryKey = 'id_ogloszenia';
    public $incrementing = true;
    protected $table = 'ogloszenia';
    protected $fillable = [
        'imie_nazwisko', 'email', 'id_user', 'id_specjalizacje', 'id_wymiar_pracy', 'id_doswiadczenie', 'wynagrodzenie_od', 'wynagrodzenie_do', 'tresc', 'podmiot', 'id_podmiot', 'typ_wynagrodzenia',
        'waluta_wynagrodzenia', 'id_lokalizacja','rekruter'
    ];
}
