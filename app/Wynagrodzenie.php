<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wynagrodzenie extends Model
{
 
    protected $primaryKey = 'id_wynagrodzenie';
    public $incrementing = true;
    protected $table = 'wynagrodzenie';
     protected $fillable = [
        'nazwa'
    ];
}
