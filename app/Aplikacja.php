<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplikacja extends Model
{
    protected $primaryKey = 'id_aplikacje';
    public $incrementing = true;
    protected $table = 'aplikacje';
     protected $fillable = [
        'id_user', 'tresc', 'email','imie_nazwisko','zalacznik','id_ogloszenia'
    ];
}
