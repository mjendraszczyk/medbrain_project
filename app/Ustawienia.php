<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ustawienia extends Model
{
    protected $primaryKey = 'id_ustawienia';
    
    public $incrementing = true;
    protected $table = 'ustawienia';
     protected $fillable = [
        'instancja','wartosc'
    ];
}
