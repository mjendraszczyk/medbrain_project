<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokalizacja extends Model
{
           protected $primaryKey = 'id_lokalizacja';
    public $incrementing = true;
    protected $table = 'lokalizacja';
     protected $fillable = [
        'alias',
        'id_user',
        'lat',
        'lon',
        'ulica',
        'id_miasta',
        'id_rodzaj_placowki'
    ];
}
