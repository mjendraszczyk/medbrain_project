<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $primaryKey = 'id_blog';
    public $incrementing = true;
    protected $table = 'blog';
     protected $fillable = [
        'tytul', 'tresc', 'image'
    ];
}
