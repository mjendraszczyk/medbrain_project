<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Podmiot extends Model
{
    protected $primaryKey = 'id_podmiot';
    public $incrementing = true;
    protected $table = 'podmiot';
    protected $fillable = [
        'nazwa', 'ulica', 'id_miasta', 'kod_pocztowy', 'lat', 'lon', 'nip','podmiot_telefon', 'podmiot_email','id_rodzaj_placowki'
    ];
}
