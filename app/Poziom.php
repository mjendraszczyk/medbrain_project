<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poziom extends Model
{
    protected $primaryKey = 'id_poziom';
    public $incrementing = true;
    protected $table = 'poziom';
     protected $fillable = [
        'nazwa'
    ];
}
