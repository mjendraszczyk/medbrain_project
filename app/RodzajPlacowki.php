<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RodzajPlacowki extends Model
{

    protected $primaryKey = 'id_rodzaj_placowki';
    public $incrementing = true;
    protected $table = 'rodzaj_placowki';
         protected $fillable = [
        'id_rodzaj_placowki','nazwa'
    ];
}
