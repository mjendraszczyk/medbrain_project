<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platnosci extends Model
{
    protected $primaryKey = 'id_platnosci';
    public $incrementing = true;
    protected $table = 'platnosci';
    protected $fillable = [
        'id_transakcji', 'data', 'kwota', 'status', 'id_podmiot'
    ];
}
