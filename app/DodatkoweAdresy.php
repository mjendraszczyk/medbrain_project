<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DodatkoweAdresy extends Model
{
    protected $primaryKey = 'id_dodatkowe_adresy';
    public $incrementing = true;
    protected $table = 'dodatkowe_adresy';
    protected $fillable = [
        'nazwa', 'lat_dodatkowy_adres', 'lon_dodatkowy_adres','id_ogloszenia'
    ];
}
