<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wojewodztwo extends Model
{
    protected $primaryKey = 'id_wojewodztwa';
    
    public $incrementing = true;
    protected $table = 'wojewodztwa';
         protected $fillable = [
        'nazwa',
        'id_kraje', 
        'tresc'
    ];
}
