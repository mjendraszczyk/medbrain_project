<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doswiadczenie extends Model
{
    protected $primaryKey = 'id_doswiadczenie';
    public $incrementing = true;
    protected $table = 'doswiadczenie';
     protected $fillable = [
        'nazwa','doswiadczenie_lata_od','doswiadczenie_lata_do','id_user'
    ];
}
