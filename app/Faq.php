<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $primaryKey = 'id_faq';
    public $incrementing = true;
    protected $table = 'faq';
     protected $fillable = [
        'nazwa','opis'
    ];
}
