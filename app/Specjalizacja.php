<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specjalizacja extends Model
{
    protected $primaryKey = 'id_specjalizacje';
    public $incrementing = true;
    protected $table = 'specjalizacje';
     protected $fillable = [
        'nazwa', 'kolor', 'symbol'
    ];
}
