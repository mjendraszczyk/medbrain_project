<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WymiarPracy extends Model
{
protected $primaryKey = 'id_wymiar_pracy';

    public $incrementing = true;
    protected $table = 'wymiar_pracy';
    protected $fillable = [
        'nazwa'
    ];
}
