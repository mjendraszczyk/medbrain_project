<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecjalizacjeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specjalizacje', function (Blueprint $table) {
            $table->bigIncrements('id_specjalizacje');
            $table->string('nazwa');
            $table->string('kolor');
            $table->string('symbol');
             $table->text('tresc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specjalizacje');
    }
}
