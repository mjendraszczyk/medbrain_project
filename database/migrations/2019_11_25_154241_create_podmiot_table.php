<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePodmiotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podmiot', function (Blueprint $table) {
            $table->bigIncrements('id_podmiot');
            $table->string('nazwa');
            $table->string('ulica')->nullable();
            $table->integer('id_miasta');
            $table->string('kod_pocztowy')->nullable();
            $table->float('lat', 128, 30)->nullable();
            $table->float('lon', 128, 30)->nullable();
            $table->string('nip')->nullable();
            $table->string('podmiot_telefon')->nullable();
            $table->string('podmiot_email')->nullable();
            $table->string('logo_upload')->nullable();
            $table->integer('id_rodzaj_placowki')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podmiot');
    }
}
