<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLokalizacjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokalizacja', function (Blueprint $table) {
            $table->bigIncrements('id_lokalizacja');
            $table->string('ulica');
            $table->int('id_rodzaj_placowki')->nullable();
            $table->int('id_miasta');
            $table->float('lat', 128, 30)->nullable();
            $table->float('lon', 128, 30)->nullable();
            $table->string('alias');
            $table->int('id_user');
            $table->string('kontakt_email');
            $table->string('kontakt_telefon');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lokalizacja');
    }
}
