<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplikacjeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aplikacje', function (Blueprint $table) {
            $table->bigIncrements('id_aplikacje');
            $table->integer('id_user')->nullable();
            $table->integer('id_ogloszenia');
            $table->text('tresc')->nullable();
            $table->string('email')->nullable();
            $table->string('imie_nazwisko')->nullable();
            $table->string('zalacznik');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aplikacje');
    }
}
