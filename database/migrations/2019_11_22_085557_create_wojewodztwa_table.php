<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWojewodztwaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wojewodztwa', function (Blueprint $table) {
            $table->bigIncrements('id_wojewodztwa');
            $table->string('nazwa');
            $table->integer('id_distinct')->nullable();
            $table->integer('id_kraje');
            $table->text('tresc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wojewodztwa');
    }
}
