<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOgloszeniaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ogloszenia', function (Blueprint $table) {
            $table->bigIncrements('id_ogloszenia');
            $table->string('imie_nazwisko')->nullable();
            $table->string('email')->nullable();
            $table->integer('id_user')->nullable();
            $table->integer('id_specjalizacje');
            $table->integer('id_wymiar_pracy');
            $table->integer('doswiadczenie_od')->nullable();
            $table->integer('doswiadczenie_do')->nullable();
            $table->integer('id_poziom');
            $table->integer('typ_wynagrodzenia')->default('1')->nullable();
            $table->integer('id_rodzaj_umowy');
            $table->float('wynagrodzenie_od')->nullable(); //float
            $table->float('wynagrodzenie_do')->nullable(); //float
            $table->text('tresc')->nullable();
            $table->integer('id_podmiot')->nullable();
            $table->string('waluta')->nullable();
            $table->string('id_lokalizacja')->nullable();
            $table->string('rekruter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ogloszenia');
    }
}
