<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDodatkoweAdresy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dodatkowe_adresy', function (Blueprint $table) {
            $table->bigIncrements('id_dodatkowe_adresy');
            $table->string('nazwa')->nullable();
            $table->float('lat_dodatkowy_adres');
            $table->float('lon_dodatkowy_adres');
            $table->integer('id_ogloszenia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dodatkowe_adresy');
    }
}
