<?php

use Illuminate\Database\Seeder;
use App\Specjalizacja;
use App\WymiarPracy;
// use App\Wynagrodzenie;
use App\Poziom;
use App\Doswiadczenie;
use App\Wojewodztwo;
use App\User;
use App\Podmiot;
use App\Ogloszenie;
use App\Kraj;
use App\Miasto;
use App\RodzajPlacowki;
use App\RodzajUmowy;
use App\TypOgloszenia;
use App\Tytul;
use App\Rola;
use App\Profil;

use App\Ustawienia;
use App\Cms;
use App\Faq;

use mrcnpdlk\Teryt\Client;
use mrcnpdlk\Teryt\NativeApi;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        /**
         * Clear all data
         */
        Miasto::truncate();
        Specjalizacja::truncate();
        WymiarPracy::truncate();
        Doswiadczenie::truncate();
        Wojewodztwo::truncate();
        User::truncate();
        Poziom::truncate();
        // Wynagrodzenie::truncate();
        Podmiot::truncate();
        Ogloszenie::truncate();
        Kraj::truncate();
        
        RodzajPlacowki::truncate();
        TypOgloszenia::truncate();
        RodzajUmowy::truncate();

        /**
         * Kraj
         */
        $kraj = new Kraj();
        $kraj->nazwa = 'Polska';
        $kraj->save();

        /**
         * Poziom
         */
        $poziomy = [
            'Asystent',
            'Pracownik',
            'Specjalista',
            'Dyrektor',
            'Kierownik',
            'Stażysta'
        ];

        foreach($poziomy as $poziom) { 
            $p = new Poziom();
            $p->nazwa = $poziom;
            $p->save();
        }

        /**
         * Wynagrodzenie
         */
        //  $wynagrodzenia = [
        //     '1000',
        //     '2000',
        //     '3000',
        //     '4000',
        //     '5000',
        //     '6000',
        //     '7000',
        //     '8000',
        //     '9000',
        //     '10000',
        //     '12000',
        //     '15000',
        //     '20000',
        //  ];

        //  foreach($wynagrodzenia as $wynagrodzenie) { 
        //     $w = new Wynagrodzenie();
        //     $w->nazwa = $wynagrodzenie;
        //     $w->save();
        // }

 
 
        $kolory = [
        'bc208b',
        '2b5cb5',
        'f12b12',
        'fcbad3',
        'ffbe53',
        '4a5158',
        '20648e',
        '76c6dd',
        'f38081',
        '8361d0',
        '76dadd',
        '667785',
        'f12b12',
        '32b128',
        'a1e6a5'
        ];
        /* Specjalizacje */
        $specjalizacje_tab = [
            [
            'nazwa'=>'Anestezjologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'A',
            ],
            [
            'nazwa'=>'Seksuologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'S',
            ],
            [
            'nazwa'=>'Pulmunologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PL',
            ],
            [
            'nazwa'=>'Onkologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'ON',
            ],
            [
            'nazwa'=>'Okulistyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'OK',
            ],
            [
            'nazwa'=>'Medycyna pracy',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'MP',
            ],
            [
            'nazwa'=>'Gastrologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'GA',
            ],
            [
            'nazwa'=>'Endokrynologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'EK',
            ],
            [
            'nazwa'=>'Diagnostyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DL',
            ],
            [
            'nazwa'=>'Diabetologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DIA',
            ],
            [
            'nazwa'=>'Choroby zakaźne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'CHZ',
            ],
            [
            'nazwa'=>'Choroby wewnętrzne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'CHW',
            ],
            [
            'nazwa'=>'Alergologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'AL',
            ],
            [
            'nazwa'=>'Ortopedia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'O',
            ],
            [
            'nazwa'=>'Kardiologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'K',
            ],
            [
            'nazwa'=>'Dietetyka',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DT',
            ],
            [
            'nazwa'=>'Stomatologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'S',
            ],
            [
            'nazwa'=>'Neurologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'N',
            ],
            [
            'nazwa'=>'Urologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'U',
            ],
            [
            'nazwa'=>'Psychologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PS',
            ],
            [
            'nazwa'=>'Psychiatria',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PSC',
            ],
            [
            'nazwa'=>'Laryngologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'LR',
            ],
            [
            'nazwa'=>'Chirurgia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'C',
            ],
            [
            'nazwa'=>'Neurochirurgia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'NC',
            ],
            [
            'nazwa'=>'Radiologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'R',
            ],
            [
            'nazwa'=>'Pielęgniarstwo',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PL',
            ],
            [
            'nazwa'=>'Pediatria',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'PD',
            ],
            [
            'nazwa'=>'Dermatologia',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'DR',
            ],
            [
            'nazwa'=>'Inne',
            'kolor'=>$kolory[rand(0,count($kolory)-1)],
            'symbol'=>'I',
            ]
        ];
        foreach($specjalizacje_tab as $specjalizacja) {
            $specjalizacje = new Specjalizacja();
            $specjalizacje->nazwa = $specjalizacja['nazwa'];
            $specjalizacje->kolor = $specjalizacja['kolor'];
            $specjalizacje->symbol = $specjalizacja['symbol'];
            $specjalizacje->save();
        }

        /* Wymiar pracy */
        $wymiary_pracy = [
            'pełny etat',
            '1/2 etatu',
            '3/4 etatu',
            'inny',
        ];

        foreach($wymiary_pracy as $wymiar) {
            $wymiary = new WymiarPracy();
            $wymiary->nazwa = $wymiar;
            $wymiary->save();
        }

        /* Uzytkownicy */

        $uzytkownicy = [
            [
                'name' => 'Adam Kowalski',
                'email' => 'example0@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Radosław Budzyński',
                'email' => 'example1@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Tomasz Nowak',
                'email' => 'example2@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Wojciech Wybicki',
                'email' => 'example3@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Kazimierz Staropolski',
                'email' => 'example4@example.com',
                'password' => '1',
                'stan' => '0',
            ],
            [
                'name' => 'Pior Piotrkowski',
                'email' => 'example5@example.com',
                'password' => '1',
                'stan' => '0',
            ]

            ];

            $lat=53;
            $lon=14;
            $i=0;
        foreach($uzytkownicy as $key => $uzytkownik) {
            $i=$i+0.5;
            $u = new User();
            $u->name = $uzytkownik['name'];
            $u->email = $uzytkownik['email'];
            $u->password = bcrypt($uzytkownik['password']);
            $u->stan = $uzytkownik['stan'];
            $u->id_podmiot = ($key+1);
            if ($key == 0) {
                $u->id_rola = 3;
            } else {
                $u->id_rola = 1;
            }
            $u->save();

            $profil = new Profil();
            $profil->id_user = $u->id;
            $profil->szukam = 1;
            $profil->lat = $lat+$i;
            $profil->lon = $lon+$i;
            $profil->save();
        }


        /**
        *  Podmioty 
        */
            //'nazwa', 'ulica', 'id_miasta', 'id_wojewodztwo', 'id_kraj', 'kod_pocztowy', 'lat', 'lon', 'nip','telefon', 'email'
     $podmioty = [
         [
             'nazwa' => 'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Unii Lubelskiej 12',
             'id_miasta' => '938',
             'id_wojewodztwo' => '32',
             'id_kraj' => '1',
             'kod_pocztowy' => '71-001',
             'lat' => '53.4507381',
             'lon' => '14.50514',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.szczecin.pl'
         ],
         [
             'nazwa' => 'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Unii Lubelskiej 12',
             'id_miasta' => '91',
             'id_wojewodztwo' => '2',
             'id_kraj' => '1',
             'kod_pocztowy' => '53-001',
             'lat' => '51.1271646',
             'lon' => '16.9216529',
             'nip' => '8510815059',
             'telefon' =>'123 123 123',
             'email' => 'adres@domena.wroclaw.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Przykladowa 1',
             'id_miasta' => '873',
             'id_wojewodztwo' => '30',
             'id_kraj' => '1',
             'kod_pocztowy' => '64-001',
             'lat' => '52.4006548',
             'lon' => '16.7612409',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.poznan.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Spokojna 9',
             'id_miasta' => '593',
             'id_wojewodztwo' => '22',
             'id_kraj' => '1',
             'kod_pocztowy' => '43-001',
             'lat' => '54.3612059',
             'lon' => '18.5496024',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.gdansk.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Szpitalna 20',
             'id_miasta' => '338',
             'id_wojewodztwo' => '12',
             'id_kraj' => '1',
             'kod_pocztowy' => '34-001',
             'lat' => '50.0468547',
             'lon' => '19.934662',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.krakow.pl'
         ],
         [
             'nazwa' =>'Publiczny Szpital Kliniczny',
             'ulica' => 'ul. Kliniczna 2',
             'id_miasta' =>  '940',
             'id_wojewodztwo' => '14',
             'id_kraj' => '1',
             'kod_pocztowy' => '00-001',
             'lat' => '52.2330649',
             'lon' => '20.9207691',
             'nip' => '8510815059',
             'telefon' => '123 123 123',
             'email' => 'adres@domena.warszawa.pl'
         ],
    ];

         foreach($podmioty as $podmiot) { 
            $p = new Podmiot();
            $p->nazwa = $podmiot['nazwa'];
            $p->ulica = $podmiot['ulica'];
            $p->id_miasta = $podmiot['id_miasta'];
            $p->kod_pocztowy = $podmiot['kod_pocztowy'];
            $p->lat = $podmiot['lat'];
            $p->lon = $podmiot['lon'];
            $p->nip = $podmiot['nip'];
            $p->podmiot_telefon = $podmiot['telefon'];
            $p->podmiot_email = $podmiot['email'];
            $p->id_rodzaj_placowki = rand(1,3);
            $p->save();

         }

         /**
          * Typ ogloszenia
          */
        $szukam = [
        'Pracownika',
        'Pracodawcy'
        ];
        foreach($szukam as $sz) {
            $typ_ogloszenia = new TypOgloszenia();
            $typ_ogloszenia->nazwa = $sz;
            $typ_ogloszenia->save();
        }
         /**
          * Rodzaj umowy
          */
          $rodzaje_umowy = [
            'Umowa o pracę',
            'Umowa zlecenie',
            'Umowa o dzieło',
            'B2B',
            'Inna'
          ];

          foreach($rodzaje_umowy as $rodzaj) {
            $um = new RodzajUmowy();
            $um->nazwa = $rodzaj;
            $um->save();
          }


         /**
          * Ogloszenia 
          */
          for($i=1;$i<=6;$i++) {
              //$getRandomSpecjalizacja = Specjalizacja::all()->random(1);
            $o = new Ogloszenie();
            $o->email = $podmioty[rand(0,count($podmioty)-1)]['email'];
            $o->imie_nazwisko = $uzytkownicy[rand(0,count($uzytkownicy)-1)]['name'];
            $o->id_user = $i;
            $o->id_wymiar_pracy = rand(1,4);
            $o->id_specjalizacje = rand(1,20);
            $o->doswiadczenie_od = rand(1,4);
            $o->doswiadczenie_do = rand(5,20);
            $o->id_podmiot = $i;
            $o->id_poziom = rand(1,6);
            $o->wynagrodzenie_od = rand(1000,4000);
            $o->wynagrodzenie_do = rand(4100,15000);
            //$o->id_typ_ogloszenia = 1;//rand(1,2);
            $o->id_rodzaj_umowy = rand(1,5);//rand(1,2);
            $o->tresc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis metus sit amet dui euismod ornare ut at tortor. Integer id metus egestas, bibendum diam eu, convallis tortor. Donec vel urna nec lectus pulvinar semper at sed ante. Nunc gravida ac leo non rutrum. Phasellus in fringilla elit. Nullam massa diam, rutrum et purus ac, lobortis hendrerit ligula. Nulla interdum sed leo hendrerit imperdiet. Sed sit amet suscipit nisl. Suspendisse porta condimentum ante vitae venenatis. Aenean a augue erat. Integer eleifend varius risus eget aliquam. Donec tellus ante, vehicula et gravida nec, posuere tempus lacus. Nam fermentum tempus diam. <br/><br/>Fusce convallis nunc elementum, blandit neque rutrum, tristique tellus. Quisque justo massa, dignissim in ex id, mattis mollis libero. Mauris ut tortor nec ex laoreet ultricies. Vivamus molestie justo sit amet neque iaculis, vel rutrum orci convallis. Curabitur pretium ante lorem, non consequat velit sollicitudin vel. Morbi malesuada arcu tortor, et dictum ipsum imperdiet vel. Vivamus feugiat lorem eget porttitor tincidunt. Sed eu tellus vel eros posuere rhoncus eleifend vel enim. Sed erat metus, placerat non nisi ut, feugiat auctor elit. Suspendisse tempus risus est, ac ultrices neque lobortis in. Mauris ac iaculis elit, vitae viverra dui. Etiam faucibus auctor mauris nec varius. Curabitur vel sem vitae magna fermentum laoreet sit amet ac mauris. Aenean porta magna et lorem vestibulum, ut cursus velit rutrum. Vestibulum condimentum, arcu in sollicitudin sollicitudin, nulla est ultrices ante, quis lacinia justo nisl eget nisl.";
            $o->save();
          }
    
    /**
     * Rodzaje placowek
     */
            $tytuly = [
                '',
                'lic.',
                'inż.',
                'mgr',
                'mgr inż.',
                'dr',
                'dr inż.',
                'dr hab.',
                'dr hab. inż.',
                'prof.',
            ];

            foreach($tytuly as $t) {
                    $tytul =  new Tytul();
                    $tytul->nazwa = $t;
                    $tytul->save();
            }



     $placowki = [
        'Klinika',
        'Szpital',
        'Przychodnia',
        'Laboratorium',
     ];

     foreach($placowki as $placowka) {
        $p = new RodzajPlacowki();
        $p->nazwa = $placowka;
        $p->save();
     }

     $role = [
         "Użytkownik",
         "Firma",
         "Administrator"
     ];

     foreach ($role as $rola) {
         $r = new Rola();
         $r->nazwa = $rola;
         $r->save();
     }
 
    $cmsy = [
        [
            'tytul' => 'O SERWISIE',
            'tresc' => '<div class="row white">
<div class="container">
<div class="row">
<div class="col s12 m12 l6">
<div class="padding-box">
<h3 class="title-section"><span class="green-text">Serwis ogłoszeń</span> Medbra.in</h3>
Medbra.in to serwis ogłoszeń o pracę dla pracownik&oacute;w w branży medycznej w tym lekarzy, stomatolog&oacute;w, farmaceut&oacute;w, fizjoterapeut&oacute;w, pielęgniarzy, położnych, psycholog&oacute;w a także personelu wspomagającego, kt&oacute;rego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.</div>
</div>
<div class="col s12 m12 l6 onas1">&nbsp;</div>
</div>
</div>
</div>
<div class="row">
<div class="container">
<div class="col s12 m12 l6 onas2">&nbsp;</div>
<div class="col s12 m12 l6">
<div class="padding-box">
<h3 class="title-section"><span class="green-text">Założenia</span> serwisu</h3>
Medbra.in to serwis ogłoszeń o pracę dla pracownik&oacute;w w branży medycznej w tym lekarzy, stomatolog&oacute;w, farmaceut&oacute;w, fizjoterapeut&oacute;w, pielęgniarzy, położnych, psycholog&oacute;w a także personelu wspomagającego, kt&oacute;rego celem jest ułatwienie pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu spełniającej ich wymagania pracy.</div>
</div>
</div>
</div>'
        ],
        [
            'tytul' => 'Regulamin',
            'tresc' => '<p class="p1"><strong><span class="s1">I. Podstawowe definicje</span></strong></p>
<ol class="ol2">
<li class="li5"><span class="s2"><strong>Medbra.in</strong> - internetowy serwis ogłoszeniowy dostępny w domenie internetowej medbra.in będący platformą ogłoszeniowa umożliwiającą zamieszczanie ogłoszeń dotyczących poszukiwania pracownik&oacute;w oraz pracodawc&oacute;w w branży usług medycznych.</span></li>
<li class="li5"><span class="s2"><strong>Użytkownik</strong> &ndash; osoba fizyczna, kt&oacute;ra ukończyła 18 rok życia, posiadająca pełną zdolność do czynności prawnych, osoba prawna lub jednostka organizacyjna nieposiadająca osobowości prawnej, ale mogąca we własnym imieniu nabywać prawa oraz zaciągać zobowiązania </span></li>
<li class="li5"><span class="s2"><strong>Ogłoszenie </strong>&ndash; strona udostępniona Użytkownikowi, zawierająca dane zamieszczone przez niego w postaci ogłoszenia.</span></li>
<li class="li4"><span class="s2"><strong>Administrator</strong> &ndash; Michał Skwira, adres do doręczeń Kaszubska 34/2, 70 - 227 Szczecin, email: </span><span class="s1">michalskwira@gmail.com</span></li>
<li class="li4"><span class="s2"><strong>Regulamin</strong> &ndash; niniejszy regulamin.</span></li>
</ol>
<p class="p3">&nbsp;</p>
<p class="p1"><span class="s1"><strong>II. Postanowienia og&oacute;lne</strong></span></p>
<p class="p1">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s2">Regulamin określa zasady korzystania z serwisu medbra.in, prawa i obowiązki Użytkownika, a także prawa, obowiązki i zakres odpowiedzialności Administratora.</span></li>
<li class="li5"><span class="s2">Użytkownik z chwilą akceptacji Regulaminu oświadcza, że zapoznał się z jego treścią, wyraża zgodę na jego wszystkie warunki i zobowiązuje się do ich przestrzegania.</span></li>
<li class="li5"><span class="s1">Aby zapewnić możliwość prawidłowego i niezakł&oacute;conego korzystania ze Serwisu i usług świadczonych za jego pośrednictwem, urządzenie, kt&oacute;rym posługuje się Użytkownik, powinno spełniać następujące minimalne wymagania techniczne:</span></li>
</ol>
<ul class="ul1">
<li class="li5"><span class="s1">posiadać połączenie z siecią Internet zapewniające transmisję danych,</span></li>
<li class="li5"><span class="s1">posiadać przeglądarkę internetową, tj. oprogramowanie służące do przeglądania treści dostępnych w sieci Internet. Wyłącznie akceptacji plik&oacute;w Cookies i Skrypt&oacute;w Java może zakł&oacute;cić prawidłowe działanie strony Sklepu,</span></li>
<li class="li5"><span class="s1">aktywne konto poczty e-mail.</span></li>
</ul>
<ol class="ol2">
<li class="li5"><span class="s1">W celu odczytania korespondencji mailowej i załącznik&oacute;w do niej niezbędne jest posiadanie:</span></li>
</ol>
<ul class="ul1">
<li class="li5"><span class="s1">w przypadku korespondencji mailowej - aktywne konto poczty e-mail,</span></li>
<li class="li5"><span class="s1">w przypadku załącznik&oacute;w, w tym w szczeg&oacute;lności plik&oacute;w tekstowych &ndash; oprogramowanie komputerowe obsługujące pliki danego rodzaju, w szczeg&oacute;lności .pdf, .doc., .docx, .txt, .jpg etc.</span></li>
</ul>
<ol class="ol2">
<li class="li5"><span class="s1">Zakres usług świadczonych przez Administratora nie obejmuje dostarczenia urządzeń ani oprogramowania wskazanych w ust. 1.</span></li>
<li class="li5"><span class="s1">Korzystanie z usług świadczonych drogą elektroniczną w ramach Serwisu, z uwagi na transmisję danych za pośrednictwem publicznej sieci Internet, wiąże się z zagrożeniem w postaci możliwości ingerencji nieupoważnionych os&oacute;b trzecich w dane przesyłane między Administratorem a Użytkownikiem.</span></li>
<li class="li5"><span class="s1">Klienta obowiązuje zakaz dostarczania treści o charakterze bezprawnym.</span><span class="s4">&nbsp;</span></li>
</ol>
<p class="p1"><span class="s1"><strong>III. Og&oacute;lne warunki korzystania</strong></span></p>
<p class="p1">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s2">Warunkiem do korzystania z pełni usług świadczonych w Serwisie jest skorzystanie z urządzenia komunikującego się z Internetem wyposażonego w przeglądarkę internetową. W celu dokonania płatności za zakupiony produkt niezbędnym jest posiadanie aktywnego konta PayPal lub PayU - do 31 sierpnia 2020 serwis bezpłatny.</span></li>
<li class="li4"><span class="s2">Użytkownik jest zobowiązany do podawania tylko prawdziwych i aktualnych danych.</span></li>
<li class="li4"><span class="s2">Administrator nie bierze udziału w transakcjach dokonywanych przez Użytkownik&oacute;w i sporach powstałych między Użytkownikami.</span></li>
<li class="li4"><span class="s2">Zawarcie i realizacja transakcji między Użytkownikami oraz wszelkie płatności z tego tytułu odbywają się bezpośrednio pomiędzy Użytkownikami.</span></li>
</ol>
<p class="p1"><span class="s1"><strong>IV. Zamieszczanie ogłoszeń</strong></span></p>
<p class="p1">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s2">Zamieszczanie ogłoszeń na portalu do dnia 31 sierpnia 2020 roku jest bezpłatne. Po dniu 1 września 2020 roku Administrator może wprowadzić odpłatność usługi na zasadach określonych w regulaminie.</span></li>
<li class="li5"><span class="s2">Celem zamieszczenia ogłoszenia konieczne jest utworzenie indywidualnego konta, kt&oacute;re odbywa się poprzez wypełnienie dostępnego na stronie formularza oraz posiadanie aktywnego konta PayPal lub PayU.</span></li>
<li class="li5"><span class="s2">Administrator nie pobiera opłat - wynagrodzenia prowizyjnego w przypadku zawarcia um&oacute;w pomiędzy Użytkownikami. </span></li>
<li class="li5"><span class="s2">Opłata za zamieszczenie ogłoszenia będzie pobierana automatycznie za pośrednictwem serwis&oacute;w świadczących takie usługi. Ogłoszenie zamieszczone przez Użytkownika ważne jest przez okres 30 dni.</span></li>
<li class="li5"><span class="s2">Po upływie okresu o jakim mowa w pkt 4 ogłoszenie będzie usuwane z portalu.</span></li>
<li class="li5"><span class="s2">Zamieszczenie Ogłoszenia wymaga akceptacji Regulaminu oraz wypełnienia przez Użytkownika odpowiedniego formularza.</span></li>
<li class="li5"><span class="s1">Użytkownik podczas zamieszczania ogłoszenia obowiązany jest opisać swoją ofertę w spos&oacute;b możliwie najdokładniejszy.</span></li>
<li class="li5"><span class="s1">Ilość Ogłoszeń zamieszczonych w przez jednego Użytkownika nie jest ograniczona. Zabronione jest<span class="Apple-converted-space">&nbsp; </span>umieszczanie ogłoszenia dotyczącego tej samej oferty więcej niż jeden raz.</span></li>
<li class="li4"><span class="s2">Ponadto zabronione jest zamieszczanie ogłoszeń kt&oacute;re: </span></li>
</ol>
<ul class="ul1">
<ul class="ul1">
<li class="li4"><span class="s2">Wprowadzają w błąd </span></li>
<li class="li4"><span class="s2">Nie są zgodne ze stanem faktycznym oferty;</span></li>
<li class="li4"><span class="s2">Zawierają treści i zdjęcia powszechnie uznane za obraźliwe, wulgarne;</span></li>
<li class="li4"><span class="s2">Zawierają zdjęcia ukazujące nagość itp.;</span></li>
<li class="li4"><span class="s2">Naruszają godność os&oacute;b trzecich;</span></li>
<li class="li4"><span class="s2">Zawierają adresy, nazwy i reklamy stron www i serwis&oacute;w internetowych;</span></li>
<li class="li4"><span class="s2">Zawierają treści lub dane osobowe mające na celu w jakikolwiek spos&oacute;b naruszyć dobra osobiste os&oacute;b trzecich;</span></li>
<li class="li4"><span class="s2">Noszą znamiona czyn&oacute;w nieuczciwej konkurencji;</span></li>
<li class="li4"><span class="s2">Naruszają obowiązujące przepisy prawa lub uprawnienia os&oacute;b trzecich</span></li>
</ul>
</ul>
<p class="p1"><span class="s1"><strong>V. Zasady odpowiedzialności</strong></span></p>
<p class="p1">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s2">Użytkownik ponosi pełną odpowiedzialność za publikowane treści w tym zdjęcia i zobowiązany jest do tego, aby nie naruszały one obowiązujących przepis&oacute;w prawa, zasad Regulaminu, prawa os&oacute;b trzecich, praw Administratora, zasad wsp&oacute;łżycia społecznego oraz dobrych obyczaj&oacute;w.</span></li>
<li class="li4"><span class="s2">Administrator nie ponosi odpowiedzialności za zachowania i działania podjęte przez Użytkownik&oacute;w w ramach serwisu.</span></li>
<li class="li5"><span class="s2">Administrator nie kontroluje wiarygodności os&oacute;b korzystających z serwisu medbra.in w szczeg&oacute;lności za podawanie niepełnych, niepoprawnych lub fałszywych danych oraz niezgodność danych ogłoszenia ze stanem faktycznym.</span></li>
<li class="li5"><span class="s2">Administrator zastrzega sobie możliwość dokonania kontroli ogłoszenia i wstrzymania jego publikacji w przypadku podejrzeń co do legalności pochodzenia towaru lub podejrzeń, a także gdy ogłoszenie może naruszać Regulamin lub obowiązujące przepisy prawa;</span></li>
<li class="li5"><span class="s2">Administrator nie ponosi odpowiedzialności za jakość, stan, pochodzenie lub legalność ofert, prawdziwość i rzetelność informacji podawanych przez Użytkownika, a także zdolności Użytkownik&oacute;w do dokonania transakcji.</span></li>
<li class="li5"><span class="s2">Administrator nie ponosi odpowiedzialności za niewykonanie lub nienależyte wykonanie przez strony um&oacute;w związanych z Ogłoszeniem, a także za szkody powstałe w wyniku wykonanie takiej umowy.</span></li>
<li class="li4"><span class="s2">Administrator zastrzega sobie prawo do usunięcia ogłoszenia, zmiany kategorii lub odpowiedniego zredagowania opublikowanych treści (w tym zdjęcia) kt&oacute;re:</span></li>
</ol>
<ul class="ul1">
<ul class="ul1">
<li class="li4"><span class="s2">są sprzeczne z<span class="Apple-converted-space">&nbsp; </span>niniejszym Regulaminem oraz obowiązującym prawem,</span></li>
<li class="li4"><span class="s2">są powszechnie uznane za obraźliwe.</span></li>
</ul>
</ul>
<ol class="ol2">
<li class="li5"><span class="s2">Administrator zastrzega sobie prawo do pełnego lub częściowego zablokowania korzystania z serwisu Użytkownikowi w przypadku wielokrotnego naruszania obowiązujących przepis&oacute;w prawa, lub Regulaminu.</span></li>
</ol>
<p class="p1"><span class="s1"><strong>IX. Reklamacje i odstąpienie od umowy</strong></span></p>
<p class="p1">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s2">Reklamacje dotyczące świadczenia usług przez Administratora, o kt&oacute;rych mowa w niniejszym Regulaminie, można zgłaszać za pomocą poczty elektronicznej na adres: </span><span class="s1">michalskwira@gmail.com</span> <span class="s1">w terminie 14 dni od dnia zakończenia emisji Ogłoszenia bądź dnia, w kt&oacute;rym emisja powinna się zakończyć.</span></li>
<li class="li5"><span class="s1">Reklamacja powinna zawierać co najmniej takie dane kt&oacute;re umożliwiają identyfikację Ogłoszenia oraz tożsamość zgłaszającego reklamację oraz okoliczności stanowiące podstawę reklamacji oraz żądanie reklamacyjne;</span></li>
<li class="li5"><span class="s1">Jeżeli podane w reklamacji dane lub informacje wymagają uzupełnienia, Administrator przed rozpatrzeniem reklamacji, zwr&oacute;ci się do Użytkownika o jej uzupełnienie we wskazanym zakresie.</span></li>
<li class="li5"><span class="s1">Reklamacja prawidłowo złożona jest rozpoznawana w terminie 14 dni od daty otrzymania. W przypadku konieczności jej uzupełnienia termin 14 dniowy na jej rozpatrzenie rozpoczyna bieg od daty jej uzupełnienia. Użytkownik otrzyma informację o sposobie rozpatrzenia reklamacji drogą korespondencji elektronicznej, na adres e-mail przypisany do Konta.</span></li>
<li class="li5"><span class="s1">Klient będący jednocześnie Konsumentem ma prawo odstąpić od zawartej na odległość umowy w terminie 14&nbsp;dni bez podawania przyczyny i bez ponoszenia koszt&oacute;w.</span></li>
<li class="li5"><span class="s1">Oświadczenie o odstąpieniu od umowy może zostać przez Konsumenta złożone na formularzu, kt&oacute;rego wz&oacute;r stanowi załącznik do niniejszego regulaminu. Posłużenie się tym formularzem nie jest obowiązkowe, a Klient może złożyć oświadczenie o odstąpieniu od umowy także w inny spos&oacute;b.</span></li>
<li class="li5"><span class="s1">Prawo odstąpienia od umowy zawartej poza lokalem przedsiębiorstwa lub na odległość nie przysługuje Konsumentowi w odniesieniu do um&oacute;w:</span></li>
</ol>
<ol class="ol3">
<li class="li5"><span class="s1">o świadczenie usług, jeżeli przedsiębiorca wykonał w pełni usługę za wyraźną zgodą konsumenta, kt&oacute;ry został poinformowany przed rozpoczęciem świadczenia, że po spełnieniu świadczenia przez przedsiębiorcę utraci prawo odstąpienia od umowy</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej cena lub wynagrodzenie zależy od wahań na rynku finansowym, nad kt&oacute;rymi przedsiębiorca nie sprawuje kontroli, i kt&oacute;re mogą wystąpić przed upływem terminu do odstąpienia od umowy</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia jest rzecz nieprefabrykowana, wyprodukowana według specyfikacji konsumenta lub służąca zaspokojeniu jego zindywidualizowanych potrzeb</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia jest rzecz ulegająca szybkiemu zepsuciu lub mająca kr&oacute;tki termin przydatności do użycia</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia jest rzecz dostarczana w zapieczętowanym opakowaniu, kt&oacute;rej po otwarciu opakowania nie można zwr&oacute;cić ze względu na ochronę zdrowia lub ze względ&oacute;w higienicznych, jeżeli opakowanie zostało otwarte po dostarczeniu</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia są rzeczy, kt&oacute;re po dostarczeniu, ze względu na sw&oacute;j charakter, zostają nierozłącznie połączone z innymi rzeczami</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia są napoje alkoholowe, kt&oacute;rych cena została uzgodniona przy zawarciu umowy sprzedaży, a kt&oacute;rych dostarczenie może nastąpić dopiero po upływie 30 dni i kt&oacute;rych wartość zależy od wahań na rynku, nad kt&oacute;rymi przedsiębiorca nie ma kontroli</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej konsument wyraźnie żądał, aby przedsiębiorca do niego przyjechał w&nbsp;celu dokonania pilnej naprawy lub konserwacji; jeżeli przedsiębiorca świadczy dodatkowo inne usługi niż te, kt&oacute;rych wykonania konsument żądał, lub dostarcza rzeczy inne niż części zamienne niezbędne do wykonania naprawy lub konserwacji, prawo odstąpienia od umowy przysługuje konsumentowi w odniesieniu do dodatkowych usług lub rzeczy</span></li>
<li class="li5"><span class="s1">w kt&oacute;rej przedmiotem świadczenia są nagrania dźwiękowe lub wizualne albo programy komputerowe dostarczane w zapieczętowanym opakowaniu, jeżeli opakowanie zostało otwarte po dostarczeniu</span></li>
<li class="li5"><span class="s1">o dostarczanie dziennik&oacute;w, periodyk&oacute;w lub czasopism, z wyjątkiem umowy o&nbsp;prenumeratę</span></li>
<li class="li5"><span class="s1">zawartej w drodze aukcji publicznej</span></li>
<li class="li5"><span class="s1">o świadczenie usług w zakresie zakwaterowania, innych niż do cel&oacute;w mieszkalnych, przewozu rzeczy, najmu samochod&oacute;w, gastronomii, usług związanych z wypoczynkiem, wydarzeniami rozrywkowymi, sportowymi lub kulturalnymi, jeżeli w umowie oznaczono dzień lub okres świadczenia usługi</span></li>
<li class="li5"><span class="s1">o dostarczanie treści cyfrowych, kt&oacute;re nie są zapisane na nośniku materialnym, jeżeli spełnianie świadczenia rozpoczęło się za wyraźną zgodą konsumenta przed upływem terminu do odstąpienia od umowy i po poinformowaniu go przez przedsiębiorcę o utracie prawa odstąpienia od umowy.</span><span class="s4">&nbsp;</span></li>
</ol>
<p class="p4"><span class="s5"><strong>X</strong></span><span class="s2"><strong> Informacje dodatkowe dla Konsument&oacute;w</strong></span></p>
<p class="p4">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s1">Klient będący Konsumentem ma możliwość skorzystania ze wszelkich możliwych pozasądowych sposob&oacute;w rozpatrywania reklamacji i dochodzenia roszczeń, w tym np. przy udziale polubownych sąd&oacute;w konsumenckich czy też ma możliwość korzystać z pomocy rzecznik&oacute;w konsument&oacute;w.</span></li>
<li class="li5"><span class="s1">Zgodnie z ustawą z dnia 15 grudnia 2000 roku o Inspekcji Handlowej (Dz. U. z 2001 r., Nr 4 poz. 25 ze zm.) stałe polubowne sądy konsumenckie działają przy wojew&oacute;dzkich inspektorach handlowych i tworzone są na podstawie um&oacute;w o zorganizowaniu takich sąd&oacute;w, zawartych przez wojew&oacute;dzkich inspektor&oacute;w z organizacjami pozarządowymi reprezentującymi konsument&oacute;w lub przedsiębiorc&oacute;w oraz innymi zainteresowanymi jednostkami organizacyjnymi.</span> <span class="s1">Stałe polubowne sądy konsumenckie rozpatrują spory o prawa majątkowe wynikłe z um&oacute;w sprzedaży produkt&oacute;w i świadczenia usług zawieranych pomiędzy konsumentami i przedsiębiorcami. W celu zainicjowania takiego postępowania konsument powinien skierować do takiego sądu polubownego odpowiedni wniosek o rozpatrzenie sporu. W takim wniosku należy dokładnie oznaczyć strony sporu (dane konsumenta i przedsiębiorcy będących stronami umowy) oraz przedmiot sporu. Do tak sporządzonego i podpisanego wniosku należy ponadto dołączyć jego odpis w celu doręczenia go stronie przeciwnej (przedsiębiorcy).</span> <span class="s1">Wyrok stałego polubownego sądu konsumenckiego i ugoda zawarta przed takim sądem stanowią tytuły egzekucyjne. Po nadaniu im klauzuli wykonalności przez sąd taki wyrok lub ugoda, jako tytuł wykonawczy, stanowi podstawę wszczęcia postępowania egzekucyjnego.</span></li>
<li class="li5"><span class="s1">Zwr&oacute;cenie się do wojew&oacute;dzkiego inspektora Inspekcji Handlowej z wnioskiem o wszczęcie postępowania mediacyjnego w sprawie polubownego zakończenia sporu między konsumentem a przedsiębiorcą</span></li>
<li class="li5"><span class="s1">Każdy konsument może skorzystać z bezpłatnej pomocy powiatowego (miejskiego) rzecznika konsument&oacute;w.&nbsp;</span></li>
</ol>
<p class="p5"><span class="s1"><strong>Rozdział XI</strong></span> <span class="s1"><strong>Postanowienia końcowe</strong></span></p>
<p class="p5">&nbsp;</p>
<ol class="ol2">
<li class="li5"><span class="s1">W sprawach nieuregulowanych w regulaminie mają zastosowanie przepisy prawa polskiego, a zwłaszcza Kodeksu cywilnego</span></li>
<li class="li5"><span class="s1">Wyb&oacute;r prawa polskiego nie pozbawia konsumenta ochrony przyznanej mu na podstawie przepis&oacute;w, kt&oacute;rych nie można wyłączyć w drodze umowy, na mocy prawa jakie byłoby właściwe w braku wyboru, tj. prawa państwa, w kt&oacute;rym konsument ma miejsce zwykłego pobytu, a przedsiębiorca (1) wykonuje swoją działalność gospodarczą lub zawodową w państwie w kt&oacute;rym konsument ma miejsce zwykłego pobytu; lub (2) w jakikolwiek spos&oacute;b kieruje taką działalność do tego państwa lub do kilku państw z tym państwem włącznie; a umowa wchodzi w zakres tej działalności;</span></li>
<li class="li5"><span class="s1">Ewentualne spory powstałe między Klientem będącym Przedsiębiorcą a Sprzedawcą rozstrzygane będą przez sąd miejscowo właściwy dla siedziby Sprzedawcy;</span></li>
<li class="li5"><span class="s1">Regulamin wchodzi w życie z dniem 1 marca 2020 r.</span></li>
</ol>'
        ],
        [
            'tytul'=>'Polityka prywatności',
            'tresc'=>'<p class="p2">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Dane Administratora Danych Osobowych</strong></span></li>
</ol>
<p class="p3"><span class="s2">Uprzejmie informujemy, że administratorem Pana/Pani danych osobowych jest </span><span class="s1">Michał Skwira, adres do doręczeń Kaszubska 34/2, 70 - 227 Szczecin, email: </span><span class="s2">michalskwira@gmail.com</span></p>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Cele i podstawy przetwarzania danych osobowych</strong></span></li>
</ol>
<p class="p3"><span class="s2">Aby świadczyć usługi zgodnie z profilem działalności, Administrator przetwarza Pana/Pani dane osobowe &mdash; w r&oacute;żnych celach, jednak zawsze zgodnie z prawem. Poniżej znajdzie Pan/Pani wyszczeg&oacute;lnione cele przetwarzania danych osobowych wraz z podstawami prawnymi.</span></p>
<p class="p3"><span class="s2">W celu <strong>wykonania usługi &ndash; publikacji ogłoszenia</strong> przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">Imię i nazwisko, firmę przedsiębiorcy;</span></li>
<li class="li3"><span class="s2">adres e-mail,</span></li>
<li class="li3"><span class="s2">Adres siedziby/zamieszkania, </span></li>
<li class="li3"><span class="s2">Numer telefonu,</span></li>
<li class="li3"><span class="s2">NIP</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. b RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli są one konieczne do wykonania umowy lub podjęcia czynności zmierzających do zawarcia umowy; jeżeli zdecyduje się Pan/Pani podać r&oacute;wnież numer telefonu, uznajemy, że wyraził(a) Pan/Pani zgodę na przetwarzanie r&oacute;wnież Pana/Pani telefonu &mdash; wtedy podstawą prawną takiego przetwarzania jest art. 6 ust. 1 lit. a RODO, kt&oacute;ry pozwala przetwarzać dane osobowe na podstawie dobrowolnie udzielonej zgody;</span></p>
<p class="p3"><span class="s2">W celu <strong>rozpatrzenia reklamacji</strong> przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">imię i nazwisko/firma</span></li>
<li class="li3"><span class="s2">adres e-mail,</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. b RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli są one konieczne do wykonania umowy lub podjęcia czynności zmierzających do zawarcia umowy;</span></p>
<p class="p3"><span class="s2">W celu <strong>przesyłania powiadomień e-mail zawierający inf. handlowe</strong><span class="Apple-converted-space">&nbsp; </span>przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">adres e-mail,</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. f RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli tym sposobem Administrator Danych Osobowych realizuje sw&oacute;j prawnie uzasadniony interes (w tym przypadku interesem Administratora jest informowanie klienta o czynnościach związanych z realizacją usługi w celu podwyższenia komfortu korzystania z serwisu);</span></p>
<p class="p3"><span class="s2">W celu <strong>wystawienia faktury i spełnienia innych obowiązk&oacute;w wynikających z przepis&oacute;w prawa podatkowego</strong>, takich jak np. przechowywanie dokumentacji księgowej przez 5 lat, przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">imię i nazwisko,</span></li>
<li class="li3"><span class="s2">firma,</span></li>
<li class="li3"><span class="s2">adres zamieszkania lub adres siedziby,</span></li>
<li class="li3"><span class="s2">numer NIP,</span></li>
<li class="li3"><span class="s2">numer zlecenia.</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. c RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli takie przetwarzanie jest konieczne do wywiązania się przez Administratora Danych Osobowych z obowiązk&oacute;w wynikających z prawa;</span></p>
<p class="p3"><span class="s2">W celu <strong>ustalenia, dochodzenia lub obrony przed roszczeniami</strong> przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">imię i nazwisko (jeżeli nazwisko zostało podane) lub ewentualnie firmę,</span></li>
<li class="li3"><span class="s2">adres zamieszkania (jeżeli został podany),</span></li>
<li class="li3"><span class="s2">numer PESEL lub numer NIP (jeżeli został podany),</span></li>
<li class="li3"><span class="s2">adres e-mail,</span></li>
<li class="li3"><span class="s2">IP,</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. f RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli tym sposobem Administrator Danych Osobowych realizuje sw&oacute;j prawnie uzasadniony interes (w tym przypadku interesem Administratora jest posiadanie danych osobowych, kt&oacute;re pozwolą ustalić, dochodzić lub bronić się przed roszczeniami, w tym klient&oacute;w i os&oacute;b trzecich);</span></p>
<p class="p3"><span class="s2">W celu <strong>archiwalnym i dowodowym</strong> przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">imię i nazwisko ,</span></li>
<li class="li3"><span class="s2">adres e-mail,</span></li>
<li class="li5"><span class="s6">IP</span></li>
</ul>
<p class="p3"><span class="s2">&mdash; na potrzeby zabezpieczenia informacji, kt&oacute;re mogą służyć wykazywaniu fakt&oacute;w o znaczeniu prawnym. Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. f RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli tym sposobem Administrator Danych Osobowych realizuje sw&oacute;j prawnie uzasadniony interes (w tym przypadku interesem Administratora jest posiadanie danych osobowych, kt&oacute;re pozwolą dowieść pewnych fakt&oacute;w związanych z realizacją usług, np. gdy jakiś organ państwowy tego zażąda);</span></p>
<p class="p3"><span class="s2">W celu <strong>analitycznym</strong>, tj. badania i analizowania aktywności na stronie internetowej należącej do Administratora, przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s1">data i godzina odwiedzin strony,</span></li>
<li class="li3"><span class="s1">rodzaj systemu operacyjnego,</span></li>
<li class="li3"><span class="s1">przybliżona lokalizacja,</span></li>
<li class="li3"><span class="s1">rodzaj przeglądarki internetowej wykorzystywanej do przeglądania strony,</span></li>
<li class="li3"><span class="s1">czas spędzony na stronie,</span></li>
<li class="li3"><span class="s1">odwiedzone podstrony,</span></li>
<li class="li3"><span class="s1">podstrona, gdzie wypełniono formularz kontaktowy.</span></li>
</ul>
<p class="p3"><span class="s2">Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. f RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli tym sposobem Administrator Danych Osobowych realizuje sw&oacute;j prawnie uzasadniony interes (w tym przypadku interesem Administratora jest poznanie aktywności klient&oacute;w na stronie internetowej);</span></p>
<p class="p3"><span class="s2">W celu <strong>wykorzystywania cookies</strong> na stronie internetowej przetwarzamy takie informacje tekstowe (cookies zostaną opisane w odrębnym punkcie). Podstawą prawną takiego przetwarzania jest art. 6 ust. 1 lit. a RODO, kt&oacute;ry pozwala przetwarzać dane osobowe na podstawie dobrowolnie udzielonej zgody (przy pierwszym wejściu na stronę internetową pojawia się zapytanie o zgodę na wykorzystanie cookies);</span></p>
<p class="p3"><span class="s2">W celu <strong>administrowania stroną internetową</strong> przetwarzamy takie dane osobowe, jak:</span></p>
<ul class="ul1">
<li class="li3"><span class="s2">adres IP,</span></li>
<li class="li3"><span class="s2">data i czas serwera,</span></li>
<li class="li3"><span class="s2">informacje o przeglądarce internetowej,</span></li>
<li class="li3"><span class="s2">informacje o systemie operacyjnym</span></li>
</ul>
<p class="p3"><span class="s2">&mdash; dane te są zapisywane automatycznie w tzw. logach serwera, przy każdorazowym korzystaniu ze strony należącej do Administratora. Administrowanie stroną internetową bez użycia serwera i bez tego automatycznego zapisu nie byłoby możliwe. Podstawą prawną takiego przetwarzania danych jest art. 6 ust. 1 lit. f RODO, kt&oacute;ry pozwala przetwarzać dane osobowe, jeżeli tym sposobem Administrator Danych Osobowych realizuje sw&oacute;j prawnie uzasadniony interes (w tym przypadku interesem Administratora jest administrowanie stroną internetową);</span></p>
<p class="p6">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong> Cookies</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Administrator na swojej stronie internetowej, podobnie jak inne podmioty, wykorzystuje tzw. cookies, czyli kr&oacute;tkie informacje tekstowe, zapisywane na komputerze, telefonie, tablecie, czy też innym urządzeniu użytkownika. Mogą być one odczytywane przez nasz system, a także przez systemy należące do innych podmiot&oacute;w, z kt&oacute;rych usług korzystamy (np. Facebooka, Google&rsquo;a).</span></li>
<li class="li3"><span class="s2">Cookies spełniają bardzo wiele funkcji na stronie internetowej, najczęściej przydatnych, kt&oacute;re postaramy się opisać poniżej (jeżeli informacje są niewystarczające, prosimy o kontakt):</span></li>
</ol>
<ul class="ul1">
<li class="li7"><span class="s2"><strong>zapewnianie bezpieczeństwa</strong> &mdash; pliki cookies są wykorzystywane w celu uwierzytelniania użytkownik&oacute;w oraz zapobiegania nieupoważnionemu korzystaniu z panelu klienta. Służą zatem do ochrony danych osobowych użytkownika przed dostępem os&oacute;b nieupoważnionych;</span></li>
<li class="li7"><span class="s2"><strong>wpływ na procesy i wydajność korzystania ze strony internetowej</strong> &mdash; pliki cookies są wykorzystywane do tego, aby witryna sprawnie działała i aby można było korzystać z funkcji na niej dostępnych, co jest możliwe między innymi dzięki zapamiętywaniu ustawień pomiędzy kolejnymi odwiedzinami na stronie. Dzięki nim można zatem sprawnie poruszać się na stronie internetowej i poszczeg&oacute;lnych podstronach;</span></li>
<li class="li7"><span class="s2"><strong>stan sesji</strong> &mdash; w plikach cookies często są zapisywane informacje o tym, jak odwiedzający korzystają ze strony internetowej, np. kt&oacute;re podstrony najczęściej wyświetlają. Umożliwiają r&oacute;wnież identyfikację błęd&oacute;w wyświetlanych na niekt&oacute;rych podstronach. Pliki cookies służące do zapisywania tzw. &bdquo;stanu sesji&rdquo; pomagają zatem ulepszać usługi i zwiększać komfort przeglądania stron;</span></li>
<li class="li7"><span class="s2"><strong>utrzymanie stanu sesji</strong> &mdash; jeżeli klient loguje się do swojego panelu, to pliki cookies umożliwiają podtrzymanie sesji. Oznacza to, że po przejściu na inną podstronę nie trzeba każdorazowo podawać ponownie loginu i hasła, co sprzyja komfortowi korzystania ze strony internetowej;</span></li>
<li class="li7"><span class="s2"><strong>tworzenie statystyk</strong> &mdash; pliki cookies są wykorzystywane do tego, aby przeanalizować, w jaki spos&oacute;b użytkownicy korzystają ze strony internetowej (jak wielu otwiera stronę internetową, jak długo na niej pozostają, kt&oacute;re treści wzbudzają największe zainteresowanie etc.). Dzięki temu można stale ulepszać stronę internetową i dostosowywać jej działanie do preferencji użytkownik&oacute;w. W celu śledzenia aktywności i tworzenia statystyk wykorzystujemy narzędzia Google&rsquo;a, takie jak Google Analytics; opr&oacute;cz raportowania statystyk użytkowania witryny pikselowy Google Analytics może r&oacute;wnież służyć, razem z niekt&oacute;rymi opisanymi powyżej plikami cookies, do pomocy w wyświetlaniu użytkownikowi bardziej trafnych treści w usługach Google (np. w wyszukiwarce Google) i w całej sieci;</span></li>
<li class="li7"><span class="s2"><strong>korzystanie z funkcji społecznościowych</strong> &mdash; na stronie internetowej posiadamy tzw. pixel Facebooka, kt&oacute;ry umożliwia polubienie naszego fanpage&rsquo;a w tym serwisie podczas korzystania z witryny. Jednak, aby to było możliwe, musimy korzystać z plik&oacute;w cookies dostarczanych przez Facebooka.</span></li>
</ul>
<ol class="ol2">
<li class="li3"><span class="s2">Pana/Pani przeglądarka internetowa domyślnie dopuszcza wykorzystywanie cookies w Pana/Pani urządzeniu, dlatego przy pierwszej wizycie prosimy o wyrażenie zgody na użycie cookies. Jeżeli jednak nie życzy Pan/Pani sobie wykorzystania cookies przy przeglądaniu strony internetowej, można zmienić ustawienia w przeglądarce internetowej &mdash; całkowicie blokować automatyczną obsługę plik&oacute;w cookies lub żądać powiadomienia o każdorazowym zamieszczeniu cookies w urządzeniu. Ustawienia można zmienić w dowolnej chwili.</span></li>
<li class="li3"><span class="s2">Szanując autonomię wszystkich os&oacute;b korzystających ze strony internetowej, czujemy się jednak w obowiązku uprzedzić, że wyłączenie lub ograniczenie obsługi plik&oacute;w cookies może spowodować dość poważne trudności w korzystaniu ze strony internetowej, np. w postaci konieczności logowania się na każdej podstronie, dłuższego okresu ładowania się strony, ograniczeń w korzystaniu z funkcjonalności, ograniczeń w polubieniu strony na Facebooku etc.</span></li>
</ol>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Prawo wycofania zgody</strong></span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol2">
<li class="li3"><span class="s2">Jeżeli przetwarzanie danych osobowych odbywa się na podstawie zgody, w każdej chwili może Pan/Pani tę zgodę cofnąć &mdash; wedle własnego uznania.</span></li>
<li class="li3"><span class="s2">Jeżeli chciałby/chciałaby Pan/Pani cofnąć zgodę na przetwarzanie danych osobowych, to w tym celu wystarczy:</span></li>
</ol>
<ul class="ul1">
<li class="li7"><span class="s2">wysłać maila bezpośrednio Administratorowi na adres michalskwira@gmail.com</span></li>
</ul>
<ol class="ol2">
<li class="li3"><span class="s2">Jeżeli przetwarzanie Pana/Pani danych osobowych odbywało się na podstawie zgody, jej cofnięcie nie powoduje, że przetwarzanie danych osobowych do tego momentu było nielegalne. Innymi słowy, do czasu cofnięcia zgody mamy prawo przetwarzać Pana/Pani dane osobowe i jej odwołanie nie wpływa na zgodność z prawem dotychczasowego przetwarzania.</span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Wym&oacute;g podania danych osobowych</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Podanie jakichkolwiek danych osobowych jest dobrowolne i zależy od Pana/Pani decyzji. Jednakże w niekt&oacute;rych przypadkach podanie określonych danych osobowych jest konieczne, aby spełnić Pana/Pani oczekiwania w zakresie korzystania z usług.</span></li>
<li class="li3"><span class="s2">Aby zlecić usługę w serwisie, konieczne jest podanie imienia oraz adresu e-mail &mdash; bez tego nie jesteśmy w stanie zawrzeć oraz wykonać umowy.</span></li>
<li class="li3"><span class="s2">Aby m&oacute;gł/mogła Pan/Pani otrzymać fakturę za usługi, konieczne jest podanie wszystkich danych wymaganych prawem podatkowym, a zatem imienia i nazwiska lub firmy, adresu zamieszkania lub adresu siedziby, numeru NIP &mdash; bez tego nie jesteśmy w stanie prawidłowo wystawić faktury.</span></li>
<li class="li3"><span class="s2">Aby m&oacute;c się skontaktować z Panem/Panią telefonicznie w sprawach związanych z realizacją usługi, konieczne jest podanie numeru telefonu &mdash; bez tego nie jesteśmy w stanie nawiązać kontaktu telefonicznego.</span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Zautomatyzowane podejmowanie decyzji i profilowanie</strong></span></li>
</ol>
<p class="p3"><span class="s2">Uprzejmie informujemy, że nie dokonujemy zautomatyzowanego podejmowania decyzji, w tym w oparciu o profilowanie. Treść zapytania, kt&oacute;ra jest przesyłana za pośrednictwem formularza kontaktowego, nie podlega ocenie przez system informatyczny. Proponowana cena usługi w żaden spos&oacute;b nie jest wynikiem oceny dokonanej przez jakikolwiek system informatyczny.</span></p>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Odbiorcy danych osobowych</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Jak większość przedsiębiorc&oacute;w, w swojej działalności korzystamy z pomocy innych podmiot&oacute;w, co niejednokrotnie wiąże się z koniecznością przekazania danych osobowych. W związku z powyższym, w razie potrzeby, przekazujemy Pana/Pani dane osobowe wsp&oacute;łpracującym z nami prawnikom, kt&oacute;rzy realizują usługi, firmom obsługującym szybkie płatności, firmie księgowej, firmie hostingowej.</span></li>
<li class="li3"><span class="s2">Opr&oacute;cz tego, może się zdarzyć, że np. na podstawie właściwego przepisu prawa lub decyzji właściwego organu będziemy musieli przekazać Pana/Pani dane osobowe r&oacute;wnież innym podmiotom, czy to publicznym czy prywatnym. Dlatego niezmiernie trudno nam przewidzieć, kto może zgłosić się z żądaniem udostępnienia danych osobowych. Niemniej ze swojej strony zapewniamy, że każdy przypadek żądania udostępnienia danych osobowych analizujemy bardzo starannie i bardzo wnikliwie, aby niechcący nie przekazać informacji osobie nieuprawnionej.</span></li>
<li class="li3"><span class="s2">Dane osobowe w ściśle określonym zakresie mogą być r&oacute;wnież przekazywane po uprzedniej Państwa zgodzie innym podmiotom &ndash; w tym partnerom handlowych.</span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Przekazywanie danych osobowych do państw trzecich</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Jak większość przedsiębiorc&oacute;w, korzystamy z r&oacute;żnych popularnych usług i technologii, oferowanych przez takie podmioty, jak Facebook, Microsoft, Google. Firmy te mają siedziby poza Unią Europejską, a zatem w świetle przepis&oacute;w RODO są traktowane jako państwa trzecie.</span></li>
<li class="li3"><span class="s2">RODO wprowadza pewne ograniczenia w przekazywaniu danych osobowych do państw trzecich, ponieważ skoro nie stosuje się tam, co do zasady przepis&oacute;w europejskich, ochrona danych osobowych obywateli Unii Europejskiej może być niestety niewystarczająca. Dlatego też każdy administrator danych osobowych ma obowiązek ustalić podstawę prawną takiego przekazywania.</span></li>
<li class="li3"><span class="s2">Ze swojej strony zapewniamy, że przy korzystaniu z usług i technologii przekazujemy dane osobowe wyłącznie podmiotom ze Stan&oacute;w Zjednoczonych i wyłącznie takim, kt&oacute;re przystąpiły do programu Privacy Shield, na podstawie decyzji wykonawczej Komisji Europejskiej z dnia 12 lipca 2016 r. &mdash; więcej na ten temat można przeczytać na stronie Komisji Europejskiej dostępnej pod adresem <a href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/eu-us-privacy-shield_pl"><span class="s7">https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/eu-us-privacy-shield_pl</span></a>. Podmioty, kt&oacute;re przystąpiły do programu Privacy Shield, gwarantują, że będą przestrzegać wysokich standard&oacute;w w zakresie ochrony danych osobowych, jakie obowiązują w Unii Europejskiej, dlatego korzystanie z ich usług i oferowanych technologii w procesie przetwarzania danych osobowych jest zgodne z prawem.</span></li>
<li class="li3"><span class="s2">W każdej chwili udzielimy Panu/Pani dodatkowych wyjaśnień odnośnie przekazywania danych osobowych, w szczeg&oacute;lności, gdy kwestia ta budzi Pana/Pani niepok&oacute;j.</span></li>
<li class="li3"><span class="s2">W każdej chwili przysługuje Panu/Pani prawo do uzyskania kopii danych osobowych przekazanych do państwa trzeciego.</span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Okres przetwarzania danych osobowych</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Zgodnie z obowiązującymi przepisami prawa nie przetwarzamy Pana/Pani danych osobowych &bdquo;w nieskończoność&rdquo;, lecz przez czas, kt&oacute;ry jest potrzebny, aby osiągnąć wyznaczony cel. Po tym okresie Pana/Pani dane osobowe zostaną nieodwracalnie usunięte lub zniszczone.</span></li>
<li class="li3"><span class="s2">W sytuacji, gdy nie potrzebujemy wykonywać innych operacji na Pana/Pani danych osobowych niż ich przechowywanie (np. gdy przechowujemy treść zlecenia na potrzeby obrony przed roszczeniami), do momentu trwałego usunięcia lub zniszczenia dodatkowo je zabezpieczamy &mdash; poprzez pseudonimizację. Pseudonimizacja polega na takim zaszyfrowaniu danych osobowych, czy też zbioru danych osobowych, że bez dodatkowego klucza nie da się ich odczytać, a zatem takie informacje stają się całkowicie bezużyteczne dla osoby nieuprawnionej.</span></li>
<li class="li3"><span class="s2">Odnośnie poszczeg&oacute;lnych okres&oacute;w przetwarzania danych osobowych, uprzejmie informujemy, że dane osobowe przetwarzamy przez okres:</span></li>
</ol>
<ul class="ul1">
<li class="li3"><span class="s2">trwania umowy &mdash; w odniesieniu do danych osobowych przetwarzanych w celu zawarcia i wykonania umowy;</span></li>
<li class="li3"><span class="s2">3 lat lub 6 lat + 1 rok &mdash; w odniesieniu do danych osobowych przetwarzanych w celu ustalenia, dochodzenia lub obrony roszczeń (długość okresu zależy od tego, czy obie strony są przedsiębiorcami, czy też nie);</span></li>
<li class="li3"><span class="s2">6 miesięcy &mdash; w odniesieniu do danych osobowych, kt&oacute;re zostały zebrane przy wycenie usługi, a jednocześnie nie doszło do niezwłocznego zawarcia umowy;</span></li>
<li class="li3"><span class="s2">5 lat &mdash; w odniesieniu do danych osobowych wiążących się ze spełnieniem obowiązk&oacute;w z prawa podatkowego;</span></li>
<li class="li3"><span class="s2">do czasu cofnięcia zgody lub osiągnięcia celu przetwarzania, jednak nie dłużej niż przez 5 lat &mdash; w odniesieniu do danych osobowych przetwarzanych na podstawie zgody;</span></li>
<li class="li3"><span class="s2">do czasu skutecznego wniesienia sprzeciwu lub osiągnięcia celu przetwarzania, jednak nie dłużej niż przez 5 lat &mdash; w odniesieniu do danych osobowych przetwarzanych na podstawie prawnie uzasadnionego interesu Administratora Danych Osobowych lub do cel&oacute;w marketingowych;</span></li>
<li class="li3"><span class="s2">do czasu zdezaktualizowania się lub utraty przydatności, jednak nie dłużej niż przez 3 lata &mdash; w odniesieniu do danych osobowych przetwarzanych gł&oacute;wnie do cel&oacute;w analitycznych, wykorzystania cookies i administrowania stroną internetową;</span></li>
</ul>
<p class="p8">&nbsp;</p>
<ol class="ol2">
<li class="li3"><span class="s2">Okresy w latach liczymy od końca roku, w kt&oacute;rym rozpoczęliśmy przetwarzanie danych osobowych, aby usprawnić proces usuwania lub niszczenia danych osobowych. Odrębne liczenie terminu dla każdej zawartej umowy wiązałoby się z istotnymi trudnościami organizacyjnymi i technicznymi, jak r&oacute;wnież znaczącym nakładem finansowym, dlatego ustanowienie jednej daty usuwania lub niszczenia danych osobowych pozwala nam sprawniej zarządzać tych procesem. Oczywiście, w przypadku skorzystania przez Pana/Panią z prawa do zapomnienia takie sytuacje są rozpatrywane indywidualnie.</span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Dodatkowy rok związany z przetwarzaniem danych osobowych zebranych na potrzeby wykonania umowy jest podyktowany tym, że hipotetycznie może Pan/Pani zgłosić roszczenie na chwilę przed upływem terminu przedawnienia, żądanie może zostać doręczone z istotnym op&oacute;źnieniem lub może Pan/Pani błędnie określić termin przedawnienia swojego roszczenia.</span></li>
</ol>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Uprawnienia podmiot&oacute;w danych</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Uprzejmie informujemy, że posiada Pan/Pani prawo do:</span></li>
</ol>
<ul class="ul1">
<li class="li3"><span class="s2">dostępu do swoich danych osobowych;</span></li>
<li class="li3"><span class="s2">sprostowania danych osobowych;</span></li>
<li class="li3"><span class="s2">usunięcia danych osobowych;</span></li>
<li class="li3"><span class="s2">ograniczenia przetwarzania danych osobowych;</span></li>
<li class="li3"><span class="s2">sprzeciwu wobec przetwarzania danych osobowych;</span></li>
<li class="li3"><span class="s2">przenoszenia danych osobowych.</span></li>
</ul>
<ol class="ol2">
<li class="li3"><span class="s2">Szanujemy Pana/Pani prawa wynikające z przepis&oacute;w o ochronie danych osobowych i staramy się ułatwiać ich realizację w najwyższym możliwym stopniu.</span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Wskazujemy, że wymienione uprawnienia nie mają charakteru absolutnego, a zatem w niekt&oacute;rych sytuacjach możemy zgodnie z prawem odm&oacute;wić Panu/Pani ich spełnienia. Jednakże, jeżeli odmawiamy uwzględnienia żądania, to tylko po wnikliwej analizie i tylko w sytuacji, gdy odmowa uwzględnienia żądania jest konieczna.</span></li>
<li class="li3"><span class="s2">Odnośnie prawa do wniesienia sprzeciwu wyjaśniamy, że w każdej chwili przysługuje Panu/Pani prawo do sprzeciwienia się przetwarzaniu danych osobowych na podstawie prawnie uzasadnionego interesu Administratora Danych Osobowych (zostały one wymienione w punkcie III) w związku z Pana/Pani szczeg&oacute;lną sytuacją. Musi Pan/Pani jednak pamiętać, że zgodnie z przepisami możemy odm&oacute;wić uwzględnienia sprzeciwu, jeżeli wykażemy, że:</span></li>
</ol>
<ul class="ul1">
<li class="li3"><span class="s2">istnieją prawnie uzasadnione podstawy do przetwarzania, kt&oacute;re są nadrzędne w stosunku do Pana/Pani interes&oacute;w, praw i wolności lub</span></li>
<li class="li3"><span class="s2">istnieją podstawy do ustalenia, dochodzenia lub obrony roszczeń.</span></li>
</ul>
<ol class="ol2">
<li class="li3"><span class="s2">Ponadto w każdej chwili może Pan/Pani wnieść sprzeciw wobec przetwarzania Pana/Pani danych osobowych do cel&oacute;w marketingowych. W takiej sytuacji po otrzymaniu sprzeciwu zaprzestaniemy przetwarzania w tym celu.</span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">Swoje uprawnienia może Pan/Pani zrealizować poprzez:</span></li>
</ol>
<ul class="ul1">
<li class="li7"><span class="s2">wysłanie maila bezpośrednio Administratorowi na adres ...</span></li>
</ul>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Prawo do wniesienia skargi</strong></span></li>
</ol>
<p class="p3"><span class="s2">Jeżeli uważa Pan/Pani, że Pana/Pani dane osobowe są przetwarzane niezgodnie z obowiązującym prawem, może Pan/Pani wnieść skargę do Prezesa Urzędu Ochrony Danych Osobowych.</span></p>
<p class="p4">&nbsp;</p>
<ol class="ol1">
<li class="li3"><span class="s2"><strong>Postanowienia końcowe</strong></span></li>
</ol>
<ol class="ol2">
<li class="li3"><span class="s2">W zakresie nieuregulowanym niniejszą Polityką prywatności obowiązują przepisy z zakresu ochrony danych osobowych.</span></li>
<li class="li3"><span class="s2">O wszelkich zmianach wprowadzonych do niniejszej Polityki prywatności zostanie Pan/Pani powiadomiony/powiadomiona drogą e-mailową.</span></li>
<li class="li3"><span class="s2">Niniejsza Polityka prywatności obowiązuje od dnia 1 marca 2020 r.</span></li>
</ol>'
        ],
        [
            'tytul' => 'Odstąpienie od umowy',
            'tresc' => '<p class="p1">&nbsp;</p>
<p class="p1">&nbsp;</p>
<p class="p1">&nbsp;</p>
<p class="p2"><span class="s1">Miejscowość, data</span></p>
<p class="p3"><span class="s1">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</span></p>
<p class="p3"><span class="s1">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</span></p>
<p class="p3"><span class="s1">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</span></p>
<p class="p3"><span class="s1">Imię i nazwisko konsumenta(-&oacute;w)</span></p>
<p class="p3"><span class="s1">Adres konsumenta(-&oacute;w)</span></p>
<p class="p4">&nbsp;</p>
<p class="p5"><span class="s1"><span class="Apple-converted-space">&nbsp; &nbsp; &nbsp; &nbsp;</span></span></p>
<p class="p5">&nbsp;</p>
<p class="p6"><span class="s1">Michał Skwira</span></p>
<p class="p6"><span class="s1">Kaszubska 34/2</span></p>
<p class="p7"><span class="s2">70 - 227 Szczecin</span></p>
<p class="p5">&nbsp;</p>
<p class="p4">&nbsp;</p>
<p class="p4">&nbsp;</p>
<p class="p8"><span class="s1"><strong>Oświadczenie</strong></span></p>
<p class="p8"><span class="s1"><strong>o odstąpieniu od umowy zawartej na odległość </strong></span></p>
<p class="p8"><span class="s1"><strong>lub poza lokalem przedsiębiorstwa</strong></span></p>
<p class="p4">&nbsp;</p>
<p class="p4">&nbsp;</p>
<p class="p4">&nbsp;</p>
<p class="p3"><span class="s1">Ja/My (*)&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;&hellip;&hellip;&hellip;&hellip;niniejszym informuję/informujemy(*) o moim/naszym(*) odstąpieniu od umowy - świadczenia usług w postaci publikacji ogłoszenia w serwisie medbra.in(*) &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;&hellip;</span></p>
<p class="p4">&nbsp;</p>
<p class="p3"><span class="s1">Data zawarcia umowy&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. </span></p>
<p class="p9">&nbsp;</p>
<p class="p9">&nbsp;</p>
<p class="p9">&nbsp;</p>
<p class="p10"><span class="s1">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</span></p>
<p class="p11"><span class="s1">Podpis konsumenta(-&oacute;w)</span></p>
<p class="p4">&nbsp;</p>'
        ],
    ];

    foreach($cmsy as $cms) {
        $c = new Cms();
        $c->tytul = $cms['tytul'];
        $c->tresc = $cms['tresc'];
        $c->save();
    }
    
    /**
     * Faq
     */

     $faq = [
         [
             "nazwa" => "Czy musze sie rejestrowac?",
             "opis" => "Rejestracja jest niezbędna abyś mógł/mogła w pełni korzystać z funkcjonalności serwisu oraz abyśmy My mogli świadczyć dla Ciebie usługi na najwyższym poziomie. Tylko bowiem dla zarejestrowanych użytkowników możemy zaproponować indywidualne oferty i przesyłać powiadomienia o nowych ogłoszeniach, dzięki czemu będą one przesyłane Tobie tak szybko jak to możliwe. Ponadto rejestrując się będziesz mógł/mogła w szybki sposób publikować nowe ogłoszenia oraz otrzymywać informacje od kandydatów zainteresowanych ofertą. Sam proces rejestracji jest bardzo łatwy i bezpieczny."
         ],
         [
             "nazwa" => "Jak się zarejestrować?",
             "opis" => "W celu zarejestrowania nowego konta użytkownika musisz wypełnić odpowiedni formularz dla firm lub osób prywatnych. Formularz rejestracyjny dla firm znajdziesz w dziale 'szukam pracownika' a dla osób prywatnych w 'szukam pracy'. Jeśli poprawnie wypełniłeś zgłoszenie kliknij 'rejestruj' następnie otrzymasz od nas e-mail na podany w formularzu adres poczty elektronicznej z prośbą o potwierdzenie rejestracji konta. W celu potwierdzenia kliknij w link znajdujący się w treści e-maila, od tej chwili będziesz zarejestrowanym użytkownikiem serwisu medbra.in."
         ],
         [
             "nazwa" => "Jak zmienić hasło do mojego konta?",
             "opis" => "Jeśli zdecydujesz, że chcesz zmienić hasło do swojego konta w medbra.in wystarczy jak zalogujesz się i wybierzesz opcję 'zmiana hasła' dostępną w menu po lewej stronie. Następnie podaj nowe hasło i ponownie powtórz je w celu uniknięcia błędu, kliknij 'zapisz' od tego momentu nowe hasło jest aktywne."
         ],
         [
             "nazwa" => "Jak zmienić moje dane?",
             "opis" => "Wszelkie dane jakie wprowadziłeś w ramach swojego konta oraz profilu CV możesz w każdej chwili modyfikować. Po zalogowaniu do konta w medbra.in i masz możliwość wprowadzenia zmian w swoim CV jak i treści ogłoszenia."
         ],
         [
             "nazwa" => "Czy mogę otrzymywać oferty na maila?",
             "opis" => "Jasne, wystarczy że zaznaczysz odpowiednie pole w formularzu a My zadbamy abyś otrzymywał/a interesujące Cię oferty prosto na Twoją skrzynkę mailową."
         ],
         [
             "nazwa" => "Zapomniałem hasła, co mam zrobić?",
             "opis" => "Jeśli zapomniałeś hasła do swojego konta, wystarczy że skorzystasz z opcji 'przypomnienie hasła' dostępnego na stronie logowania. W formularzu podaj swój adres e-mail i kliknij 'wyślij hasło' nasz system wyśle na zarejestrowany adres hasło do Twojego konta."
         ],
         [
             "nazwa" => "Jak wyszukać interesujące mnie ogłoszenia?",
             "opis" => "Wystarczy skorzystać z Naszej wyszukiwarki, a w niej wybrać interesującą Ciebie kategorię i ewentualnie województwo, w którym poszukujesz pracy."
         ],
         [
             "nazwa" => "Jak zamieścić moje CV?",
             "opis" => "Jeśli chcesz zamieścić swoje CV w Naszej bazie wystarczy wypełnić formularz."
         ],
         [
             "nazwa" => "Czy mogę otrzymywać oferty na maila?",
             "opis" => "Jasne, wystarczy że zaznaczysz odpowiednie pole w formularzu a My zadbamy abyś otrzymywał/a interesujące Cię oferty prosto na Twoją skrzynkę mailową."
         ],
         [
             "nazwa" => "Jak się z Nami skontaktować?",
             "opis" => "Jeśli potrzebujesz dodatkowych informacji lub potrzebujesz dodatkowej pomocy np. przy publikacji ogłoszenia wystarczy, że napiszesz do Nas maila lub zadzwonisz a My postaramy się odpowiedzieć najszybciej jak tylko będzie to możliwe."
         ]
         ];

         foreach($faq as $f) {
             $fq = new Faq();
             $fq->nazwa = $f['nazwa'];
             $fq->opis = $f['opis'];
             $fq->save();
         }

         $ustawienia = [
            [
                'instancja' => 'email',
                'wartosc' => 'serwismedbrain@gmail.com',
            ],
            [
                'instancja' => 'telefon',
                'wartosc' => '+48 501 505 545',
            ],
            [
                'instancja' => 'nazwa',
                'wartosc' => 'Medbrain.in',
            ],
            [
                'instancja' => 'opis',
                'wartosc' => 'Opis strony',
            ],
            [
                'instancja' => 'keywords',
                'wartosc' => 'medbrain, serwis, portal',
            ],
            [
                'instancja' => 'firma',
                'wartosc' => 'Medbrain.in',
            ],
            [
                'instancja' => 'adres',
                'wartosc' => 'Kaszubska 34/2, 70 - 227 Szczecin',
            ],
            [
                'instancja' => 'nip',
                'wartosc' => '8531515066',
            ],
            [
                'instancja' => 'analytics',
                'wartosc' => 'UA-xxx',
            ],
            [
                'instancja' => 'facebook api',
                'wartosc' => '',
            ],
            [
                'instancja' => 'facebook url',
                'wartosc' => 'facebook.com',
            ],
            [
                'instancja' => 'mails',
                'wartosc' => '1',
            ],
            [
                'instancja' => 'maintenance',
                'wartosc' => '0',
            ],
         ];

foreach($ustawienia as $u) {
    $us = new Ustawienia(); 

    $us->instancja = $u['instancja'];
    $us->wartosc = $u['wartosc'];
    $us->save();
}

    $oClient = new Client();
    //$oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oClient->setConfig('mjendraszczyk', 'Di9!neTp78', true);
    $oNativeApi = NativeApi::create($oClient);
    $oNativeApi->CzyZalogowany();

    //var_dump($oNativeApi->PobierzListeWojewodztw());
    foreach ($oNativeApi->PobierzListeWojewodztw() as $wojewodztwo) {

        /* Wojewodztwo */
        $checkIfExist = Wojewodztwo::where('nazwa', $wojewodztwo->name)->where('id_distinct', $wojewodztwo->provinceId)->count();

        if($checkIfExist== 0) {
            $w = new Wojewodztwo();
            $w->nazwa = $wojewodztwo->name;
            $w->id_distinct = $wojewodztwo->provinceId;
            $w->id_kraje = '1';
            $w->save();
        }
        //var_dump($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId));

        foreach ($oNativeApi->PobierzListePowiatow($wojewodztwo->provinceId) as $powiat) {
            //dd($powiat);
            //dd($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId));
            foreach ($oNativeApi->PobierzListeGmin($powiat->provinceId, $powiat->districtId) as $gmina) {
                if (($gmina->typeName != 'miasto') && ($gmina->typeName != 'gmina miejska')){
                $checkIfExist = Miasto::where('nazwa',$gmina->name)->where('id_wojewodztwa', $w->id_wojewodztwa)->count();
                if ($checkIfExist == 0) {
                    $m = new Miasto();
                    $m->nazwa = $gmina->name;
                    $m->id_wojewodztwa = $w->id_wojewodztwa;//$gmina->provinceId;
                    $m->save();
                }

                    //dd($gmina);
                }
            }
        }
    }

    // $waw = new Miasto();
    // $waw->nazwa = "Warszawa";
    // $waw->id_wojewodztwa = "14";
    // $waw->save();
 

    }
}
