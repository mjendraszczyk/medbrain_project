(function ($) {
  $(function () {

    $('.sidenav').sidenav();

  }); // end of document ready
})(jQuery); // end of jQuery name space


// new Vue({
//   el: '#app',
//   created() {
//     this.fetchData();
//   },
//   data: {
//     posts: []
//   },
//   methods: {
//     fetchData() {
//       axios.get('https://jsonplaceholder.typicode.com/posts').then(response => {
//         this.posts = response.data;
//       });
//     }
//   }
// });

/* <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>//vuejsexamples.net</title>
    </head>
  <body>
    <div id="app">
      Posts:
      <li v-for="post of posts">
      <strong>{{post.title}}</strong>
      {{post.body}}
      </li>
    </div>
  </body>
</html> */

function goToByScroll(id) {
  // Reove "link" from the ID
  //id = id.replace("link", "");
  // Scroll
  $('html,body').animate({
    scrollTop: $("#" + id).offset().top+500
  },
    'slow');
  // var x = $("#"+id).position(); //gets the position of the div element...
  // window.scrollTo(x.left, x.top);
}

$("#aplikuj_btn").click(function (e) {
  //alert("B");
  // Prevent a page reload when a link is pressed
  e.preventDefault();
  // Call the scroll function
  goToByScroll($(this).attr("id"));
});
$(document).ready(function () {

$("#zmien_haslo_toggle").click(function () {
  //$('.password_box').toggle();
  // alert($(this).hasClass('active'));
  if ($(this).hasClass('active') == true) {
    $(this).removeClass('active');
    $('.password_box').css('display', 'none');
  } else {
    $(this).addClass('active');
    $('.password_box').css('display','block');
  }
  //alert("G");
});
});