<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ' Pole :attribute należy zaakceptować',
    'active_url' => ' Pole :attribute nie jest poprawnym URL',
    'after' => ' Pole :attribute must be a date after :date.',
    'after_or_equal' => ' Pole :attribute musi byc datą późniejszą lub równą :date.',
    'alpha' => ' Pole :attribute moze zawierac tylko litery.',
    'alpha_dash' => ' Pole :attribute moze zawierac litery, cyfry, myslniki, , dashes and kropki.',
    'alpha_num' => ' Pole :attribute moze zawierac tylko litery i cyfry.',
    'array' => ' Pole :attribute musi byc tablicą',
    'before' => ' Pole :attribute musi byc datą wczesniejszą niz :date.',
    'before_or_equal' => ' Pole :attribute musi byc datą wczesniejsza lub rowna :date.',
    'between' => [
        'numeric' => ' Pole :attribute musi byc pomiędzy :min i :max.',
        'file' => ' Pole :attribute must be between :min and :max kilobytes.',
        'string' => ' Pole :attribute must be between :min and :max characters.',
        'array' => ' Pole :attribute must have between :min and :max items.',
    ],
    'boolean' => ' Pole :attribute musi zawierac wartosc 1 lub 0.',
    'confirmed' => ' Pole :attribute nie jest potwierdzony.',
    'date' => ' Pole :attribute nie jest poprawną datą.',
    'date_equals' => ' Pole :attribute must be a date equal to :date.',
    'date_format' => ' Pole :attribute does not match the format :format.',
    'different' => ' Pole :attribute and :other must be different.',
    'digits' => ' Pole :attribute must be :digits digits.',
    'digits_between' => ' Pole :attribute must be between :min and :max digits.',
    'dimensions' => ' Pole :attribute has invalid image dimensions.',
    'distinct' => ' Pole :attribute field has a duplicate value.',
    'email' => ' Pole :attribute musi byc poprawnym adresem e-mail.',
    'ends_with' => ' Pole :attribute must end with one of the following: :values',
    'exists' => 'The selected Pole :attribute is invalid.',
    'file' => ' Pole :attribute must be a file.',
    'filled' => ' Pole :attribute field must have a value.',
    'gt' => [
        'numeric' => ' Pole :attribute must be greater than :value.',
        'file' => ' Pole :attribute must be greater than :value kilobytes.',
        'string' => ' Pole :attribute must be greater than :value characters.',
        'array' => ' Pole :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => ' Pole :attribute must be greater than or equal :value.',
        'file' => ' Pole :attribute must be greater than or equal :value kilobytes.',
        'string' => ' Pole :attribute must be greater than or equal :value characters.',
        'array' => ' Pole :attribute must have :value items or more.',
    ],
    'image' => ' Pole :attribute musi byc obrazkiem.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => ' Pole :attribute field does not exist in :other.',
    'integer' => ' Pole :attribute musi byc liczbą.',
    'ip' => ' Pole :attribute must be a valid IP address.',
    'ipv4' => ' Pole :attribute must be a valid IPv4 address.',
    'ipv6' => ' Pole :attribute must be a valid IPv6 address.',
    'json' => ' Pole :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => ' Pole :attribute must be less than :value.',
        'file' => ' Pole :attribute must be less than :value kilobytes.',
        'string' => ' Pole :attribute must be less than :value characters.',
        'array' => ' Pole :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => ' Pole :attribute must be less than or equal :value.',
        'file' => ' Pole :attribute must be less than or equal :value kilobytes.',
        'string' => ' Pole :attribute must be less than or equal :value characters.',
        'array' => ' Pole :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => ' Pole :attribute nie moze miec wiecej niz :max.',
        'file' => ' Pole :attribute may not be greater than :max kilobytes.',
        'string' => ' Pole :attribute nie moze miec więcej niz :max znaków.',
        'array' => ' Pole :attribute may not have more than :max items.',
    ],
    'mimes' => ' Pole :attribute must be a file of type: :values.',
    'mimetypes' => ' Pole :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => ' Pole :attribute musi miec minimalnie :min.',
        'file' => ' Pole :attribute must be at least :min kilobytes.',
        'string' => ' Pole :attribute musi miec conajmniej :min znaków.',
        'array' => ' Pole :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => ' Pole :attribute format is invalid.',
    'numeric' => ' Pole :attribute musi być liczbą',
    'present' => ' Pole :attribute field must be present.',
    'regex' => ' Pole :attribute format is invalid.',
    'required' => ' Pole :attribute jest wymagane.',
    'required_if' => ' Pole :attribute jest wymagany kiedy :other ma wartosc :value.',
    'required_unless' => ' Pole :attribute jest wymagany kiedy :other ma wartosc mniejsza niz :values.',
    'required_with' => ' Pole :attribute field is required when :values is present.',
    'required_with_all' => ' Pole :attribute field is required when :values are present.',
    'required_without' => ' Pole :attribute field is required when :values is not present.',
    'required_without_all' => ' Pole :attribute field is required when none of :values are present.',
    'same' => ' Pole :attribute and :other must match.',
    'size' => [
        'numeric' => ' Pole :attribute musi miec rozmiar :size.',
        'file' => ' Pole :attribute must be :size kilobytes.',
        'string' => ' Pole :attribute must be :size characters.',
        'array' => ' Pole :attribute must contain :size items.',
    ],
    'starts_with' => ' Pole :attribute must start with one of the following: :values',
    'string' => ' Pole :attribute must be a string.',
    'timezone' => ' Pole :attribute must be a valid zone.',
    'unique' => ' Ten :attribute jest już zajęty',
    'uploaded' => ' Pole :attribute failed to upload.',
    'url' => ' Pole :attribute nie zawiera poprawnego formatu',
    'uuid' => ' Pole :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'poziom'  => 'stanowisko',
        'tresc'  => 'treść',
        'email_register' => 'email',
        'password_register' => 'hasło'

    ],

];
