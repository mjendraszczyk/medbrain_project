@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa kraju</label>


    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_poziom_edit' )
        value="{{$poziom->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">


</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>