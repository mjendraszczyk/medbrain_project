@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Miasta
      <a href="{{route('backend_miasta_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Wojewodztwo</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_miasta_filter')}}">
              @csrf
              <th>
                <input id="nazwa" type="text" class="@error('email') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('miasta_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <select name="id_wojewodztwa">
                  @foreach(App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                  <option value="{{$wojewodztwo->id_wojewodztwa}}" @if($wojewodztwo->id_wojewodztwa ==
                    Session::get('miasto_id_wojewodztwa'))
                    selected="selected"
                    @endif>
                    {{$wojewodztwo->nazwa}}</option>
                  @endforeach
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $miasto)
          <tr>

            <td>{{$miasto->nazwa}}</td>

            <td>
              {{App\Http\Controllers\Controller::getWojewodztwo($miasto->id_wojewodztwa)}}</td>

            <td>
              <a href="{{route('backend_miasta_edit',['id'=>$miasto->id_miasta])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_miasta_delete',['id'=>$miasto->id_miasta])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>

          </tr>
          @endforeach
        </tbody>

      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection