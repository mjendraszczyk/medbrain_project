@extends('layouts.medbrain_backend')

@section('content')
<div class="container">
    <div class="card">
        @include('backend.kraje.header')
        <div class="card-body">
            <form method="POST" action="{{route('backend_kraje_store')}}">
                @include('backend.kraje.form')
            </form>
        </div>
    </div>
</div>
@endsection