@extends('layouts.medbrain_backend')


@section('content')
<div class="container dashboard">
  <div class="row">
    <div class="col s12 m6">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Dodaj Ogłoszenie</span>
          <i class="material-icons">
            add
          </i>
        </div>
        <div class="card-action">
          <a href="{{route('backend_ogloszenia_create')}}" class="btn btn-default">Dodaj ogłoszenie</a>
        </div>
      </div>
    </div>

    <div class="col s12 m6">
      <div class="card green">
        <div class="card-content white-text">
          <span class="card-title">Dodaj Podmiot</span>
          <i class="material-icons">
            business_center
          </i>
        </div>
        <div class="card-action">
          <a href="{{route('backend_podmiot_create')}}" class="btn btn-default">Dodaj podmiot</a>
        </div>
      </div>
    </div>

    <div class="col s12 m6">
      <div class="card white">
        <div class="card-content">
          <span class="card-title">Strony CMS</span>
          <i class="material-icons">
            post_add
          </i>
        </div>
        <div class="card-action">
          <a href="{{route('backend_cms_index')}}" class="btn btn-default">Zarządzaj stronami</a>
        </div>
      </div>

    </div>

    <div class="col s12 m6">
      <div class="card blue">
        <div class="card-content white-text">
          <span class="card-title">Użytkownicy</span>
          <i class="material-icons">
            people_alt
          </i>
        </div>
        <div class="card-action">
          <a href="{{route('backend_uzytkownicy_index')}}" class="btn btn-default">Zarządzaj użytkownikami</a>
        </div>
      </div>

    </div>

  </div>
</div>
@endsection