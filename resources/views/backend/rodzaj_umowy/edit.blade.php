@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Rodzaj umowy
            <a href="{{route('backend_rodzaj_umowy_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST"
                action="{{route('backend_rodzaj_umowy_update', ['id' => $rodzaj_umowy->id_rodzaj_umowy])}}">
                @method('PUT')
                @include('backend.rodzaj_umowy.form')
            </form>
        </div>
    </div>
</div>
@endsection