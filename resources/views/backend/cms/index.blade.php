@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Strony CMS
      <a href="{{route('backend_cms_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Tytuł</th>
            <th>Stan</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_cms_filter')}}">
              @csrf
              <th>
                <input id="tytul" type="text" class="@error('tytul') is-invalid @enderror" name="tytul"
                  value="{{ Session::get('cms_tytul') }}" autocomplete="no" placeholder="Tytuł" autofocus>
              </th>
              <th>
                <input id="stan" type="text" class="@error('stan') is-invalid @enderror" name="stan"
                  value="{{ Session::get('cms_stan') }}" autocomplete="no" placeholder="Stan" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $cms)
          <tr>
            <td>{{$cms->tytul}}</td>
            <td>{{$cms->stan}}</td>
            <td>
              <a href="{{route('backend_cms_edit',['id'=>$cms->id_cms])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_cms_delete',['id'=>$cms->id_cms])}}">
                @csrf
                @method('DELETE')
                <button type="submit" disabled class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection