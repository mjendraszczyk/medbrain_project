@extends('layouts.medbrain_backend')

@section('content')
<div class="container">
    <div class="card">
        @include('backend.cms.header')
        <div class="card-body">
            <form method="POST" action="{{route('backend_cms_store')}}">
                @include('backend.cms.form')
            </form>
        </div>
    </div>
</div>
@endsection