@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Płatności
            <a href="{{route('backend_platnosci_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_platnosci_update', ['id' => $platnosci->id_platnosci])}}">
                @method('PUT')
                @include('backend.platnosci.form')
            </form>
        </div>
    </div>
</div>
@endsection