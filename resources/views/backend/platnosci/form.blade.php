@csrf
<div class="col s12 m12 l12">


    <label>Specjalizacje</label>

    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_specjalizacje_edit' )
        value="{{$specjalizacje->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">


    <label>Kolor</label>
    <input required placeholder="Kolor" style="width:100%;" name="kolor"
        @if(Route::currentRouteName()=='backend_specjalizacje_edit' ) value="#{{$specjalizacje->kolor}}" @else value=""
        @endif type="color" class="form-control-input validate">

    <label>Symbol</label>
    <input required placeholder="Symbol" name="symbol" @if(Route::currentRouteName()=='backend_specjalizacje_edit' )
        value="{{$specjalizacje->symbol}}" @else value="" @endif type="text" class="form-control-input validate">
</div>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>