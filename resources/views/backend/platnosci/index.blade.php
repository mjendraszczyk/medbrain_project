@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Płatności
      <a href="{{route('backend_platnosci_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>ID transakcji</th>
            <th>Podmiot</th>
            <th>Data</th>
            <th>Status</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_platnosci_filter')}}">
              @csrf
              <th>
                <input id="id_transakcji" type="text" class="@error('id_transakcji') is-invalid @enderror"
                  name="id_transakcji" value="{{ Session::get('platnosci_id_transakcji') }}" autocomplete="no"
                  placeholder="ID transakcji" autofocus>
              </th>
              <th>
                <input id="podmiot" type="text" class="@error('podmiot') is-invalid @enderror" name="podmiot"
                  value="{{ Session::get('platnosci_podmiot') }}" autocomplete="no" placeholder="Podmiot" autofocus>
              </th>
              <th>
                <input id="data" type="date" class="@error('data') is-invalid @enderror" name="data"
                  value="{{ Session::get('platnosci_data') }}" autocomplete="no" placeholder="Data" autofocus>
              </th>
              <th>
                <select required name="status">
                  <option value="" disabled selected>Status</option>
                  <option value="1">Zapłacone</option>
                  <option value="0">Niezapłacone</option>
                </select>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>

        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection