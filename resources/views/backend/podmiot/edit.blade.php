@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Firma
            <a href="{{route('backend_podmiot_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" enctype="multipart/form-data"
                action="{{route('backend_podmiot_update', ['id' => $podmiot->id_podmiot])}}">
                @method('PUT')
                @if($podmiot->logo_upload != '')
                <img src="{{asset('img/logo')}}/{{$podmiot->logo_upload}}" />
                @endif
                <br>
                <label>Logo</label>
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Plik</span>
                        <input type="file" name="logo_upload">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                @include('backend.podmiot.form')
            </form>
        </div>
    </div>
</div>
@endsection