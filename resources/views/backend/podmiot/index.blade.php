@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Firma
      <a href="{{route('backend_podmiot_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>NIP</th>
            <th>Miasto</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_podmiot_filter')}}">
              @csrf
              <th>
                <input id="nazwa" type="text" class="@error('email') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('podmiot_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <input id="nip" type="text" class="@error('nip') is-invalid @enderror" name="nip"
                  value="{{ Session::get('podmiot_nip') }}" autocomplete="no" placeholder="NIP" autofocus>
              </th>
              <th>
                <select required name="id_rodzaj_placowki">
                  <option value="" disabled selected>Rodzaj placówki</option>
                  @foreach (App\Http\Controllers\Controller::getRodzajPlacowki(null) as $rodzaj_placowki)
                  <option value="{{$rodzaj_placowki->id_rodzaj_placowki}}">
                    {{$rodzaj_placowki->nazwa}}
                  </option>
                  {{-- @foreach (App\Http\Controllers\Controller::getMiastaByWojewodztwo($wojewodztwo->id_wojewodztwa) as
                    $miasto)
                    <option value="{{$miasto->id_miasta}}" @if($miasto->id == Session::get('podmiot_id_miasto'))
                  selected="selected"
                  @endif>{{$miasto->nazwa}}</option>
                  @endforeach --}}
                  </optgroup>
                  @endforeach
                </select>


              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $podmiot)
          <tr>
            <td>{{$podmiot->nazwa}}</td>
            <td>{{$podmiot->nip}}</td>
            <td>
              {{App\Http\Controllers\Controller::getRodzajPlacowki($podmiot->id_rodzaj_placowki)->nazwa}}
              {{-- {{App\Http\Controllers\Controller::getMiasto($podmiot->id_miasta)}},
              {{App\Http\Controllers\Controller::getKrajByIdMiasto($podmiot->id_miasta)}} --}}
            </td>
            <td>
              <a href="{{route('backend_podmiot_edit',['id'=>$podmiot->id_podmiot])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_podmiot_delete',['id'=>$podmiot->id_podmiot])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection