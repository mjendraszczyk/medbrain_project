@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa firmy</label>

    <input placeholder="Nazwa firmy" name="nazwa" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->nazwa}}" @else value="{{old('nazwa')}}" @endif type="text" class="form-control-input">

    <label>NIP</label>
    <input placeholder="NIP" name="nip" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->nip}}" @else value="{{old('nip')}}" @endif type="text" class="form-control-input">

    <div class="row">
        <div class="col s12 m4 l4">
            <label>Ulica</label>
            <input placeholder="Ulica" name="ulica" @if(Route::currentRouteName()=='backend_podmiot_edit' )
                value="{{$podmiot->ulica}}" @else value="{{old('ulica')}}" @endif type="text"
                class="form-control-input">
        </div>

        <div class="col s12 m8 l8">
            <label>Województwo</label>
            <select name="id_wojewodztwa" class="zmien_miasta">
                @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                @if(Route::currentRouteName()=='backend_podmiot_edit' )
                @if((App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($podmiot->id_miasta) ==
                $wojewodztwo->id_wojewodztwa) || ($wojewodztwo->id_wojewodztwa == old('id_wojewodztwa')))
                <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
                    @else
                <option value="{{$wojewodztwo->id_wojewodztwa}}">
                    @endif
                    {{$wojewodztwo->nazwa}}
                </option>
                @else
                @if((App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1) ==
                $wojewodztwo->id_wojewodztwa) || (($wojewodztwo->id_wojewodztwa == old('id_wojewodztwa'))))
                <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
                    @else
                <option value="{{$wojewodztwo->id_wojewodztwa}}">
                    @endif
                    {{$wojewodztwo->nazwa}}
                </option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m4 l4">
            <label>Kod pocztowy</label>
            <input placeholder="Kod pocztowy" name="kod_pocztowy" @if(Route::currentRouteName()=='backend_podmiot_edit'
                ) value="{{$podmiot->kod_pocztowy}}" @else value="{{old('kod_pocztowy')}}" @endif type="text"
                class="form-control-input validate">
        </div>
        <div class="col s12 m8 l8">
            <label>Miasto</label>
            <select name="id_miasta" @if((Route::currentRouteName()=='backend_podmiot_edit' ))
                data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($podmiot->id_miasta)}}"
                @endif>
                <option value="" disabled selected>Miasto</option>
                {{-- @foreach ($wojewodztwa as $wojewodztwo) --}}
                @if(Route::currentRouteName()=='backend_podmiot_edit')
                @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($podmiot->id_miasta))
                as
                $miasto)
                <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_podmiot_edit' ) &&
                    ($podmiot->id_miasta
                    == $miasto->id_miasta))
                    selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                @endforeach
                @else
                @if(old('id_wojewodztwa') == '')
                @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1))
                as
                $miasto)
                <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_podmiot_edit' ) &&
                    ($podmiot->id_miasta
                    == $miasto->id_miasta))
                    selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                @endforeach
                @else
                @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(old('id_wojewodztwa')))
                as
                $miasto)
                <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_podmiot_edit' ) &&
                    ($podmiot->id_miasta
                    == $miasto->id_miasta))
                    selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                @endforeach
                @endif
                @endif
            </select>
        </div>
    </div>
    <div class="col s12 m12 l12">
        <label>Rodzaj placówki</label>
        <select name="id_rodzaj_placowki">
            <option value="" disabled selected>Rodzaj placówki</option>
            @foreach ($rodzaje_placowki as $placowka)
            <option value="{{$placowka->id_rodzaj_placowki}}" @if(Route::currentRouteName()=='backend_podmiot_edit' )
                @if(($placowka->id_rodzaj_placowki == $podmiot->id_rodzaj_placowki))
                selected="selected"
                @endif
                @else
                @if($placowka->id_rodzaj_placowki == old('id_rodzaj_placowki'))
                selected="selected"
                @endif
                @endif>{{$placowka->nazwa}}</option>
            @endforeach
        </select>
    </div>
    <label>Telefon</label>
    <input placeholder="Telefon" name="podmiot_telefon" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->podmiot_telefon}}" @else value="{{old('podmiot_telefon')}}" @endif type="text"
        class="form-control-input validate">

    <label>E-mail</label>
    <input placeholder="E-mail" name="podmiot_email" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->podmiot_email}}" @else value="{{old('podmiot_email')}}" @endif type="text"
        class="form-control-input validate">

    <input required placeholder="Lat" id="lat" name="lat" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->lat}}" @else value="" @endif type="hidden" class="form-control-input validate">
    <input required placeholder="Lon" id="lon" name="lon" @if(Route::currentRouteName()=='backend_podmiot_edit' )
        value="{{$podmiot->lon}}" @else value="" @endif type="hidden" class="form-control-input validate">

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>