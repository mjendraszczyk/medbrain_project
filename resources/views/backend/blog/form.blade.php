@csrf
<div class="col s12 m12 l12">

    @include('backend/_main/message')
    <label>Tytuł</label>

    <input required placeholder="Nazwa" name="tytul" @if(Route::currentRouteName()=='backend_blog_edit' )
        value="{{$blog->tytul}}" @else value="{{old('tytul')}}" @endif type="text" class="form-control-input validate">

    <label>Tresc</label>
    <div class="alert alert-info">
        Aby skopiować treść użyj skrótów klawiszowych CTR+C i CTRL+V
    </div>
    @if(Route::currentRouteName()=='backend_blog_edit' )
    <textarea name="tresc" placeholder="Wpisz treść ogloszenia"
        class="editor form-control-area materialize-textarea validate">{{$blog->tresc}}</textarea>
    @else
    <textarea name="tresc" placeholder="Wpisz treść ogloszenia"
        class="editor form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
    @endif

    <label>Obraz</label>
    @if(Route::currentRouteName()=='backend_blog_edit' )
    <img src="{{asset('img/blog/'.$blog->image)}}" class="thumbnail" style="max-width:100%;" />
    @endif
    <div class="file-field input-field">
        <div class="btn">
            <span>Plik</span>
            <input type="file" name="blog_upload">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
        </div>
    </div>
    <button type="submit" class="btn  waves-effect waves-light btn-large">
        Zapisz</button>
</div>