@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')

    <div class="clearfix"></div>
    <label>Specjalizacja</label>
    <select name="id_specjalizacje">
        <option value="" disabled selected>np. dermatologia (wymagane)</option>
        @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
        <option value="{{$specjalizacja->id_specjalizacje}}" @if((Route::currentRouteName()=='backend_ogloszenia_edit' )
            && ($ogloszenia->id_specjalizacje ==
            ($specjalizacja->id_specjalizacje)))
            selected="selected"
            @else
            @if(old('id_specjalizacje') == $specjalizacja->id_specjalizacje)
            selected="selected"
            @endif
            @endif>{{$specjalizacja->nazwa}}</option>
        @endforeach
    </select>
    <div class="clearfix"></div>

    <div class="row">

        <div class="col s12 m6 l3">
            <label>Wynagrodzenie od:</label>
            <input placeholder="Wynagrodzenie od" name="wynagrodzenie_od"
                @if(Route::currentRouteName()=='backend_ogloszenia_edit' ) value="{{$ogloszenia->wynagrodzenie_od}}"
                @else value="{{old('wynagrodzenie_od')}}" @endif type="number" class="form-control-input validate">
        </div>
        <div class="col s12 m6 l3">
            <label>Wynagrodzenie do:</label>
            <input placeholder="Wynagrodzenie do" name="wynagrodzenie_do"
                @if(Route::currentRouteName()=='backend_ogloszenia_edit' ) value="{{$ogloszenia->wynagrodzenie_do}}"
                @else value="{{old('wynagrodzenie_do')}}" @endif type="number" class="form-control-input validate">
        </div>
        <div class="col s12 m12 l3">
            <label>Typ wynagrodzenia:</label>
            <select name="typ_wynagrodzenia">
                <option value="" disabled selected>Typ wynagrodzenia</option>
                @foreach(App\Http\Controllers\Controller::getTypWynagrodzenia(null) as $typ)
                <option value="{{$typ->id_typ_wynagrodzenia}}" @if((Route::currentRouteName()=='backend_ogloszenia_edit'
                    ) && ($typ->id_typ_wynagrodzenia ==
                    ($ogloszenia->typ_wynagrodzenia))) selected="selected" @else
                    @if($typ->id_typ_wynagrodzenia ==
                    old('typ_wynagrodzenia'))
                    selected="selected"
                    @endif
                    @endif>{{$typ->nazwa}}
                </option>
                @endforeach
            </select>
        </div>
        <div class="col s12 m6 l3">
            <label>Waluta wynagrodzenia:</label>
            <select name="waluta_wynagrodzenia" required>
                <option value="" disabled selected>Waluta</option>
                @foreach(App\Http\Controllers\Controller::getWalutaWynagrodzenia(null) as $waluta)
                <option value="{{$waluta->id_waluta_wynagrodzenia}}"
                    @if((Route::currentRouteName()=='backend_ogloszenia_edit' ) &&($waluta->id_waluta_wynagrodzenia ==
                    $ogloszenia->waluta_wynagrodzenia))
                    selected="selected"
                    @endif>{{$waluta->nazwa}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row" style="display:none;">
        <div class="col s12 m6 l6">
            <label>Doświadczenie od:</label>

            <input placeholder="Doswiadczenie od" name="doswiadczenie_od"
                @if(Route::currentRouteName()=='backend_ogloszenia_edit' ) value="{{$ogloszenia->doswiadczenie_od}}"
                @else value="{{old('doswiadczenie_od')}}" @endif type="text" class="form-control-input validate">
        </div>

        <div class="col s12 m6 l6">
            <label>Doświadczenie do:</label>

            <input placeholder="Doswiadczenie do" name="doswiadczenie_do"
                @if(Route::currentRouteName()=='backend_ogloszenia_edit' ) value="{{$ogloszenia->doswiadczenie_do}}"
                @else value="{{old('doswiadczenie_do')}}" @endif type="text" class="form-control-input validate">
        </div>

    </div>
    <label>Podmiot</label>
    <select name="id_podmiot">
        <option value="" disabled selected>Pomiot</option>
        @foreach ((App\Http\Controllers\Controller::getPodmiot(null)) as $podmiot)
        <option value="{{$podmiot->id_podmiot}}" @if(Route::currentRouteName()=='backend_ogloszenia_edit' )
            selected="{{$podmiot->id_podmiot}}" @else @if($podmiot->id_podmiot == old('id_podmiot'))
            selected="selected"
            @endif
            @endif>#{{$podmiot->id_podmiot}} {{$podmiot->nazwa}},
            {{$podmiot->ulica}},{{App\Http\Controllers\Controller::getMiasto($podmiot->id_miasta)}}
        </option>
        @endforeach
    </select>

    <div class="clearfix"></div>
    <label>Poziom</label>
    <select name="id_poziom">
        <option value="" disabled selected>Poziom</option>
        @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
        <option value="{{$poziom->id_poziom}}" @if((Route::currentRouteName()=='backend_ogloszenia_edit' ) && ($poziom->
            id_poziom ==
            $ogloszenia->id_poziom))
            selected="selected"
            @else
            @if($poziom->id_poziom == old('id_poziom'))
            selected="selected"
            @endif
            @endif>{{$poziom->nazwa}}</option>
        @endforeach
    </select>
    <div class="clearfix"></div>
    <label>Typ umowy</label>
    <select name="id_rodzaj_umowy">
        <option value="" disabled selected>Rodzaj umowy</option>
        @foreach(App\Http\Controllers\Controller::getRodzajUmowy(null) as $rodzaj)
        <option value="{{$rodzaj->id_rodzaj_umowy}}" @if((Route::currentRouteName()=='backend_ogloszenia_edit' ) &&
            ($rodzaj->id_rodzaj_umowy ==
            $ogloszenia->id_rodzaj_umowy))
            selected="selected"
            @else
            @if($rodzaj->id_rodzaj_umowy == old('id_rodzaj_umowy'))
            selected="selected"
            @endif
            @endif>{{$rodzaj->nazwa}}</option>
        @endforeach
    </select>
    <div class="clearfix"></div>
    <label>Wymiar pracy</label>
    <select name="id_wymiar_pracy">
        <option value="" disabled selected>Wymiar pracy</option>
        @foreach(App\Http\Controllers\Controller::getWymiarPracy(null) as $wymiar)
        <option value="{{$wymiar->id_wymiar_pracy}}" @if((Route::currentRouteName()=='backend_ogloszenia_edit' ) &&
            ($wymiar->id_wymiar_pracy ==
            $ogloszenia->id_wymiar_pracy))
            selected="selected"
            @else
            @if($wymiar->id_wymiar_pracy == old('id_wymiar_pracy'))
            selected="selected"
            @endif
            @endif>{{$wymiar->nazwa}}</option>
        @endforeach
    </select>
    <div class="clearfix"></div>
    <label>Treść</label>
    <div class="alert alert-info">
        Aby skopiować treść użyj skrótów klawiszowych CTR+C i CTRL+V
    </div>
    @if(Route::currentRouteName()=='backend_ogloszenia_edit' )
    <textarea name="tresc" class="editor" placeholder="Wpisz treść ogloszenia"
        class="form-control-area materialize-textarea validate">{{$ogloszenia->tresc}}</textarea>
    @else
    <textarea name="tresc" class="editor" placeholder="Wpisz treść ogloszenia"
        class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
    @endif
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>