@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Ogłoszenia
            <a href="{{route('backend_podmiot_create')}}" class="btn waves-effect waves-light btn-third"
                style="background: #2d3040;margin:0;">
                <i class="material-icons">business_center</i>
                Dodaj firmę</a>

            <a target="_blank" href="{{route('ogloszenia_zobacz',['id_ogloszenia'=>$ogloszenia->id_ogloszenia, 'nazwa'=>str_slug($ogloszenia->nazwa)])}}"
                class="btn waves-effect waves-light btn-third">
                <i class="material-icons">remove_red_eye</i>
                Podgląd</a>
            <a href="{{route('backend_ogloszenia_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        {{-- <div class="card-body">
            <form method="POST" action="{{route('backend_ogloszenia_update', ['id' => $ogloszenia->id_ogloszenia])}}">
                @method('PUT')
                @include('backend.ogloszenia.form')
            </form>
            <h6 style="font-weight: bold;font-size: 1.5rem;margin: 20px 0;">Domyślna lokalizacja
                ogłoszenia:</h6>
            <ul>
                <li>
                    {{App\Http\Controllers\Controller::getPodmiotAddress($ogloszenia->id_podmiot)}}
                </li>
            </ul>
            <h6 style="font-weight: bold;font-size: 1.5rem;margin: 20px 0;">Lista dodatkowych lokalizacji
                ogłoszenia:</h6>
            <ul>
                @foreach($dodatkowe_adresy as $adres)
                <li style="height:50px;">
                    <form method="POST"
                        action="{{route('delete_dodatkowe_lokalizacje',['id'=>$adres->id_dodatkowe_adresy])}}">
                        @csrf
                        @method('DELETE')
                        {{$adres->nazwa}}, ({{$adres->lat_dodatkowy_adres}},{{$adres->lon_dodatkowy_adres}})
                        <button type="submit" class="btn waves-effect waves-light btn-danger" style="float:right;">
                            <i class="material-icons">
                                restore_from_trash
                            </i></button></form>
                </li>
                @endforeach
            </ul>

            <div class="dodatkowe_lokalizacje">
                <form method="POST"
                    action="{{route('store_dodatkowe_lokalizacje',['id_ogloszenia'=>$ogloszenia->id_ogloszenia])}}">
                    @csrf
                    @method('POST')
                    <label>Adres</label>
                    <input value="" placeholder="Ulica" name="ulica" type="text" class="form-control-input">
                    <input value="" placeholder="Miasto" name="miasto" type="text" class="form-control-input">

                    <label>Wybierz lokalizacje</label>
                    <div id="mapLatLon"></div>

                    <input value="" id="lat" name="lat" type="hidden" class="form-control-input">
                    <input value="" id="lon" name="lon" type="hidden" class="form-control-input">
            </div>

            <button type="submit" class="dodaj_lokalizacje btn waves-effect waves-light btn-large green min-200">Dodaj
                dodatkową lokalizację</button>
            </form>
        </div> --}}
    </div>
</div>
@endsection