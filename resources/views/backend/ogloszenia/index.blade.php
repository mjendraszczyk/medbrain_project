@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Ogłoszenia
      <a href="{{route('backend_ogloszenia_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Imię nazwisko</th>
            <th>Email</th>
            <th>Podmiot</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_ogloszenia_filter')}}">
              @csrf
              <th>
                <input id="imie_nazwisko" type="text" class="@error('imie_nazwisko') is-invalid @enderror"
                  name="imie_nazwisko" value="{{ Session::get('ogloszenia_imie_nazwisko') }}" autocomplete="no"
                  placeholder="Imię i nazwisko" autofocus>
              </th>
              <th>
                <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email"
                  value="{{ Session::get('ogloszenia_email') }}" autocomplete="no" placeholder="E-mail" autofocus>
              </th>
              <th>
                <input id="id_podmiot" type="text" class="@error('id_podmiot') is-invalid @enderror" name="id_podmiot"
                  value="{{ Session::get('ogloszenia_id_podmiot') }}" autocomplete="no" placeholder="Podmiot" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $ogloszenie)
          <tr>
            <td>
              {{(App\Http\Controllers\Controller::getUzytkownikName($ogloszenie->id_user))}}
            </td>
            <td>
              {{App\Http\Controllers\Controller::getPodmiotMail($ogloszenie->id_podmiot)}}
            </td>
            <td>
              {{App\Http\Controllers\Controller::getPodmiot($ogloszenie->id_podmiot)}}
            </td>
            <td>
              <a href="{{route('backend_ogloszenia_edit',['id'=>$ogloszenie->id_ogloszenia])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_ogloszenia_delete',['id'=>$ogloszenie->id_ogloszenia])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection