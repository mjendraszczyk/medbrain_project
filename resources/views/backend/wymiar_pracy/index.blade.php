@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Wymiar pracy
      <a href="{{route('backend_wymiar_pracy_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_wymiar_pracy_filter')}}">
              @csrf
              <th class="first-td">
                <input id="nazwa" type="text" class="@error('email') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('wymiar_pracy_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $wymiar_pracy)
          <tr>
            <td>{{$wymiar_pracy->nazwa}}</td>

            <td>
              <a href="{{route('backend_wymiar_pracy_edit',['id'=>$wymiar_pracy->id_wymiar_pracy])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST"
                action="{{route('backend_wymiar_pracy_delete',['id'=>$wymiar_pracy->id_wymiar_pracy])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection