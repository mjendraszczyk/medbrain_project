@include('frontend/_main/head')

<body class="backoffice">
  <nav class="white lighten-1" role="navigation">
    <div class="nav-wrapper"><a id="logo-container" href="{{route('backend_index')}}" class="brand-logo"><img
          src="{{asset('img/logo-header.png')}}" /></a>
      @include('frontend/_main/nav')

      <ul id="nav-mobile" class="sidenav admin">
        <div class="slideNavContent admin">
          <a href="{{route('backend_index')}}"><img src="{{asset('img/logo-header.png')}}" /></a>
          @include('backend/_main/menu')
        </div>
      </ul>

    </div>
  </nav>