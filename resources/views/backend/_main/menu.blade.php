<ul class="collapsible">
    <li class="collapsible-header">
        <div><a href="#">
                <i class="material-icons">
                    message
                </i>
                Ogłoszenia</a></div>
        <div class="collapsible-body">
            <ul>
                <li><a href="{{route('backend_ogloszenia_index')}}">Zarządzaj ogłoszeniami</a></li>
                <li><a href="{{route('backend_wymiar_pracy_index')}}">Wymiar pracy</a></li>
                <li><a href="{{route('backend_rodzaj_placowki_index')}}">Rodzaj placówki</a></li>
                <li><a href="{{route('backend_rodzaj_umowy_index')}}">Rodzaj umowy</a></li>
                <li><a href="{{route('backend_poziom_index')}}">Poziomy</a></li>
            </ul>
        </div>
    </li>
</ul>


<ul class="collapsible">
    <li class="collapsible-header">
        <div><a href="#">
                <i class="material-icons">
                    people_outline
                </i>
                Użytkownicy</a></div>
        <div class="collapsible-body">
            <ul>
                <li><a href="{{route('backend_uzytkownicy_index')}}">Zarządzaj użytkownikami</a></li>
                <li><a href="{{route('backend_tytuly_index')}}">Tytuły</a></li>
                <li><a href="{{route('backend_specjalizacje_index')}}">Specjalizacje</a></li>
            </ul>
        </div>
    </li>
</ul>


<ul class="collapsible">
    <li class="collapsible-header">
        <div><a href="#">
                <i class="material-icons">
                    business_center
                </i>
                Firmy</a></div>
        <div class="collapsible-body">
            <ul>
                <li><a href="{{route('backend_podmiot_index')}}">Zarządzaj podmiotami</a></li>
                <li><a href="{{route('backend_platnosci_index')}}">Płatności</a></li>
            </ul>
        </div>
    </li>
</ul>

<ul class="collapsible">
    <li class="collapsible-header">
        <div><a href="#">
                <i class="material-icons">
                    map
                </i>Lokalizacja</a></div>
        <div class="collapsible-body">
            <ul>
                <li><a href="{{route('backend_miasta_index')}}">Miasta</a></li>
                <li><a href="{{route('backend_wojewodztwa_index')}}">Województwa</a></li>
                <li><a href="{{route('backend_kraje_index')}}">Kraje</a></li>
            </ul>
        </div>
    </li>
</ul>

<ul class="collapsible">
    <li class="collapsible-header">
        <div><a href="#">
                <i class="material-icons">
                    web
                </i>Witryna</a></div>
        <div class="collapsible-body">
            <ul>
                <li><a href="{{route('backend_cms_index')}}">Strony CMS</a></li>
                <li><a href="{{route('backend_faq_index')}}">FAQ</a></li>
                <li><a href="{{route('backend_blog_index')}}">Blog</a></li>
                <li><a href="{{route('backend_ustawienia_index')}}">Informacje o witrynie</a></li>
            </ul>
        </div>
    </li>
</ul>