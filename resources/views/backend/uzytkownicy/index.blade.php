@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Użytkownicy
      <a href="{{route('backend_uzytkownicy_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_uzytkownicy_filter')}}">
              @csrf
              <th>
                <input id="name" type="text" class="@error('email') is-invalid @enderror" name="name"
                  value="{{ Session::get('name') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email"
                  value="{{ Session::get('email') }}" autocomplete="no" placeholder="E-mail" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $uzytkownik)
          <tr>
            <td>{{$uzytkownik->name}}</td>
            <td>{{$uzytkownik->email}}</td>
            <td>
              <a href="{{route('backend_uzytkownicy_edit',['id'=>$uzytkownik->id])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_uzytkownicy_delete',['id'=>$uzytkownik->id])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection