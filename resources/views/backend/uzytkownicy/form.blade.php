@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa użytkownika</label>
    @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )

    @endif
    <input required placeholder="Nazwa" name="name" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->name}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Email</label>
    <input required placeholder="E-mail" name="email" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->email}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Hasło</label>
    <input placeholder="Hasło" name="password" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' ) value=""
        @else value="" @endif type="password" class="form-control-input validate">
    <label>Powtórz hasło</label>
    <input placeholder="Powtórz hasło" name="password_repeat" @if(Route::currentRouteName()=='backend_uzytkownicy_edit'
        ) value="" @else value="" @endif type="password" class="form-control-input validate">

    <label>Rola</label>
    <select name="id_rola">
        <option value="" disabled selected>rola</option>
        @foreach(App\Http\Controllers\Controller::getRole(null) as $rola)
        <option value="{{$rola->id_rola}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ) &&
            ($uzytkownicy->id_rola ==
            $rola->id_rola))
            selected="selected"
            @else
            @endif>{{$rola->nazwa}}</option>
        @endforeach
    </select>
    <label>Specjalizacja</label>
    <select name="id_specjalizacje">
        <option value="" disabled selected>np. dermatologia (wymagane)</option>
        @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
        <option value="{{$specjalizacja->id_specjalizacje}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit'
            ) && ($uzytkownicy->id_specjalizacje ==
            ($specjalizacja->id_specjalizacje)))
            selected="selected"
            @else
            @endif>{{$specjalizacja->nazwa}}</option>
        @endforeach
    </select>
    <label>Wojewodztwo</label>
    <select name="id_wojewodztwa" class="zmien_miasta">
        @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
        @if(Route::currentRouteName()=='backend_podmiot_edit' )
        @if(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($podmiot->id_miasta) ==
        $wojewodztwo->id_wojewodztwa)
        <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
            @else
        <option value="{{$wojewodztwo->id_wojewodztwa}}">
            @endif
            {{$wojewodztwo->nazwa}}
        </option>
        @else
        @if(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1) ==
        $wojewodztwo->id_wojewodztwa)
        <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
            @else
        <option value="{{$wojewodztwo->id_wojewodztwa}}">
            @endif
            {{$wojewodztwo->nazwa}}
        </option>
        @endif
        @endforeach
    </select>

    <label>Miasto</label>
    <select required name="id_miasta" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ))
        data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($uzytkownicy->id_miasta)}}"
        @endif>
        <option value="" disabled selected>Miasto</option>
        {{-- @foreach ($wojewodztwa as $wojewodztwo) --}}
        @if(Route::currentRouteName()=='backend_uzytkownicy_edit')
        @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($uzytkownicy->id_miasta))
        as
        $miasto)
        <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ) &&
            ($uzytkownicy->id_miasta
            == $miasto->id_miasta))
            selected="selected" @else @endif>{{$miasto->nazwa}}</option>
        @endforeach
        @else
        @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1))
        as
        $miasto)
        <option value="{{$miasto->id_miasta}}" @if((Route::currentRouteName()=='backend_uzytkownicy_edit' ) &&
            ($podmiot->id_miasta
            == $miasto->id_miasta))
            selected="selected" @else @endif>{{$miasto->nazwa}}</option>
        @endforeach
        @endif
    </select>

    <div class="col s12 m12 l12">
        <label>Podmiot</label>
        <select name="id_podmiot">
            <option value="" disabled selected>Podmiot</option>
            @foreach ($podmioty as $podmiot)
            <option value="{{$podmiot->id_podmiot}}" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
                @if(($uzytkownicy->id_podmiot == $podmiot->id_podmiot))
                selected="selected"
                @endif
                @else
                @if($podmiot->id_podmiot == old('id_podmiot'))
                selected="selected"
                @endif
                @endif>#{{$podmiot->id_podmiot}} {{$podmiot->nazwa}} {{$podmiot->podmiot_email}}</option>
            @endforeach
        </select>
    </div>

    <div class="clearfix"></div>
    <label>Status</label>
    <select name="stan">
        @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        <option value="1" @if($uzytkownicy->stan == '1') selected="selected" @endif>Włączone</option>
        <option value="0" @if($uzytkownicy->stan != '1') selected="selected" @endif>Wyłączone</option>
        @else
        <option value="1">Włączone</option>
        <option value="0" selected="selected">Wyłączone</option>
        @endif
    </select>
    <label>Wyswietlic w wynikach w serwisie</label>
    <select name="szukam">
        @if(Route::currentRouteName()=='backend_uzytkownicy_create' )
        <option value="1">Szukam pracy (pokaż profil na
            portalu)</option>
        <option value="0">Nie szukam (ukryj profil na portalu)</option>
        @endif
        @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        <option value="1" @if($uzytkownicy->szukam == '1') selected="selected" @endif>Szukam pracy (pokaż profil na
            portalu)</option>
        <option value="0" @if($uzytkownicy->szukam != '1') selected="selected" @endif>Nie szukam (ukryj profil na
            portalu)</option>
        @endif

    </select>
    <input required placeholder="Lat" id="lat" name="lat" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->lat}}" @else value="" @endif type="hidden" class="form-control-input validate">
    <input required placeholder="Lon" id="lon" name="lon" @if(Route::currentRouteName()=='backend_uzytkownicy_edit' )
        value="{{$uzytkownicy->lon}}" @else value="" @endif type="hidden" class="form-control-input validate">
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>