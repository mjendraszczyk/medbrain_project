@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Użytkownicy
            <a href="{{route('backend_uzytkownicy_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_uzytkownicy_store')}}">
                @include('backend.uzytkownicy.form')
            </form>
        </div>
    </div>
</div>
@endsection