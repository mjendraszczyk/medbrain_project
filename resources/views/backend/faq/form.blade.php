@csrf
<div class="col s12 m12 l12">


    <label>Nazwa</label>

    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_faq_edit' )
        value="{{$faq->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Opis</label>
    @if(Route::currentRouteName()=='backend_faq_edit' )
    <textarea name="opis" required placeholder="Wpisz treść ogloszenia"
        class="form-control-area materialize-textarea validate">{{$faq->opis}}</textarea>
    @else
    <textarea name="opis" required placeholder="Wpisz treść ogloszenia"
        class="form-control-area materialize-textarea validate">{{old('opis')}}</textarea>
    @endif
</div>
</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>