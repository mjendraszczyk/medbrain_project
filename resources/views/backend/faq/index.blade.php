@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      FAQ
      <a href="{{route('backend_faq_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Opis</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_faq_filter')}}">
              @csrf
              <th>
                <input id="nazwa" type="text" class="@error('nazwa') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('faq_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <input id="opis" type="text" class="@error('opis') is-invalid @enderror" name="opis"
                  value="{{ Session::get('faq_opis') }}" autocomplete="no" placeholder="Opis" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $faq)
          <tr>

            <td>{{$faq->nazwa}}</td>

            <td>{{$faq->opis}}</td>

            <td>
              <a href="{{route('backend_faq_edit',['id'=>$faq->id_faq])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_faq_delete',['id'=>$faq->id_faq])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection