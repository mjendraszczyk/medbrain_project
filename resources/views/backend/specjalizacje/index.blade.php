@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Specjalizacje
      <a href="{{route('backend_specjalizacje_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Symbol</th>
            <th>Nazwa</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_specjalizacje_filter')}}">
              @csrf
              <th>
                <input id="symbol" type="text" class="@error('symbol') is-invalid @enderror" name="symbol"
                  value="{{ Session::get('specjalizacje_symbol') }}" autocomplete="no" placeholder="Symbol" autofocus>
              </th>
              <th>
                <input id="nazwa" type="text" class="@error('nazwa') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('specjalizacje_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $specjalizacja)
          <tr>
            <td><span class="custom badge blue white-text"
                style="background-color:#{{$specjalizacja->kolor}} !important;">{{$specjalizacja->symbol}}</span></td>
            <td>{{$specjalizacja->nazwa}}</td>

            <td>
              <a href="{{route('backend_specjalizacje_edit',['id'=>$specjalizacja->id_specjalizacje])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST"
                action="{{route('backend_specjalizacje_delete',['id'=>$specjalizacja->id_specjalizacje])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection