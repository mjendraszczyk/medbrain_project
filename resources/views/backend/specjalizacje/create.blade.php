@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Specjalizacje
            <a href="{{route('backend_specjalizacje_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_specjalizacje_store')}}">
                @include('backend.specjalizacje.form')
            </form>
        </div>
    </div>
</div>
@endsection