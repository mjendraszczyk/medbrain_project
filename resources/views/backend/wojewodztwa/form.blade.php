@csrf
<div class="col s12 m12 l12">
    @include('backend/_main/message')
    <label>Nazwa</label>

    <input required placeholder="Nazwa" name="nazwa" @if(Route::currentRouteName()=='backend_wojewodztwa_edit' )
        value="{{$wojewodztwa->nazwa}}" @else value="" @endif type="text" class="form-control-input validate">

    <label>Kraj</label>
    <select required name="id_kraje">
        <option value="" disabled selected>Kraj</option>

        @foreach (App\Http\Controllers\Controller::getKraj(null) as
        $kraj)
        <option value="{{$kraj->id_kraje}}" @if(Route::currentRouteName()=='backend_wojewodztwa_edit' ) @if( $kraj->
            id_kraje ==
            $wojewodztwa->id_kraje)
            selected="selected"
            @endif
            @endif
            >{{$kraj->nazwa}}</option>
        @endforeach
    </select>

    <label>Tresc</label>
    
    @if(Route::currentRouteName()=='backend_wojewodztwa_edit')
    <textarea class="form-control-input editor" name="tresc">{{$wojewodztwa->tresc}}</textarea>
    @else
    <textarea class="form-control-input editor" name="tresc"></textarea>
    @endif

</div>
<button type="submit" class="btn  waves-effect waves-light btn-large">
    Zapisz</button>