@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      Województwa
      <a href="{{route('backend_wojewodztwa_create')}}" class="btn waves-effect waves-light btn-secondary">
        <i class="material-icons">add_circle_outline</i>
        Nowy</a>
    </div>

    <div class="card-body">
      <table>
        <thead>
          <tr>
            <th>Nazwa</th>
            <th>Kraj</th>
            <th>Opcje</th>
          </tr>
          <tr>
            <form method="GET" action="{{route('backend_wojewodztwa_filter')}}">
              @csrf
              <th>
                <input id="nazwa" type="text" class="@error('nazwa') is-invalid @enderror" name="nazwa"
                  value="{{ Session::get('wojewodztwa_nazwa') }}" autocomplete="no" placeholder="Nazwa" autofocus>
              </th>

              <th>
                <select name="id_kraje">
                  @foreach(App\Http\Controllers\Controller::getKraj(null) as $kraj)
                  <option value="{{$kraj->id_kraje}}" @if($kraj->id_kraje == Session::get('wojewodztwa_id_kraje'))
                    selected="selected"
                    @endif>
                    {{$kraj->nazwa}}</option>
                  @endforeach
                </select>
              </th>
              <th>
                <button type="submit" name="save_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    search
                  </i> Szukaj</button>
                <button type="submit" name="reset_filter" class="btn  waves-effect waves-light btn-large">
                  <i class="material-icons">
                    clear
                  </i>
                  Reset</button>
              </th>
            </form>
          </tr>

        </thead>

        <tbody>
          @foreach($items as $wojewodztwo)
          <tr>
            <td>{{$wojewodztwo->nazwa}}</td>
            <td>{{App\Http\Controllers\Controller::getKraj($wojewodztwo->id_kraje)->nazwa}}</td>
            <td>
              <a href="{{route('backend_wojewodztwa_edit',['id'=>$wojewodztwo->id_wojewodztwa])}}"
                class="btn waves-effect waves-light btn-default">
                <i class="material-icons">
                  edit
                </i>
                Edytuj</a>
              <form method="POST" action="{{route('backend_wojewodztwa_delete',['id'=>$wojewodztwo->id_wojewodztwa])}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń</button></form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      {{ $items->links() }}
    </div>
  </div>
</div>
@endsection