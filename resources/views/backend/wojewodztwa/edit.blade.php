@extends('layouts.medbrain_backend')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Województwa
            <a href="{{route('backend_wojewodztwa_index')}}" class="btn waves-effect waves-light btn-secondary">
                <i class="material-icons">keyboard_backspace</i>
                Powrót</a>
        </div>

        <div class="card-body">
            <form method="POST" action="{{route('backend_wojewodztwa_update',['id'=>$id])}}">
                @method('PUT')
                @include('backend.wojewodztwa.form')
            </form>
        </div>
    </div>
</div>
@endsection