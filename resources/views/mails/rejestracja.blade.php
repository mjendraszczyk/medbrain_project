<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

<div style="text-align:center;background:#fbfbfb;width:100%;padding:25px 0;">
    <img src="{{asset('img/logo.png')}}" style="width:250px;" />
</div>
<div class="container">

    <h1 style="margin:50px 0 20px 0;display:block;text-align:center;">{{$temat}}</h1>
    <div style="height: 5px;max-width:25%;background-color: #4dc083;margin:auto;">
    </div>
    <div class='card' style="border:0px;border-radius:0px;width:85%;margin:20px auto;display:block;">
        <div class='card-body'>
            <div style="text-align:center;width:100%;display:block;margin:25px 0;">
                <strong>Dane użytkownika</strong>
                <br />
                {{-- Imię i nazwisko: {{$konto['name']}} --}}
                E-mail: {{$konto['email']}}
                {{-- <br>
                <a href="{{route('aktywacja',['email'=>$konto['email']])}}" class="btn btn-success"
                    style="padding:15px 25px;">Aktywuj konto</a> --}}
            </div>
        </div>
    </div>

</div>
<div style="width:100%;clear:both;text-align:center;background:#eee;padding:5px 0;">
    Pozdrawiam,<br>
    {{(App\Http\Controllers\Controller::getUstawienia()['firma'])}}
    <br>
    {{(App\Http\Controllers\Controller::getUstawienia()['adres'])}} <br />
    tel. {{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}} <br />
    email:
    {{(App\Http\Controllers\Controller::getUstawienia()['email'])}}
    <br>
</div>