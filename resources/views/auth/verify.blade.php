@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Weryfikuj adres e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Nowy link do aktywacji konta został wysłany.') }}
                    </div>
                    @endif

                    {{ __('Przed procedurą upewnij się czy otrzymałeś link aktywacyjny.') }}
                    {{ __('Jeśli nie uzyskałes linku aktywacyjnego') }}, <a
                        href="{{ route('verification.resend') }}">{{ __('Kliknij aby wygenerować') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection