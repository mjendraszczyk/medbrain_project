@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="text-align:center;padding: 25px;
    margin: 50px 0;">

                <h1 class="title-section bold rem3">{{ __('Resetuj hasło') }}</h1>

                {{-- test: {{print_r(Route::currentRouteName())}} --}}
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row" style="text-align:left;">
                            <label for="email"
                                class="col l6 m12 col-form-label text-md-right offset-l3">{{ __('Adres E-Mail') }}</label>

                            <div class="col l6 m12 offset-l3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <ul class="alert alert-danger red-text">
                                    <li>{{ $message }}</li>
                                </ul>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary green" style="margin:20px auto;">
                                    {{ __('Wyślij link do resetowania hasła') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection