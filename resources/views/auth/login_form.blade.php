<div class="row center logowanie">

            <div class="col s12 m12 l12 zaloguj_box">
                <div class="padding-box">
                    <img src="{{asset('img')}}/zaloguj_icon.png" alt="">
                    <h1 class="title-section bold rem3">Zaloguj się</h1>
                    <p>Jeśli masz już konto</p>
                    @if(Route::currentRouteName() == 'oglosznia_dodaj_krok1')
                    <form method="POST" action="{{ route('login') }}?proces=ogloszenie" style="width: 50%;" class="m-auto m-t50">
                        @else
                        <form method="POST" action="{{ route('login') }}" style="width: 50%;" class="m-auto m-t50">
                        @endif
                        @csrf
            
            
                        <input id="email" type="email" class="gray-input white-text @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="email" autofocus>
            
                        @error('email')
                        <ul class="alert alert-danger">
                            <li>{{ $message }}</li>
                        </ul>
                        @enderror
            
                        <input id="password" type="password" class="gray-input white-text @error('password') is-invalid @enderror"
                            placeholder="hasło" name="password" required autocomplete="current-password">
            
                        @error('password')
                        
    
                        <ul class="alert alert-danger">
                            <li>{{ $message }}</li>
                        </ul>
                
                        @enderror
                        <label style="display:flex;background: none !important;">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>
            
                            <span>
                                {{ __('Zapamiętaj mnie') }}
                            </span>
            
                        </label>
                        <label style="font-size:11px;display:block;text-align:left;">
                            Nie pamiętam hasła <a style="font-size:11px;" class="green-text"
                                href="{{URL::to('/password/reset')}}">[Resetuj
                                hasło]</a>
                        </label>
            
                        <button name="login_btn" type="submit" class="btn-grey-gradient m-t50 btn waves-effect waves-light btn-large gray">Zaloguj
                            się</button>
                    </form>
                </div>
            </div>

            <div class="col s12 m12 l12 zarejestruj_box">
                
                <div class="padding-box">
                    <h1 class="gray-text title-section bold rem3 uppercase">Nie masz jeszcze konta?</h1>
                    <img src="{{asset('img')}}/zarejestruj_icon.png" alt="">
                    <h1 class="title-section bold rem3">Zarejestruj się</h1>
                    <p class="gray-text">Korzystaj z portalu {{config('app.name')}}</p>
                    {{-- <form method="POST" style="width: 50%;margin: auto;" action="{{ route('register') }}"> --}}
                        @if(Route::currentRouteName() == 'oglosznia_dodaj_krok1')
                            <form method="POST" action="{{ route('register') }}?proces=ogloszenie" style="width: 50%;" class="m-auto m-t50">
                                @else
                                <form method="POST" action="{{ route('register') }}" style="width: 50%;" class="m-auto m-t50">
                                    @endif
                        @csrf
                        <input id="email_register" type="email"
                            class="gray-input @error('email_register') is-invalid @enderror" placeholder="email"
                            name="email_register" value="{{ old('email_register') }}" required autocomplete="email_register">
                        @error('email_register')
                        <ul class="alert alert-danger">
                            <li>{{ $message }}</li>
                        </ul>
                  
                        @enderror

                        <input id="password_register" type="password"
                            class="gray-input @error('password_register') is-invalid @enderror" placeholder="hasło"
                            name="password_register" required autocomplete="new-password">

                        @error('password_register')
                   
                        <ul class="alert alert-danger">
                            <li>{{ $message }}</li>
                        </ul>
                
                        @enderror

                        <input id="password_register-confirm" type="password" class="gray-input white-text"
                            name="password_register_confirmation" required placeholder="powtórz hasło"
                            autocomplete="new-password">

                        <label style="display:block;text-align:left;">
                            <input class="form-check-input" required type="checkbox" name="accept">
                            <span class="" style="font-size: 11px;text-align:left;line-height: initial;">
                                Zapoznałem się i akceptuję postanowienia <a href="{{route('regulamin')}}" style="font-size: 11px;
    color: #fff;">regulaminu</a>.</span></label>

                        <p class="" style="font-size: 11px;line-height: initial;">Jednym z Naszym priorytetów
                            jest dbanie o Państwa dane osobowe, a o tym
                            jak to robimy, a przede wszystkim do czego te
                            dane wykorzystujemy możecie Państwo przeczytać na naszej stronie medbra.in, w dedykowanej do
                            tego celu zakładce:
                            Polityka prywatności</p>

                        <button name="register_btn" type="submit" class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green">Załóż swój
                            profil</button>
                    </form>
                </div>
            </div>

           
        </div>