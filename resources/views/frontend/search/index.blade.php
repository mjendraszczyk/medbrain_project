@extends('layouts.medbrain')

@section('content')
@if(Route::currentRouteName() == 'home')
@include('frontend.homepage.homepage_header')
@endif
{{-- @include('frontend._main.filtry') --}}
<div id="app2">
    {{-- @include('frontend._main.filtry_content') --}}
    <div class="section">
        <!--   Icon Section   -->
        <div class="row praca-section @if((Route::currentRouteName() == 'ogloszenia_zobacz')) container @endif">
            <div class="col s12 m12 l12 no-padding">


                {{-- Dla listy ofert --}}
                <div class="oferty-pracy">
                    <div class="gray header_oferta_detail white list_header" style="background:#{{App\Http\Controllers\Controller::getSpecjalizacjeColor(Request::route('id_specjalizacje'))}} !important;">

                        <div class="col s12 m3 l3 pading-20-0 box-radius-profil"
                            style="background: #{{App\Http\Controllers\Controller::getSpecjalizacjeColor(Request::route('id_specjalizacje'))}};">
                            <h2 style="color: #fff;font-size: 32px;">
                                {{App\Http\Controllers\Controller::getSpecjalizacjeSymbol(Request::route('id_specjalizacje'))}}
                            </h2>
                        </div>
                        <div class="col s12 m9 l9">
                            <div class="row">
                                <div class="col s12 m12 l12  white-text">
                                    <h4>{{App\Http\Controllers\Controller::getSpecjalizacje(Request::route('id_specjalizacje'))}}
                                    </h4>
                                </div>
                            </div>
                        </div>

                        @endif
                        @endif
                    </div>
                    <ul class="lista">
                        @if((Route::currentRouteName() == 'pracownicy_specjalizacja_index') ||
                        (Route::currentRouteName() == 'pracownicy_wojewodztwa_index'))
                        @include('frontend._main.lista_profile')
                        @else
                        @include('frontend._main.lista_ogloszenia')
                        @endif
                    </ul>
                    @if(Route::currentRouteName() == 'home')
                    {{ $ogloszenia->links() }}
                    <div class="view-more-button btn green" style="margin:auto;display:table;">Następne</div>
                    @endif

                </div>
                @endif
            </div>
            {{-- @include('frontend.homepage.map') --}}
        </div>
    </div>
</div>
@endsection