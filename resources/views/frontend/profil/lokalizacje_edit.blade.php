@extends('layouts.medbrain')

@section('content')

<div class="profil container">
    <div class="row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">


            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')


                <form method="POST" action="{{route('profil_lokalizacje_update',['id'=>$lokalizacja->id_lokalizacja])}}">
                    @method('PUT')
                    @include('frontend.profil.lokalizacje_form')
                </form>

            </div>
        </div>
    </div>
</div>
@endsection