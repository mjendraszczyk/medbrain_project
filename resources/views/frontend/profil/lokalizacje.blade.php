@extends('layouts.medbrain')

@section('content')

<div class="profil container">
    <div class="row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">


            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')

                <h3 class="title_menu" style="margin:10px 0;">Lokalizacje</h3>
<a href="{{route('profil_lokalizacje_create')}}" class="btn btn-primary green">Dodaj lokalizację</a>
                <div class="uzytkownik_dane">
                    <div class="row lokalizacja">
                        

                    @foreach ($lokalizacje as $lokalizacja)
                    <div class="card panel col l6 m6 s12">
                        <div class="panel-default padding-25">
                            <a href="{{route('profil_lokalizacje_edit',['id'=>$lokalizacja->id_lokalizacja])}}" class="btn btn-primary green">Edytuj</a>
                            <form method="POST" style="display: inline-block" action="{{route('profil_lokalizacje_delete',['id'=>$lokalizacja->id_lokalizacja])}}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn waves-effect waves-light btn-danger">
                                    <i class="material-icons">
                                        restore_from_trash
                                    </i>Usuń</button></form>
                        <h6>{{$lokalizacja->alias}}</h6>
                        <p>
                            {{$lokalizacja->ulica}}
                            <br>
                            {{App\Http\Controllers\Controller::getMiasto($lokalizacja->id_miasta)}} <br/>
                            {{$lokalizacja->id_kraj}}
                        </p>
                        </div>
                        </div>
                    @endforeach
                    </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection