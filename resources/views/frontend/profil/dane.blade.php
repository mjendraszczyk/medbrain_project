@extends('layouts.medbrain')

@section('content')

<div class="profil container">
  <div class="row konto">
    <div class="col s12 m12 l3 xl3">
      <div class="white panel">
        @include('frontend.profil.menu')
      </div>
    </div>

    <div class="col s12 m12 l9 xl9">


      <div class="row white panel account_detail">
        @include('frontend.profil.profil_header')

        <h3 class="title_menu" style="margin:10px 0;">Twoje dane</h3>

        <div class="uzytkownik_dane">
          @include('backend/_main/message')
          {{-- <div class="input-field col s12 m12 l6">
            <div class="row card padding-25 no-shadow">
              <h3 class="card-title">Użytkownik</h3>
              <form method="POST" enctype="multipart/form-data" action="{{route('profil_dane_update')}}">
                @csrf
                @method('PUT')

                <div class="clearfix"></div>
                <div style="display: none;">
                <label>Imię i nazwisko</label>
                <input required placeholder="Imię i nazwisko" name="name" value="{{$profil->name}}" type="text"
                  class="form-control-input validate">
                <div class="clearfix"></div>
              </div>
                <label>E-mail</label>
                <input required placeholder="E-mail" name="email" value="{{$profil->email}}" type="text"
                  class="form-control-input validate">
                <div class="clearfix"></div>
                <label>Hasło</label>
                <input placeholder="Hasło" name="password" value="" type="password" class="form-control-input validate">

                <label>Powtórz Hasło</label>
                <input placeholder="Powtórz Hasło" name="password_repeat" value="" type="password"
                  class="form-control-input validate">

                <label>Telefon</label>
                <input placeholder="Telefon" value="{{$profil->telefon}}" name="telefon" type="text"
                  class="form-control-input validate">

                <label>Logo</label>
                <div class="file-field input-field">
                  <div class="btn">
                    <span>Plik</span>
                    <input type="file" name="avatar_upload">
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                  </div>
                </div>

                <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
              </form>

              <label>Usuwanie konta</label>
              <form method="POST" id="usun_konto" name="usun_konto" action="{{route('profil_usun_konto')}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn waves-effect waves-light btn-danger js-alert"
                  style="background:#a91212 !important;">
                  <i class="material-icons">
                    restore_from_trash
                  </i>Usuń konto</button></form>
            </div>
          </div> --}}

          <div class="input-field col s12 m12 l12">
            <div class="row card padding-25 no-shadow no-border">
              {{-- @if($profil->status == 0) --}}
              <form method="POST" enctype="multipart/form-data" action="{{route('profil_dane_firma_update')}}">
                @csrf
                @method('PUT')
                <h3 class="card-title">Firma</h3>
                <div class="col s12 m12 l12">
                  <div class="clearfix"></div>
                  <label>Nazwa firmy</label>
                  <input required placeholder="Nazwa firmy" name="nazwa" @if($profil->nazwa != '') value="{{$profil->nazwa}}" @else value="{{old('nazwa')}}" @endif type="text"
                    class="form-control-input validate">
                </div>
                <div class="col s12 m12 l12" style="display: none">
                  {{-- <label>NIP</label> --}}
                  <input required placeholder="NIP" name="nip" @if($profil->nip != '') value="{{$profil->nip}}" @else value="{{old('nip')}}" @endif type="hidden"
                    class="form-control-input validate">
                </div>

                <div class="col s12 m12 l12">

                  <label>E-mail kontaktowy</label>
                  <input required placeholder="E-mail" name="podmiot_email" @if($profil->podmiot_email != '') value="{{$profil->podmiot_email}}" @else value="{{old('podmiot_email')}}" @endif 
                    type="email" class="form-control-input validate">

                  <label>Telefon kontaktowy</label>
                  <input required placeholder="Telefon" @if($profil->podmiot_telefon != '') value="{{$profil->podmiot_telefon}}" @else value="{{old('podmiot_telefon')}}" @endif name="podmiot_telefon"
                    type="text" class="form-control-input validate">
                </div>
                <div class="col s12 m12 l12">
                  <label>Ulica</label>
                  <input required placeholder="Ulica" name="ulica" @if($profil->ulica != '') value="{{$profil->ulica}}" @else value="{{old('ulica')}}" @endif type="text"
                    class="form-control-input validate">

                  <label>Kod pocztowy</label>
                  <input required placeholder="Kod pocztowy" name="kod_pocztowy" @if($profil->kod_pocztowy != '') value="{{$profil->kod_pocztowy}}" @else value="{{old('kod_pocztowy')}}" @endif
                    type="text" class="form-control-input validate">


                  <label>Województwo</label>
                  <select name="id_wojewodztwa" class="zmien_miasta">
                    @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                    @if(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($profil->id_miasta) ==
                    $wojewodztwo->id_wojewodztwa)
                    <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
                      @else
                    <option value="{{$wojewodztwo->id_wojewodztwa}}">
                      @endif
                      {{$wojewodztwo->nazwa}}
                    </option>

                    @endforeach
                  </select>


                  <label>Miasto</label>
                  <select required name="id_miasta">
                    <option value="" disabled selected>Miasto (obowiązkowe)</option>
                    {{-- @foreach ($wojewodztwa as $wojewodztwo) --}}

                    @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($profil->id_miasta))
                    as
                    $miasto)
                    <option value="{{$miasto->id_miasta}}" @if(($profil->id_miasta
                      == $miasto->id_miasta))
                      selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                    @endforeach

                  </select>

                  <div>
                    <label>Rodzaj placówki</label>
                    <select name="id_rodzaj_placowki" required>
                      <option value="" disabled selected>Rodzaj placówki (obowiązkowe)</option>
                      @foreach ($rodzaje_placowki as $placowka)
                      <option value="{{$placowka->id_rodzaj_placowki}}" @if(($placowka->id_rodzaj_placowki ==
                        $profil->id_rodzaj_placowki) || ($placowka->id_rodzaj_placowki ==
                        old('id_rodzaj_placowki')))
                        selected="selected"
                        @endif>{{$placowka->nazwa}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="clearfix"></div>
                  <label>Logo</label>
                  <div class="file-field input-field">
                    <div class="btn">
                      <span>Plik</span>
                      <input type="file" name="logo_upload">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" type="text">
                    </div>
                  </div>

                  <input required placeholder="Lat" name="lat" id="lat" value="{{$profil->lat}}" type="hidden"
                    class="form-control-input validate">
                  <input required placeholder="Lon" name="lon" id="lon" value="{{$profil->lon}}" type="hidden"
                    class="form-control-input validate">

                  <button type="submit"
                    class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
              </form>
              {{-- @else
              <a href="#" class="btn btn-primary">Otwórz konto firmowe</a>
              @endif --}}
            </div>
          </div>


        </div>
      </div>

    </div>
  </div>
</div>
</div>
@endsection