@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="profil row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">
            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                <h3 class="title_menu">Twoje dane</h3>
                <form method="POST"
                    action="{{route('profil_ogloszenia_update', ['id' => $ogloszenia->id_ogloszenia])}}">
                    @method('PUT')
                    @csrf
                    <div class="col s12 m12 l12">
                        @include('backend/_main/message')

                        <div class="clearfix"></div>
                        <label>Specjalizacja</label>
                        <select name="id_specjalizacje">
                            <option value="" disabled selected>np. dermatologia (wymagane)</option>
                            @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                            <option value="{{$specjalizacja->id_specjalizacje}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($ogloszenia->
                                id_specjalizacje ==
                                ($specjalizacja->id_specjalizacje)))
                                selected="selected"
                                @else
                                @endif>{{$specjalizacja->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Wynagrodzenie od:</label>
                        <input placeholder="Wynagrodzenie od" name="wynagrodzenie_od"
                            @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                            value="{{$ogloszenia->wynagrodzenie_od}}" @else value="" @endif type="number"
                            class="form-control-input validate">

                        <label>Wynagrodzenie do:</label>
                        <input placeholder="Wynagrodzenie do" name="wynagrodzenie_do"
                            @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                            value="{{$ogloszenia->wynagrodzenie_do}}" @else value="" @endif type="number"
                            class="form-control-input validate">
                        <label>Typ wynagrodzenia:</label>

                        <select name="typ_wynagrodzenia" required>
                            <option value="" disabled selected>Typ wynagrodzenia</option>
                            @foreach(App\Http\Controllers\Controller::getTypWynagrodzenia(null) as $typ)
                            <option value="{{$typ->id_typ_wynagrodzenia}}" @if(($typ->id_typ_wynagrodzenia ==
                                $ogloszenia->typ_wynagrodzenia) || ($typ->id_typ_wynagrodzenia ==
                                old('typ_wynagrodzenia')))
                                selected="selected"
                                @endif>{{$typ->nazwa}}</option>
                            @endforeach
                        </select>

                        <label>Waluta wynagrodzenia:</label>
                        <select name="waluta_wynagrodzenia" required>
                            <option value="" disabled selected>Waluta</option>
                            @foreach(App\Http\Controllers\Controller::getWalutaWynagrodzenia(null) as $waluta)
                            <option value="{{$waluta->id_waluta_wynagrodzenia}}" @if(($waluta->id_waluta_wynagrodzenia
                                ==
                                $ogloszenia->waluta_wynagrodzenia))
                                selected="selected"
                                @endif>{{$waluta->nazwa}}</option>
                            @endforeach
                        </select>

                        <div class="row">
                            <div class="col s12 m6 l6">
                                <label>Doświadczenie od:</label>

                                <input placeholder="Doswiadczenie od" name="doswiadczenie_od"
                                    @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                                    value="{{$ogloszenia->doswiadczenie_od}}" @else value="" @endif type="text"
                                    class="form-control-input validate">
                            </div>

                            <div class="col s12 m6 l6">
                                <label>Doświadczenie do:</label>

                                <input placeholder="Doswiadczenie do" name="doswiadczenie_do"
                                    @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                                    value="{{$ogloszenia->doswiadczenie_do}}" @else value="" @endif type="text"
                                    class="form-control-input validate">
                            </div>

                        </div>

                        <div class="clearfix"></div>
                        <label>Poziom</label>
                        <select name="id_poziom">
                            <option value="" disabled selected>Poziom</option>
                            @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
                            <option value="{{$poziom->id_poziom}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($poziom->
                                id_poziom ==
                                $ogloszenia->id_poziom))
                                selected="selected"
                                @else @endif>{{$poziom->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Typ umowy</label>
                        <select name="id_rodzaj_umowy">
                            <option value="" disabled selected>Rodzaj umowy</option>
                            @foreach(App\Http\Controllers\Controller::getRodzajUmowy(null) as $rodzaj)
                            <option value="{{$rodzaj->id_rodzaj_umowy}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($rodzaj->id_rodzaj_umowy
                                ==
                                $ogloszenia->id_rodzaj_umowy))
                                selected="selected"
                                @else @endif>{{$rodzaj->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <label>Wymiar pracy</label>
                        <select name="id_wymiar_pracy">
                            <option value="" disabled selected>Wymiar pracy</option>
                            @foreach(App\Http\Controllers\Controller::getWymiarPracy(null) as $wymiar)
                            <option value="{{$wymiar->id_wymiar_pracy}}"
                                @if((Route::currentRouteName()=='profil_ogloszenia_edit' ) && ($wymiar->id_wymiar_pracy
                                ==
                                $ogloszenia->id_wymiar_pracy))
                                selected="selected"
                                @else @endif>{{$wymiar->nazwa}}</option>
                            @endforeach
                        </select>
                        <div class="clearfix"></div>
                        <input type="hidden" name="id_podmiot" readonly value="{{Auth::user()->id_podmiot}}" />
                        <label>Tresc</label>
                        <div class="alert alert-info">
                            Aby skopiować treść użyj skrótów klawiszowych CTR+C i CTRL+V
                        </div>
                        @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                        <textarea name="tresc" required placeholder="Wpisz treść ogloszenia"
                            class="editor form-control-area materialize-textarea validate">{{$ogloszenia->tresc}}</textarea>
                        @else
                        <textarea name="tresc" required placeholder="Wpisz treść ogloszenia"
                            class="editor form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
                        @endif
                    </div>
                    <label>Link do systemu rekrutacyjnego</label>
                        @if(Route::currentRouteName()=='profil_ogloszenia_edit' )
                        <input value="{{$ogloszenia->rekruter}}" placeholder="Link do systemu rekrutacyjnego" name="rekruter" type="text"
                            class="form-control-input">
                        @else
                        <input value="" placeholder="Link do systemu rekrutacyjnego" name="rekruter" type="text" class="form-control-input">
                        @endif

                    <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">
                        Zapisz</button>
                </form>
                {{--
                <h6 style="font-weight: bold;font-size: 1.5rem;margin: 20px 0;">Domyślna lokalizacja
                    ogłoszenia:</h6>
                <ul>
                    <li>
                        {{App\Http\Controllers\Controller::getPodmiotAddress($ogloszenia->id_podmiot)}}
                    </li>
                </ul>
                <h6 style="font-weight: bold;font-size: 1.5rem;margin: 20px 0;">Lista dodatkowych lokalizacji
                    ogłoszenia:</h6>
                <ul>
                    @foreach($dodatkowe_adresy as $adres)
                    <li>
                        <form method="POST"
                            action="{{route('delete_dodatkowe_lokalizacje',['id'=>$adres->id_dodatkowe_adresy])}}">
                            @csrf
                            @method('DELETE')
                            {{$adres->nazwa}}, ({{$adres->lat_dodatkowy_adres}},{{$adres->lon_dodatkowy_adres}})
                            <button type="submit" class="btn waves-effect waves-light btn-danger">
                                <i class="material-icons">
                                    restore_from_trash
                                </i></button></form>
                    </li>
                    @endforeach
                </ul>
               <div class="dodatkowe_lokalizacje">
                    <form method="POST"
                        action="{{route('store_dodatkowe_lokalizacje',['id_ogloszenia'=>$ogloszenia->id_ogloszenia])}}">
                        @csrf
                        @method('POST')
                        <label>Adres</label>
                        <input value="" placeholder="Ulica, miasto" name="ulica" type="text" class="form-control-input">
                        <input value="" placeholder="Miasto" required name="miasto" type="text"
                            class="form-control-input">
                        <input value="" id="lat" name="lat" type="hidden" class="form-control-input">
                        <input value="" id="lon" name="lon" type="hidden" class="form-control-input">
                </div> --}}
                {{-- <button type="submit"
                    class="dodaj_lokalizacje btn waves-effect waves-light btn-large green min-200">Dodaj
                    dodatkową lokalizację</button> --}}
              
                </form>
            </div>

        </div>
    </div>
</div>
</div>
@endsection