@csrf
<div class="col s12 m12 l12">
    {{-- <h4 class="gray-text">Adresy</h4> --}}
    <h3 class="title_menu" style="margin:10px 0;">Lokalizacje</h3>
    @include('backend/_main/message')
    <label>Alias</label>

    <input required placeholder="Alias" name="alias" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
        value="{{$lokalizacja->alias}}" @else value="" @endif type="text" class="form-control-input validate">


        <label>E-mail</label>
            
            <input required placeholder="E-mail" name="kontakt_email" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
                value="{{$lokalizacja->kontakt_email}}" @else value="" @endif type="email" class="form-control-input validate">


                <label>Telefon</label>
                    
                    <input required placeholder="Telefon" name="kontakt_telefon" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
                        value="{{$lokalizacja->kontakt_telefon}}" @else value="" @endif type="number" class="form-control-input validate">


        <label>Ulica</label>
        
        <input required placeholder="Ulica" name="ulica" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
            value="{{$lokalizacja->ulica}}" @else value="" @endif type="text" class="form-control-input validate">
                <label>Wojewodztwo</label>
                <select name="id_wojewodztwa" class="zmien_miasta form-control-input validate">
                    @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                    <option value="{{$wojewodztwo->id_wojewodztwa}}" @if((old('id_wojewodztwa')==$wojewodztwo->id_wojewodztwa))
                        selected="selected" @endif @if(Route::currentRouteName()=='profil_lokalizacje_edit') @if(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($lokalizacja->id_miasta) ==
                        $wojewodztwo->id_wojewodztwa) selected="selected" @endif @endif>
                        {{$wojewodztwo->nazwa}}
                    </option>
                    @endforeach
                </select>

                <label>Miasto</label>
                
                @if(Route::currentRouteName()=='profil_lokalizacje_edit')
                <select name="id_miasta" class="form-control-input validate"
                    data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($lokalizacja->id_miasta)}}">
                    <option value="" disabled>Miasto</option>
                
                    @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($lokalizacja->id_miasta))
                    as
                    $miasto)
                    <option @if((old('id_miasta')==$miasto->id_miasta) || ($lokalizacja->id_miasta == $miasto->id_miasta))
                        selected="selected" @endif
                        value="{{$miasto->id_miasta}}">{{$miasto->nazwa}}</option>
                    @endforeach
                </select>
                @else
                <select name="id_miasta" class="form-control-input validate"
                    data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1)}}">
                    <option value="" disabled>Miasto</option>
                
                    @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1))
                    as
                    $miasto)
                    <option @if((old('id_miasta')==$miasto->id_miasta)) selected="selected" @endif
                        value="{{$miasto->id_miasta}}">{{$miasto->nazwa}}</option>
                    @endforeach
                </select>
                @endif
                
<label>Rodzaj placówki</label>
<select name="id_rodzaj_placowki">
                <option value="" disabled selected>Rodzaj placówki</option>
                @foreach ($rodzaje_placowki as $placowka)
                <option value="{{$placowka->id_rodzaj_placowki}}" @if(($placowka->id_rodzaj_placowki ==
                    Session::get('form_rodzaj_placowki')) || ($placowka->id_rodzaj_placowki == old('id_rodzaj_placowki')))
                    selected="selected"
                    @endif   @if(Route::currentRouteName()=='profil_lokalizacje_edit') @if($lokalizacja->id_rodzaj_placowki == $placowka->id_rodzaj_placowki)  selected="selected" @endif @endif>{{$placowka->nazwa}}</option>
                @endforeach
            </select>
           <div style="display: none;">
                <label>Wybierz lokalizację</label>
                <div class="alert alert-danger" style="margin: 25px 0;">
                    Wybierz lokalizację klikająć punkt na mapie
                </div>
                <div id="mapLatLon"></div>
            </div>
<input required placeholder="" id="lon" name="lon" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
    value="{{$lokalizacja->lon}}" @else value="" @endif type="hidden" class="form-control-input validate">

<input required placeholder="" id="lat" name="lat" @if(Route::currentRouteName()=='profil_lokalizacje_edit' )
    value="{{$lokalizacja->lat}}" @else value="" @endif type="hidden" class="form-control-input validate">

</div>
<button type="submit" class="green btn  waves-effect waves-light btn-large">
    Zapisz</button>