@extends('layouts.medbrain')

@section('content')

<div class="profil container">
    <div class="row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">


            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                  @if(Request::query('status') == 'krok4')
                <form method="POST" action="{{route('profil_lokalizacje_store')}}?status=krok4">
                  @else
                  <form method="POST" action="{{route('profil_lokalizacje_store')}}">
                  @endif
            @include('frontend.profil.lokalizacje_form')
        </form>

            </div>
        </div>
    </div>
</div>
@endsection
