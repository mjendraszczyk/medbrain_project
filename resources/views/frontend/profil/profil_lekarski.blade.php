@extends('layouts.medbrain')

@section('content')
<div class="container">
  <div class="profil row konto">
    <div class="col s12 m12 l3 xl3">
      <div class="white panel">
        @include('frontend.profil.menu')
      </div>
    </div>

    <div class="col s12 m12 l9 xl9">
      <div class="row white panel account_detail">
        @include('frontend.profil.profil_header')
        <h3 class="title_menu">Profil lekarski</h3>
        <form method="POST" id="app" novalidate action="{{route('profil_profil-lekarski_update')}}">
          @include('backend/_main/message')
          @csrf
          @method('PUT')
          <div class="input-field col s12">
            <div class="row">

              <div class="col s12 m12 l12">
                <label>Specjalizacja</label>
                <select name="id_specjalizacje" required>
                  <option value="" disabled selected>np. dermatologia (wymagane)</option>
                  @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                  <option value="{{$specjalizacja->id_specjalizacje}}" @if($specjalizacja->id_specjalizacje ==
                    $profil->id_specjalizacje)
                    selected="selected"
                    @else
                    @if($specjalizacja->id_specjalizacje == old('id_specjalizacje'))
                    selected="selected"
                    @endif
                    @endif>{{$specjalizacja->nazwa}}</option>
                  @endforeach
                </select>
              </div>


              <div id="app3" class="col s12 m12 l12">
                <button type="button" class="edukacja add_row btn waves-effect waves-light btn-small green min-200"
                  style="display: block;">
                  <i class="material-icons">
                    add
                  </i>
                  Dodaj</button>

                <div class="edukacja_block adding_row">
                  <label>Edukacja</label>

                  <select required name="id_tytul[]">
                    <option value="" disabled selected>Tytuł</option>
                    @foreach (App\Http\Controllers\Controller::getTytuly(null) as
                    $tytul)
                    <option value="{{$tytul->id_tytuly}}" @if($tytul->id_tytuly == $edukacja->id_tytul)
                      selected="selected"
                      @else
                      @if($tytul->id_tytuly == old('id_tytul[]'))
                      selected="selected"
                      @endif
                      @endif>{{$tytul->nazwa}}</option>
                    @endforeach
                  </select>

                  <div class="content">
                    <input required placeholder="Edukacja" name="edukacja_nazwa[]" value="{{$edukacja->nazwa}}"
                      type="text" class="form-control-input validate">
                    <div class="row">
                      <div class="col s12 m6 l6">
                        <input required placeholder="Lata od" value="{{$edukacja->edukacja_lata_od}}"
                          name="edukacja_lata_od[]" type="date" min="0" class="form-control-input validate" />
                      </div>
                      <div class="col s12 m6 l6">
                        <input required placeholder="Lata do" value="{{$edukacja->edukacja_lata_do}}"
                          name="edukacja_lata_do[]" type="date" min="0" class="form-control-input validate" />
                      </div>
                    </div>
                  </div>


                </div>
                @foreach($edukacja_others as $key => $edukacja)
                @if($key > 0)
                <div class="edukacja_block adding_row">
                  <div class="col m10 no-padding">
                    <label>Edukacja</label>
                    <div>
                      <select required name="id_tytul[]">
                        <option value="" disabled selected>Tytuł</option>
                        @foreach (App\Http\Controllers\Controller::getTytuly(null) as
                        $tytul)
                        <option value="{{$tytul->id_tytuly}}" @if($tytul->id_tytuly == $edukacja->id_tytul)
                          selected="selected"
                          @endif>{{$tytul->nazwa}}</option>
                        @endforeach
                      </select>

                    </div>
                    <div class="content">
                      <input required placeholder="Edukacja" name="edukacja_nazwa[]" value="{{$edukacja->nazwa}}"
                        type="text" class="form-control-input validate">
                      <div class="row">
                        <div class="col s12 m6 l6">
                          <input required placeholder="Lata od" value="{{$edukacja->edukacja_lata_od}}"
                            name="edukacja_lata_od[]" type="date" min="0" class="form-control-input validate" />
                        </div>
                        <div class="col s12 m6 l6">
                          <input required placeholder="Lata do" value="{{$edukacja->edukacja_lata_do}}"
                            name="edukacja_lata_do[]" type="date" min="0" class="form-control-input validate" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col m2">
                    <span class="remove_row btn btn-default">Usuń</span>
                  </div>

                </div>
                @endif
                @endforeach
              </div>

              <div class="col s12 m12 l12">
                <button type="button" class="doswiadczenie add_row btn waves-effect waves-light btn-small green min-200"
                  style="display: block;">
                  <i class="material-icons">
                    add
                  </i>
                  Dodaj</button>
                <div class="doswiadczenie_block adding_row">
                  <label>Doświadczenie</label>
                  <div class="content">
                    <input required placeholder="Doswiadczenie" name="doswiadczenie_nazwa[]"
                      value="{{$doswiadczenie->nazwa}}" type="text" class="form-control-input validate">
                    <div class="row">
                      <div class="col s12 m6 l6">
                        <input required placeholder="Staz od" value="{{$doswiadczenie->doswiadczenie_lata_od}}"
                          name="doswiadczenie_lata_od[]" type="date" class="form-control-input validate" />
                      </div>
                      <div class="col s12 m6 l6">
                        <input required placeholder="Staz do" value="{{$doswiadczenie->doswiadczenie_lata_do}}"
                          name="doswiadczenie_lata_do[]" type="date" class="form-control-input validate" />
                      </div>
                    </div>
                  </div>
                </div>
                @foreach($doswiadczenie_others as $key => $doswiadczenie)
                @if($key > 0)
                <div class="doswiadczenie_block adding_row">
                  <div class="col m10">
                    <label>Doświadczenie</label>
                    <div class="content">
                      <input required placeholder="Doswiadczenie" name="doswiadczenie_nazwa[]"
                        value="{{$doswiadczenie->nazwa}}" type="text" class="form-control-input validate">
                      <div class="row">
                        <div class="col s12 m6 l6">
                          <input required placeholder="Staz od" value="{{$doswiadczenie->doswiadczenie_lata_od}}"
                            name="doswiadczenie_lata_od[]" type="date" class="form-control-input validate" />
                        </div>
                        <div class="col s12 m6 l6">
                          <input required placeholder="Staz do" value="{{$doswiadczenie->doswiadczenie_lata_do}}"
                            name="doswiadczenie_lata_do[]" type="date" class="form-control-input validate" />
                        </div>

                      </div>
                    </div>
                    <div class="col m2">
                      <span class="remove_row btn btn-default">Usuń</span>
                    </div>
                  </div>
                </div>
                @endif
                @endforeach

              </div>
              <div class="col s12 m12 l12">
                <label>Województwo</label>
                <select name="id_wojewodztwa" class="zmien_miasta">
                  @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                  @if(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($profil->id_miasta) ==
                  $wojewodztwo->id_wojewodztwa)
                  <option value="{{$wojewodztwo->id_wojewodztwa}}" selected="selected">
                    @else
                  <option value="{{$wojewodztwo->id_wojewodztwa}}">
                    @endif
                    {{$wojewodztwo->nazwa}}
                  </option>
                  @endforeach
                </select>
              </div>

              <div class="col s12 m12 l12">
                <label>Miasto</label>
                <select required name="id_miasta"
                  data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($profil->id_miasta)}}">
                  <option value="" disabled selected>Miasto</option>
                  {{-- @foreach ($wojewodztwa as $wojewodztwo) --}}
                  @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($profil->id_miasta))
                  as
                  $miasto)
                  <option value="{{$miasto->id_miasta}}" @if( ($profil->id_miasta
                    == $miasto->id_miasta))
                    selected="selected" @else @endif>{{$miasto->nazwa}}</option>
                  @endforeach
                </select>
                <input type="hidden" required name="lat" id="lat" value="{{$profil->lat}}" />
                <input type="hidden" required name="lon" id="lon" value="{{$profil->lon}}" />
              </div>
              <div class="col s12 m12 l12">
                <label>O mnie</label>
                <div class="alert alert-info">
                  Aby skopiować treść użyj skrótów klawiszowych CTR+C i CTRL+V
                </div>
                <textarea name="tresc" required placeholder="O mnie..."
                  class="editor form-control-area materialize-textarea validate">{{$profil->o_mnie}}</textarea>
              </div>
              <div class="col s12 m12 l12">
                <label>Praca</label>
                <select name="szukam">

                  <option value="1" @if($profil->szukam == '1') selected="selected" @endif>Szukam pracy (pokaż profil na
                    portalu)</option>
                  <option value="0" @if($profil->szukam != '1') selected="selected" @endif>Nie szukam (ukryj profil na
                    portalu)</option>

                </select>
              </div>
            </div>

            <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
          </div>


        </form>
      </div>
    </div>
  </div>
</div>
@endsection