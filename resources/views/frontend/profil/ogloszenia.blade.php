@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="profil row konto">
        <div class="col s12 m3 l3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m9 l9">
            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                <h3 class="title_menu">Twoje dane</h3>


                @if(count($ogloszenia) > 0)
                {{-- <table> --}}
                {{-- <thead> --}}
                <div class="row table-div">
                    <div class="col m12 l3" style="text-align:left;">Nazwa firmy</div>
                    <div class=" col m12 l2">Specjalizacja</div>
                    <div class="col m12 l2">Wynagrodzenie</div>
                    <div class="col m12 l5">Opcje</div>
                </div>


                {{-- </tr> --}}
                {{-- </thead> --}}
                {{-- <tbody> --}}
                @foreach($ogloszenia as $ogloszenie)
                <div class="row">
                    <div class="col m12 l3" style="text-align:left">
                        {{App\Http\Controllers\Controller::getPodmiot($ogloszenie->id_podmiot)}}
                    </div>
                    <div class="col m12 l2">
                        {{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie->id_specjalizacje)}}
                    </div>
                    <div class="col m12 l2">
                        {!!
                        App\Http\Controllers\Controller::parseMoneyFormat($ogloszenie->wynagrodzenie_od,
                        $ogloszenie->id_ogloszenia)!!}
                        <br>
                        {!!
                        App\Http\Controllers\Controller::parseMoneyFormat($ogloszenie->wynagrodzenie_do,
                        $ogloszenie->id_ogloszenia)!!}
                    </div>
                    <div class="col m12 l5">
                        <a href="{{route('profil_wiadomosci_ogloszenia',['id'=>$ogloszenie->id_ogloszenia])}}"
                            class="btn waves-effect waves-light btn-prime green">

                            <i class="material-icons">
                                mail
                            </i> wiadomości
                        </a>
                        <a target="_blank" href="{{route('ogloszenia_zobacz',['id'=>$ogloszenie->id_ogloszenia, 'nazwa'=>str_slug(App\Http\Controllers\Controller::getPodmiot($ogloszenie->id_podmiot))])}}"
                            class="btn waves-effect waves-light btn-prime">

                            <i class="material-icons">
                                visibility
                            </i>
                        </a>

                        <a href="{{route('profil_ogloszenia_edit',['id'=>$ogloszenie->id_ogloszenia])}}"
                            class="btn waves-effect waves-light btn-default">

                            <i class="material-icons">
                                edit
                            </i>
                        </a>
                        <form class="inline" method="POST"
                            action="{{route('profil_ogloszenia_delete',['id'=>$ogloszenie->id_ogloszenia])}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn waves-effect waves-light btn-danger">
                                <i class="material-icons">
                                    restore_from_trash
                                </i></button></form>
                    </div>
                </div>
                {{-- <tr>
                                <td colspan="4">
                                sfdsf
                                </td>
                            </tr>
                        </tr> --}}
                @endforeach
                {{-- </tbody>
                </table> --}}
                @else
                <ul class="alert alert-info">
                    <li>Brak ogłoszeń</li>
                </ul>

                @endif
            </div>
        </div>
    </div>
</div>
@endsection