@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">
            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                <h3 class="title_menu">Płatności</h3>
                <ul class="alert alert-success">
                    <li>Brak zobowiązań</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection