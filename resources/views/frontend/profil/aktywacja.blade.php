@extends('layouts.medbrain')


@section('content')
<div class="container">
    <div class="white m-50" style="padding:50px;text-align:center;margin: 150px;">
        @if($user->stan == '0')
        <h3 class="bold">Aktywacja konta</h3>
        @include('backend/_main/message')
        <form method="POST" action="{{route('aktywacja_proces', ['id' => $user_id])}}">
            @method('PUT')
            @csrf
            <label>Ustaw hasło</label>
            <input required placeholder="Hasło" name="password" value="" type="password"
                class="form-control-input validate">
            <input required placeholder="Powtórz hasło" name="password_repeat" value="" type="password"
                class="form-control-input validate">
            <label>Ustaw specjalizację</label>
            <select name="id_specjalizacje">
                <option value="" disabled selected>np. dermatologia (wymagane)</option>
                @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                <option value="{{$specjalizacja->id_specjalizacje}}">{{$specjalizacja->nazwa}}</option>
                @endforeach
            </select>
            <label>Ustaw miejscowość</label>
            <select required name="id_miasta">
                <option value="" disabled selected>Województwo</option>
                @foreach ($wojewodztwa as $wojewodztwo)
                <optgroup label="{{$wojewodztwo->nazwa}}">
                    @foreach (App\Http\Controllers\Controller::getMiastaByWojewodztwo($wojewodztwo->id_wojewodztwa)
                    as
                    $miasto)
                    <option value="{{$miasto->id_miasta}}">{{$miasto->nazwa}}</option>
                    @endforeach
                </optgroup>
                @endforeach
            </select>
            <button type="submit" class="m-t50 btn waves-effect waves-light btn-large green min-200"
                style="width:100%;">
                Aktywuj</button>
            @else
            <h3 class="bold">Konto zostało juz aktywowane</h3>
            @endif


    </div>
</div>
</div>
@endsection