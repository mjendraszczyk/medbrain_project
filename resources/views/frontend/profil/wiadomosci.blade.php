@extends('layouts.medbrain')

@section('content')
<div class="container">
    <div class="profil row konto">
        <div class="col s12 m3 l3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m9 l9">
            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')
                <h3 class="title_menu">Wiadomości</h3>

                @if(count($wiadomosci) > 0)

                @foreach($wiadomosci as $wiadomosc)
                <div class="message row card">
                    <div class="card-body">
                        <div class="col m12 l2" style="text-align:center;padding: 20px;">
                            @if($wiadomosc->id_user != null)
                            @if(App\Http\Controllers\Controller::getUzytkownik($wiadomosc->id_user)->avatar_upload !=
                            '')
                            <img src="{{asset('/img/avatar')}}/{{App\Http\Controllers\Controller::getUzytkownik($wiadomosc->id_user)->avatar_upload}}"
                                style="width: 64px;display: block;margin: 0 auto;">
                            @else
                            <img src="{{asset('/img/icons/avatar.png')}}"
                                style="width: 64px;display: block;margin: 0 auto;">
                            @endif
                            @else
                            <img src="{{asset('/img/icons/avatar.png')}}"
                                style="width: 64px;display: block;margin: 0 auto;">
                            @endif
                            @if($wiadomosc->id_user != null)
                            <h6 style="font-weight:bold;">
                                {{App\Http\Controllers\Controller::getUzytkownik($wiadomosc->id_user)->name}}</h6>
                            @else
                            <h6 style="font-weight:bold;">{{$wiadomosc->imie_nazwisko}}</h6>
                            @endif
                        </div>
                        <div class="col m12 l10">
                            <form method="post"
                                action="{{route('usun_wiadomosci_ogloszenia',['id'=>$wiadomosc->id_aplikacje])}}">
                                @csrf
                                @method('DELETE')
                                {{-- <input type="hidden" name="id_aplikacje" value="{{$wiadomosc->id_aplikacje}}"> --}}
                                <button type="submit" class="btn btn-small btn-danger"
                                    style="float: right;">Usuń</button>
                            </form>
                            <h6 class="uppercase rem085">Wiadomość</h6>
                            <p style="font-size: 0.8rem;border-bottom: 1px solid #eee;margin: 0;color: #202020;">
                                <i class="material-icons"
                                    style="font-size: 1rem;vertical-align: middle;">mail</i>{{$wiadomosc->email}}</p>
                            <br />
                            {{$wiadomosc->tresc}}
                        </div>
                        <div class="card-action" style="background: #fbfbfb;clear: both;">
                            <h6 class="uppercase rem085">Załącznik</h6>
                            <i class="material-icons" style="vertical-align:middle;">
                                attach_file
                            </i> <a
                                href="{{asset('/documents')}}/{{$wiadomosc->zalacznik}}">{{$wiadomosc->zalacznik}}</a>
                        </div>
                    </div>
                </div>

                @endforeach

                @else
                <ul class="alert alert-info">
                    <li>Brak wiadomości</li>
                </ul>

                @endif
            </div>
        </div>
    </div>
</div>
@endsection