@extends('layouts.medbrain')

@section('content')

<div class="profil container">
    <div class="row konto">
        <div class="col s12 m12 l3 xl3">
            <div class="white panel">
                @include('frontend.profil.menu')
            </div>
        </div>

        <div class="col s12 m12 l9 xl9">


            <div class="row white panel account_detail">
                @include('frontend.profil.profil_header')

                <h3 class="title_menu" style="margin:10px 0;">Twoje dane</h3>

                <div class="uzytkownik_dane">
                    @include('backend/_main/message')
                    <div class="input-field col s12 m12 l12">
            <div class="row card padding-25 no-shadow">
              <h3 class="card-title">Użytkownik</h3>
              <form method="POST" enctype="multipart/form-data" action="{{route('profil_dane_update')}}">
                    @csrf
                    @method('PUT')

                    <div class="clearfix"></div>
                    <div style="display: none;">
                        <label>Imię i nazwisko</label>
                        <input required placeholder="Imię i nazwisko" name="name" value="{{$profil->name}}" type="text"
                            class="form-control-input validate">
                        <div class="clearfix"></div>
                    </div>
                    <label>E-mail</label>
                    <input required placeholder="E-mail" name="email" value="{{$profil->email}}" type="text"
                        class="form-control-input validate">
                    <div class="clearfix"></div>

                    <label style="display:flex;background: none !important;">
                        <input class="form-check-input" type="checkbox" id="zmien_haslo_toggle">
                        <span>
                            Zmień hasło
                        </span>
                    </label>
                    <div class="password_box" style="display:none">
                    <label>Hasło</label>
                    <input placeholder="Hasło" name="password" value="" type="password"
                        class="form-control-input validate">

                    <label>Powtórz Hasło</label>
                    <input placeholder="Powtórz Hasło" name="password_repeat" value="" type="password"
                        class="form-control-input validate">
                    </div>
                    <div style="display: none;">x
                    <label>Telefon</label>
                    <input placeholder="Telefon" value="{{$profil->telefon}}" name="telefon" type="text"
                        class="form-control-input validate">

                    <label>Logo</label>
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Plik</span>
                            <input type="file" name="avatar_upload">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>

                    <button type="submit"
                        class="m-t50 btn waves-effect waves-light btn-large green min-200">Zapisz</button>
                    </form>

                    <label>Usuwanie konta</label>
                    <form method="POST" id="usun_konto" name="usun_konto" action="{{route('profil_usun_konto')}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn waves-effect waves-light btn-danger js-alert"
                            style="background:#a91212 !important;">
                            <i class="material-icons">
                                restore_from_trash
                            </i>Usuń konto</button></form>
                </div>
            </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection