@extends('layouts.medbrain')

@section('content')
<div class="row section-faq">
    <h1 class="white-text bold">FAQ</h1>
</div>
<div class="row faq-section">

    <ol class="list list-striped">
        @foreach ($faq as $key => $f)
        <li>
            <div class="container">
                <span class="faq_count">{{($key+1)}}</span>
                {{$f->nazwa}}
                <p class="content_faq_box">
                    {{$f->opis}}
                </p>
            </div>
        </li>
        @endforeach
    </ol>
</div>
</div>
@endsection