@extends('layouts.medbrain')

@section('content')
@foreach($polityka as $p)
@if($p->stan == '1')
<div class="row center onas-section">
    <h1 class="gray-text bold">{{$p->tytul}}</h1>
</div>

<div class="row white">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="padding-box">
                    {!!$p->tresc!!}
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="white m-50" style="margin: 150px;">
    <div class="alert alert-danger blank-page">
        <h1 style="font-size:1.5rem;">Strona nie jest aktywna</h1>
    </div>
</div>
@endif
@endforeach
</div>
@endsection