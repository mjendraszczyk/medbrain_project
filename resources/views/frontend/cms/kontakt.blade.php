@extends('layouts.medbrain')

@section('content')

<div class="kontakt">
    <div class="row kontakt-section container">

        <div class="row center">

            <h6>Potrzebujesz pomocy? </h6>
            <h1 class="bold green-text">Masz pytania?</h1>
            <h4>Skontaktuj się z nami</h4>
            <div class="row section-block-contact">

                <div class="col s12 m12 l6">
                    <div class="row">
                        <div class="col s10 m8 l8 white gray-text  block-contact">
                            <img src="{{asset('img/phone.png')}}" class="icon-radius green" />
                            <p>Zadzwoń pod numer</p>
                            <br />
                            <h3 class="bold">
                                {{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}}
                            </h3>
                            <div class="chevron-bottom">
                                <a href="tel:{{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}}">
                                    <i class="material-icons">
                                        expand_less
                                    </i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l6">
                    <div class="row">
                        <div class="col s10 m8 l8 gray white-text block-contact">
                            <img src="{{asset('img/mail_icon.png')}}" class="icon-radius green" />
                            <p>Napisz do nas</p>
                            <br />
                            <h3 class="bold">
                                {{(App\Http\Controllers\Controller::getUstawienia()['email'])}}
                            </h3>
                            <div class="chevron-bottom">
                                <a href="mailto:{{(App\Http\Controllers\Controller::getUstawienia()['email'])}}"><i
                                        class="material-icons">
                                        expand_less
                                    </i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>

@endsection