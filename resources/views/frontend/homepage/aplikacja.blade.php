<div class="row panel white padding-25" id="aplikuj">
    <h5 class="panel-title">
        Aplikuj teraz
    </h5>
    <div class="panel panel-body">
        <form method="POST" action="{{route('storeAplikacje',['id'=>$id_ogloszenia])}}" enctype="multipart/form-data">
            <div class="m-15">
                @include('backend/_main/message')
            </div>
            @if(Auth::check())
            <input required placeholder="Imię i nazwisko" readonly name="imie_nazwisko" value="{{Auth::user()->name}}"
                type="text" class="form-control-input validate">
            @else
            <input required placeholder="Imię i nazwisko" name="imie_nazwisko" value="{{old('imie_nazwisko')}}"
                type="text" class="form-control-input validate">
            @endif
            @if(Auth::check())
            <input required placeholder="E-mail" readonly name="email" value="{{Auth::user()->email}}" type="text"
                class="form-control-input validate" />
            @else
            <input required placeholder="E-mail" name="email" value="{{old('email')}}" type="text"
                class="form-control-input validate" />
            @endif

            <textarea name="tresc" required placeholder="Wpisz wiadomosc do pracodawcy"
                class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
            <button type="submit" class="green btn btn-primary" style="height: 4rem;font-size: inherit;">Wyślij</button>
            <div class="file-field input-field" style="display: inline-block;vertical-align: middle;">
                <div class="btn btn-small">
                    <span>
                        <i class="material-icons">
                            backup
                        </i> Prześlij CV</span>
                    <input type="file" name="cv">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" style="margin:0;">
                </div>
            </div>
            <p style="font-size: 0.8rem;margin: 0;">Przesyłając formularz wyrażam zgodę na przetwarzanie danych drogą elektroniczną.
            </p>
            @csrf
        </form>
    </div>
</div>