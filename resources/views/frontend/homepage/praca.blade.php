<div class="row">
    @foreach($ogloszenie as $item)
    <div class="white oferta_detail">
        <div class="col s12 m12 l12">
            <div class="main_detail">
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s3 m3 l3">
                            <div class="icon-oferta">
                                <img src="{{asset('img')}}/place_icon.png" style="height: 24px;display: flex;">
                            </div>
                        </div>
                        <div class="col s9 m9 l9">
                            <h6>Miejsce pracy</h6>
                            <p>
                                @if($lokalizacja != null)
                                {{App\Http\Controllers\Controller::getMiasto($lokalizacja->id_miasta)}},
                                {{$lokalizacja->nazwa}}
                                {{$lokalizacja->ulica}}
                                <br/>
                                <span class="bold lowercase">({{App\Http\Controllers\Controller::getWojewodztwoByMiasto($lokalizacja->id_miasta)}})
                                </span>
                                @else
                                {{App\Http\Controllers\Controller::getMiasto($item->id_miasta)}},
                                {{$item->nazwa}}
                                {{$item->ulica}}
                                <br/>
                                <span class="bold lowercase">({{App\Http\Controllers\Controller::getWojewodztwoByMiasto($item->id_miasta)}})
                                </span>
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s3 m3 l3">
                            <div class="icon-oferta">
                                <img src="{{asset('img')}}/mail_icon2.png" style="height: 24px;display: flex;">
                            </div>
                        </div>
                        <div class="col s9 m9 l9">
                            <h6>Informacje kontaktowe</h6>
                            <p>
                                @if($lokalizacja != null)
                                @if($lokalizacja->kontakt_email != '' && $lokalizacja->kontakt_telefon)
                                {{$lokalizacja->kontakt_email}}
                                <br />
                                {{$lokalizacja->kontakt_telefon}}
                                @else 
                                    {{$item->podmiot_email}}
                                    <br />
                                    {{$item->podmiot_telefon}}
                                @endif
                                @else
                               {{$item->podmiot_email}}
                               <br/>
                               {{$item->podmiot_telefon}}
                               @endif
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s3 m3 l3">
                            <div class="icon-oferta">
                                <img src="{{asset('img')}}/checklist_icon.png" style="height: 24px;display: flex;">
                            </div>
                        </div>
                        <div class="col s9 m9 l9">
                            <h6>Poziom stanowiska</h6>
                            <p>
                               {{(App\Http\Controllers\Controller::getPoziom($item->id_poziom)->nazwa)}}
                            </p>
                        </div>
                    </div>
                    
                </div>
                <div class="col s12 m6 l6">
<div class="row">
    <div class="col s3 m3 l3">
        <div class="icon-oferta">
            <img src="{{asset('img')}}/user_icon.png" style="height: 24px;display: flex;">
        </div>
    </div>
    <div class="col s9 m9 l9">
        <h6>Imię i nazwisko / Firma</h6>
        <p>
            @if($item->id_lokalizacja != null && \App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia) != null)
            {{\App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia)->alias}}
            @else
            {{$item->nazwa}}
            @endif
        </p>
    </div>
</div>
<div class="row">
    <div class="col s3 m3 l3">
        <div class="icon-oferta">
            <img src="{{asset('img')}}/time_icon.png" style="height: 24px;display: flex;">
        </div>
    </div>
    <div class="col s9 m9 l9">
        <h6>Wymiar pracy</h6>
        <p>
            @foreach(App\Http\Controllers\Controller::getWymiarPracy($item->id_wymiar_pracy)
            as $wymiar)
            {{$wymiar->nazwa}}
            @endforeach
        </p>
    </div>
</div>
<div class="row">
    <a href="#aplikuj" id="aplikuj_btn" class="btn btn-primary aplikuj-btn">
        Aplikuj
    </a>
</div>
                </div>
            </div>
        </div>
        <div class="col s12 m12 l12">
            <div class="row content" style="display: block;">
                <h4 class="bold gray-text rem1-5 title-section">
                
                @if($item->id_lokalizacja != null &&
                \App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia) != null)
                {{\App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia)->alias}}
                @else
                    {{$item->nazwa}}
                @endif
                </h4>
                {!!$item->tresc!!}
                @if($item->rekruter != '')
                <a href="{{{$item->rekruter}}}" class="btn btn-primary gray btn-small">
                    Link do systemu rekrutacyjnego
                </a>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>