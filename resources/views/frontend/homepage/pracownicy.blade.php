<div class="row">
    <div class="oferta_detail">
        <div class="gray header_oferta_detail">
            <div class="col s12 m3 l3 pading-20-0 box-radius-profil no-padding">
                <div class="avatar">
                    @if($profil->avatar_upload != '')
                    <img src="{{asset('img/avatar')}}/{{$profil->avatar_upload}}">
                    @else
                    <img src="{{asset('img/icons/avatar.png')}}" />
                    @endif

                </div>
            </div>
            <div class="col s12 m9 l9 gray">
                <div class="row">
                    <div class="col s12 m8 l8  white-text">
                        <h4>{{App\Http\Controllers\Controller::getUzytkownikName($profil->id)}}</h4>
                    </div>
                    <div class="col s12 m4 l4">
                        <a href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>$profil->id_specjalizacje, 'name'=>str_slug($profil->nazwa)])}}"
                            class="btn btn-default border-white">Szukam pracy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="white profil_detail">
        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_specialization.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">Specjalizacja</h6>
                    <div class="clearfix"></div>
                    <h4 class="bold gray-text rem095">
                        @if($profil->id_specjalizacje == null)
                        Niezdefiniowano
                        @else
                        {{App\Http\Controllers\Controller::getSpecjalizacje($profil->id_specjalizacje)}}
                        @endif
                    </h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_experience.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">Doświadczenie</h6>
                    <div class="clearfix"></div>
                    @if(count($doswiadczenie) > 0)
                    @foreach($doswiadczenie as $key => $d)
                    @if($key >= 0)
                    <h4 class="bold gray-text rem095">{{$d->nazwa}}</h4>
                    <p style="margin:0;">
                        @if($d->doswiadczenie_lata_od != null)
                        {{date('Y',strtotime($d->doswiadczenie_lata_od))}}-{{date('Y',strtotime($d->doswiadczenie_lata_do))}}
                        @endif
                    </p>
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_education.png')}}" />
            </div>
            <div class="col s12 m9 l9">

                <div class="row">
                    <h6 class="uppercase rem085">Edukacja</h6>
                    <div class="clearfix"></div>
                    @if(count($edukacja) > 0)
                    @foreach($edukacja as $key => $e)
                    @if($key >= 0)
                    <h4 class="bold gray-text rem095">{{$e->nazwa}}</h4>
                    <p style="margin:0;">
                        @if($e->edukacja_lata_od != null)
                        {{-- {{App\Http\Controllers\Controller::getTytuly($e->id_tytul)->nazwa}}, --}}
                        {{date('Y',strtotime($e->edukacja_lata_od))}}-{{date('Y',strtotime($e->edukacja_lata_do))}}
                        @endif
                    </p>
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m3 l3">
                <img src="{{asset('img/icons/profil_about.png')}}" />
            </div>
            <div class="col s12 m9 l9">
                <div class="row">
                    <h6 class="uppercase rem085">O mnie</h6>
                    <div class="clearfix"></div>
                    {!!$profil->o_mnie!!}
                </div>
            </div>
        </div>
    </div>
</div>