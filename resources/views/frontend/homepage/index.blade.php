@extends('layouts.medbrain')

@section('content')
@if(Route::currentRouteName() == 'home')
@include('frontend.homepage.homepage_header')
@endif
{{-- @include('frontend._main.filtry') --}}
<div id="app2" @if(Route::currentRouteName() == 'home') class="homepage" @endif @if((Route::currentRouteName() == 'ogloszenia_zobacz')) class="ogloszenie_zobacz" @endif @if(Route::currentRouteName() == 'ogloszenia_specjalizacja_index' || Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index') class="ogloszenia_kategoria" @endif>
      
    @if(Route::currentRouteName() == 'ogloszenia_specjalizacja_index')
    <h2 class="center gray-text">
        Aktualna lista ofert pracy
    </h2>
    @endif
    <div class="section praca-row">
        @if((Route::currentRouteName() == 'ogloszenia_zobacz'))
        <h4 class="subtitle-ogloszenie">Ogłoszenie</h4>
        <h2 class="center gray-text">
            @foreach($ogloszenie as $item)
            @if($item->id_lokalizacja != null &&
            \App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia) != null)
            {{\App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($item->id_ogloszenia)->alias}}
            @else
                {{$item->nazwa}}
            @endif
            @endforeach
        </h2>
        @endif
        @if((Route::currentRouteName() == 'ogloszenia_specjalizacja_index')
        || (Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index'))
        <div class="row">
        
            @include('frontend._main.filtry_content') 
        </div>
        
        @endif
        <!--   Icon Section   -->
        <div class="">
            <div class="breadcrumb">
                
                <a href="{{route('home')}}" class="breadcrumb-item">
                    Medbra.in
                </a>
                <a class="breadcrumb-item" href="{{route('ogloszenia_specjalizacja_wojewodztwa_index',['id_wojewodztwa'=>0,'nazwa_wojewodztwa'=>'wszystkie','id_specjalizacje'=>0,'name'=>'wszystkie'])}}">
                    Ogłoszenia
                </a>
                

                @if((is_numeric(Request::route('id_wojewodztwa')) == false) && (Route::currentRouteName() != 'ogloszenia_zobacz'))
                @else
                    @if(Route::currentRouteName() == 'ogloszenia_zobacz')
                        <a class="breadcrumb-item lowercase" href="{{route('ogloszenia_specjalizacja_wojewodztwa_index',['id_wojewodztwa'=>App\Http\Controllers\Controller::getWojewodztwoIdByMiasto($ogloszenie[0]->id_miasta),'nazwa_wojewodztwa'=>str_slug(App\Http\Controllers\Controller::getWojewodztwoByMiasto($ogloszenie[0]->id_miasta)),'id_specjalizacje'=>0,'name'=>'wszystkie'])}}" class="lowercase">
                            {{-- {{dd($ogloszenie)}} --}}
                              {{App\Http\Controllers\Controller::getWojewodztwoByMiasto($ogloszenie[0]->id_miasta)}} 
                        </a>
                    @else
                    <a class="breadcrumb-item" href="{{route('ogloszenia_specjalizacja_wojewodztwa_index',['id_wojewodztwa'=>Request::route('id_wojewodztwa'),'nazwa_wojewodztwa'=>str_slug(App\Http\Controllers\Controller::getWojewodztwo(Request::route('id_wojewodztwa'))),'id_specjalizacje'=>0,'name'=>'wszystkie'])}}" class="lowercase">
                    {{App\Http\Controllers\Controller::getWojewodztwo(Request::route('id_wojewodztwa'))}}
                    </a> 
                    @endif
                @endif
{{-- {{dd($ogloszenie)}} --}}
                @if((Route::currentRouteName() == 'ogloszenia_zobacz'))
                <a class="breadcrumb-item lowercase" href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>$ogloszenie[0]->id_specjalizacje,'name'=>str_slug(App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie[0]->id_specjalizacje))])}}">{{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie[0]->id_specjalizacje)}}
                </a> 
                @else
                    @if(Request::route('id_specjalizacje') != 0)
                    <a class="breadcrumb-item" href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>Request::route('id_specjalizacje'),'name'=>str_slug(App\Http\Controllers\Controller::getSpecjalizacje(Request::route('id_specjalizacje')))])}}">{{App\Http\Controllers\Controller::getSpecjalizacje(Request::route('id_specjalizacje'))}}
                    </a>
                    @endif
                @endif
                @if((Route::currentRouteName() == 'ogloszenia_zobacz'))
                <a
                    class="breadcrumb-item" href="{{route('ogloszenia_zobacz',['id_ogloszenia'=>$ogloszenie[0]->id_ogloszenia, 'nazwa'=>str_slug($ogloszenie[0]->nazwa)])}}">
                   {{$ogloszenie[0]->nazwa}}
                </a>
                @endif

            </div>
        <div class="row praca-section @if((Route::currentRouteName() == 'ogloszenia_zobacz')) container @endif">
            @if((Route::currentRouteName() == 'ogloszenia_zobacz') || (Route::currentRouteName() == 'profil_zobacz'))
            <div class="col s12 m12 l11" style="padding:25px">
                @else
                <div class="col s12 m12 l6 no-padding">
                    @endif
                    @if((Route::currentRouteName() == 'ogloszenia_zobacz'))
                    {{-- Dla pojedyńczego ogłoszenia --}}
                    @include('frontend.homepage.praca')
                    @include('frontend.homepage.ogloszenia_firmy')
                    @include('frontend.homepage.aplikacja')
                    @elseif((Route::currentRouteName() == 'profil_zobacz'))
                    {{-- Dla pojedyńczego profilu --}}
                    @include('frontend.homepage.pracownicy')
                    @else
                    {{-- Dla listy ofert --}}
                    <div class="oferty-pracy">
                        <div class="gray header_oferta_detail white white-text list_header" style="background:#{{App\Http\Controllers\Controller::getSpecjalizacjeColor(Request::route('id_specjalizacje'))}} !important;">
                            @if((Route::currentRouteName() == 'pracownicy_specjalizacja_index') ||
                            Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index' ||
                            (Route::currentRouteName() == 'pracownicy_wojewodztwa_index') || (Route::currentRouteName()
                            == 'ogloszenia_specjalizacja_index') ||
                            (Route::currentRouteName() == 'ogloszenia_wojewodztwa_index'))

                            @if((Route::currentRouteName() == 'ogloszenia_wojewodztwa_index') ||
                            (Route::currentRouteName() == 'pracownicy_wojewodztwa_index'))

                            <div class="col s12 m3 l3 pading-20-0 box-radius-profil" style="background: #64c886;">
                            </div>
                            <div class="col s12 m12 l12">
                                <div class="row">
                                    <div class="col s12 m12 l12  white-text">
                                        <h4>
                                            @if(is_numeric(Request::route('id_wojewodztwa')) == false)
                                            Wszystkie
                                            @else
                                            {{App\Http\Controllers\Controller::getWojewodztwo(Request::route('id_wojewodztwa'))}}

                                            @endif

                                        </h4>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col s12 m12 l12">
                                <div class="row">
                                    <div class="col s12 m12 l12  white-text">
                                    @if(Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index')
                                    <h6 class="ogloszenia_wojewodztwo_name">
                                    {{App\Http\Controllers\Controller::getWojewodztwo($id_wojewodztwa)}}
                                    </h6>
                                    @endif
                                        <h4>{{App\Http\Controllers\Controller::getSpecjalizacje(Request::route('id_specjalizacje'))}}
                                        </h4>
                                    </div>
                                </div>
                            </div>

                            @endif
                            @endif
                        </div>
                        <ul class="lista">
                            @if((Route::currentRouteName() == 'pracownicy_specjalizacja_index') ||
                            (Route::currentRouteName() == 'pracownicy_wojewodztwa_index'))
                            @include('frontend._main.lista_profile')
                            @else
                            @include('frontend._main.lista_ogloszenia')
                            @endif
                        </ul>
                        @if(Route::currentRouteName() == 'home')
                        {{ $ogloszenia->links() }}
                        <div class="view-more-button btn green" style="margin:auto;display:table;">Następne</div>
                        @endif
                    </div>
                    @endif
                </div>
                @include('frontend.homepage.map')
            </div>
        </div>
        </div>
        <div class="row">
        <div class="container">
            <div class="center">
        @if(Route::currentRouteName() == 'ogloszenia_specjalizacja_index')
        @if($specjalizacja->tresc != '')
        <h1>{{$specjalizacja->nazwa }}</h1>
        {!! $specjalizacja->tresc !!}
        @endif
        @endif
        @if(Route::currentRouteName() == 'ogloszenia_wojewodztwa_index')
        @if($wojewodztwo->tresc != '')
        <h1>{{$wojewodztwo->nazwa }}</h1>
        {!! $wojewodztwo->tresc !!}
        @endif
        @endif
        </div>
        </div>
        </div>
        @if(Route::currentRouteName() == 'home')
       
        <div class="row">
            <div class="col s12 m12 l6">
                <img src="{{asset('img')}}/home_banner_left.png" style="max-width: 100%;" alt="">
            </div>
            <div class="col s12 m12 l6" style="padding: 100px 0;
margin: 0;">
<div class="box_banner">
                <h1 class="green-text bold">
                    Dalej nie szukaj
                </h1>
                <p style="max-width: 50%;">
                    Największa baza <strong>darmowych ogłoszeń</strong> o pracę w sektorze medycznym w Polsce.
                </p>
                </div>
                <div class="row" style="margin: 0;">
                    <div class="col s12 m6 l6">
                        <div class="box-grey">
                            <img src="{{asset('img')}}/home_icon_app.png" style="max-width: 100%;" alt="">
                            <p>
                                Pomoc w znalezieniu pracy i pracownika
                            </p>
                        </div>
                    </div> 
                    <div class="col s12 m6 l6">
                        <div class="box-green">
                            <img src="{{asset('img')}}/home_icon_payment.png" style="max-width: 100%;" alt="">
                            <p style="width: 100%;">
                                Brak ukrytych opłat
                            </p>
                        </div>
                    </div>
                    <a href="{{route('oglosznia_dodaj_krok1')}}" class="btn btn-default outline" style="min-width: 50%;">
                        <i class="material-icons">add</i>
                        Dodaj ogłoszenie</a>
                </div>
            </div>
        </div>
        <div class="row">
            @include('frontend._main.filtry_content')
        </div>
        <div class="home_seo">
        <div class="container">
            <div class="center">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="grey-text" style="max-width: 800px;
                        line-height: 28px;
                        font-size:18px;
                        margin: 50px auto;">
                            Jesteśmy <strong class="green-text">serwisem ogłoszeń o pracę dla pracowników</strong> w branży
                            medycznej w tym lekarzy, stomatologów,
                            farmaceutów,
                            fizjoterapeutów, pielęgniarzy, położnych, psychologów a także personelu wspomagającego, którego
                            celem
                            jest
                            <strong class="green-text">ułatwienie
                                pracodawcom dotarcia do wysoko wykwalifikowanego personelu oraz pomoc pracownikom w znalezieniu
                                spełniającej
                                ich
                                wymagania pracy.</strong>
                        </div>
                    <div class="col s12 m6 l6">
                        <p class="grey-text" style="line-height: 28px;
                        font-size:18px;
                        margin: 50px auto;">
                Medbra.in to idealne miejsce dla pracodawców poszukujących kadry medycznej. Wokół naszego portalu skupiamy społeczność
                tej konkretnej grupy zawodowej dzięki czemu ogłoszenia trafiają tylko do osób faktycznie zainteresowanych co znacząco
                zwiększa skuteczność publikowanych ogłoszeń.
                
             
                        </p>
                    </div>
                    <div class="col s12 m6 l6">
                        <p class="grey-text" style="line-height: 28px;
                        font-size:18px;
                        margin: 50px auto;">
                   Nie zwlekaj i złóż swoją aplikację właśnie teraz. Sprawdź jakie proste jest znalezienie pracy z medbra.in!
                    </p>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    {{-- <img src="{{asset('img')}}/home_seo.png"  alt=""> --}}
                </div>
                </div>
            </div>
        </div>
        </div>
        @endif
    </div>
    @endsection