<div class="container">
<div class="row white top-header">
    <div class="col s12 m12 l6 home-header">
        <div class="home-title">
            <h2 class="gray-text">
                Ponad <span class="count-offers">{{$countOgloszenia}}</span> ofert pracy
            </h2>
            <h2 class="green-text">
                w branży medycznej
            </h2>
        </div>
        <div class="home-search-form">
            <form style="display:flex;width:100%;flex-wrap:wrap;" method="POST"
                action="{{route('ogloszenia_specjalizacja_wojewodztwa_store')}}">
                @csrf
                <select name="specjalizacja">
                    {{-- v-model="values['specjalizacja']" --}}
                    <option value="" disabled selected>np. dermatologia (wymagane)</option>
                    @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
                    <option value="{{$specjalizacja->id_specjalizacje}}" @if(($specjalizacja->id_specjalizacje ==
                        Session::get('form_specjalizacja')) || ($specjalizacja->id_specjalizacje ==
                        old('specjalizacja')))
                        selected="selected"
                        @endif>{{$specjalizacja->nazwa}}</option>
                    @endforeach
                </select>
                <select name="id_wojewodztwa" class="zmien_miasta form-control-input validate">
                    <option value="0" @if(old('id_wojewodztwa')=='0' ) selected="selected" @endif>
                        wszystkie
                    </option>
                    @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                    <option value="{{$wojewodztwo->id_wojewodztwa}}" @if(old('id_wojewodztwa')==$wojewodztwo->
                        id_wojewodztwa)
                        selected="selected" @endif>
                        {{$wojewodztwo->nazwa}}
                    </option>
                    @endforeach
                </select>
                <button type="submit" class="green white-text home-search">
                    <i class="material-icons">search</i>
                    Znajdź pracę
                </button>
            </form>
        </div>
    </div>
    <div class="col s12 m12 l6" style="text-align: right;">
        <img src="{{asset('img')}}/home_banner_right.png" style="max-width: 100%;" alt="">
    </div>
</div>
</div>
{{-- <div class="section no-pad-bot" id="slider-banner">
    <div class="slogan-slider">
        <div class="container">
            <div class="slider-content">
                <h6 class="center gray-text">{{ config('app.name') }}</h6>
<h1 class="header center gray-text">Portal z ofertami pracy</h1>
<div class="row center">
    <h3 class="green-text">Dla branży medycznej</h3>
</div>

<div style="display: none;position: relative;background: #fff;padding: 10px;margin-top: 15px;">
    <form style="display:flex;width:100%;" method="POST" action="{{route('frontend_post_find_firma')}}">
        @csrf
        <i class="material-icons" style="display: flex;justify-content: center;align-items: center;">search</i><input
            name="firma" type="text" placeholder="Wyszukaj placówki medyczne" id="search_podmiot" name="search"
            style="display: flex;align-content: center;justify-content: center;border-radius: 0px !important;">

    </form>
</div>
</div>
</div>

</div>
</div> --}}