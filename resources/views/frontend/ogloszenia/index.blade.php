@extends('layouts.medbrain')

@section('content')
<div class="row container ogloszenia_index">
    <div class="col s12 m12 l6 ogloszenia_publikacja">
        <h1 class="white-text bold">Szukasz <br /> pracownika?</h1>
        <p class="white-text">Skorzystaj z możliwości <br />zamieszczenia ogłoszenia</p>
        <a href="{{route('oglosznia_dodaj_krok1')}}" class="btn waves-effect waves-light btn-large gray">Stwórz
            ogłoszenie</a>
    </div>
    <div class="col s12 m12 l6 ogloszenia_lekarz">
        <h1 class="white-text bold">Szukasz <br />pracy?</h1>
        <p class="white-text">Stwórz swoje konto <br /> na portalu {{config('app.name')}}</p>
        <a href="{{route('konto')}}" class="btn waves-effect waves-light btn-large green">Załóz swój profil</a>
    </div>
</div>
<div class="container">
    <div class="row ogloszenia-section">
        <div class="col s12 m12 l6">
            <h2 class="title-section bold gray-text">Co Ci daje <span class="green-text">publikacja ogłoszenia</span> na
                platformie {{config('app.name')}}</h2>
            <div class="oferty-pracy">
                <ul class="lista">
                    <li>
                        <span class="icon-specjalizacja-ogloszenia">
                            <img src="{{asset('img/icons/clock.png')}}" />
                        </span>
                        <p><strong>Publikuj</strong> szybko i wygodnie</p>

                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia">
                            <img src="{{asset('img/icons/doctor.png')}}" />
                        </span>
                        <p>
                            <strong>Docieraj</strong> do najlepszych pracowników
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia">
                            <img src="{{asset('img/icons/country.png')}}" />
                        </span>
                        <p>
                            <strong>Modyfikuj</strong> treść ogłoszeń
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia">
                            <img src="{{asset('img/icons/country.png')}}" />
                        </span>
                        <p>
                            <strong>Wybierz</strong> dowolne miasto
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia">
                            <img src="{{asset('img/icons/social.png')}}" />
                        </span>
                        <p>
                            <strong>Połączenie z profilem</strong> w social mediach
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col s12 m12 l6">
            <h2 class="title-section bold gray-text">Co Ci daje założenie <strong class="green-text">profilu
                    lekarskiego</strong> na platformie {{config('app.name')}}</h2>
            <div class="oferty-pracy">
                <ul class="lista">
                    <li>
                        <span class="icon-specjalizacja-ogloszenia gray">
                            <img src="{{asset('img/icons/offer.png')}}" />
                        </span>
                        <p>
                            <strong>Stały dostęp</strong> do ofert pracy
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia gray">
                            <img src="{{asset('img/icons/find.png')}}" />
                        </span>
                        <p>
                            <strong>Stań się widoczny</strong> dla pracodawcy
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia gray">
                            <img src="{{asset('img/icons/user_add.png')}}" />
                        </span>
                        <p>
                            <strong>Stała dostępność</strong> profilu w serwisie
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia gray">
                            <img src="{{asset('img/icons/mail.png')}}" />
                        </span>
                        <p>
                            <strong>Otrzymywanie ofert</strong> na skrzynkę mailową
                        </p>
                    </li>
                    <li>
                        <span class="icon-specjalizacja-ogloszenia gray">
                            <img src="{{asset('img/icons/social.png')}}" />
                        </span>
                        <p>
                            <strong>Łatwe logowanie</strong> przez social media
                        </p>
                    </li>
                </ul>
            </div>



        </div>

    </div>
</div>
@endsection