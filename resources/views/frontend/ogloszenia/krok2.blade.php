@extends('layouts.medbrain')

@section('content')
<div class="row center ogloszenia_create krok2 padding-box">
  <div class="container">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s20 steep">
      </li>
      <li class="col s20  steep current">
        <span class="steep-legend">
          Krok 2
        </span>
      </li>
      <li class="col s20  steep">

      </li>
      <li class="col s20  steep">

      </li>
      <li class="col s20  steep">
      
      </li>
    </ul>
  </div>

  <div class="row white card padding-50 radius-5">
    <img src="{{asset('img')}}/krok2_icon.png" alt="">
    <h2 class="gray-text bold rem1-75">Wybierz specjalizację</h2>
    <h6>z listy ponizej</h6>
    <form method="POST" id="app" action="{{route('krok2Request')}}" novalidate>
      @include('backend/_main/message')
      @csrf
      <div class="input-field col l8 offset-l2">
        <div class="row">
          <div class="col s12 m12 l12">
            <select name="specjalizacja" required>
            
              <option value="" disabled selected>np. dermatologia (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
              <option value="{{$specjalizacja->id_specjalizacje}}" @if(($specjalizacja->id_specjalizacje ==
                Session::get('form_specjalizacja')) || ($specjalizacja->id_specjalizacje == old('specjalizacja')))
                selected="selected"
                @endif>{{$specjalizacja->nazwa}}</option>
              @endforeach
            </select>
            {{-- <select name="szukam" required>
              <option value="" disabled selected>Szukam (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getTypOgloszenia(null) as $typ)
              <option value="{{$typ->id_typ_ogloszenia}}" @if(($typ->id_typ_ogloszenia == Session::get('form_szukam'))
                || ($typ->id_typ_ogloszenia == old('szukam')))
                selected="selected"
                @endif>{{$typ->nazwa}}</option>
              @endforeach
            </select> --}}

            <div class="row">
              <div class="col s12 m6 l6">
                <input style="text-indent: -1px;" type="hidden" placeholder="Doswiadczenie od"
                  @if(Session::get('form_doswiadczenie_od') !='' ) value="{{Session::get('form_doswiadczenie_od')}}"
                  @else value="{{old('doswiadczenie_od')}}" @endif name="doswiadczenie_od" type="number" min="0"
                  class="form-control-input validate" />
              </div>
              <div class="col s12 m6 l6">
                <input style="text-indent: -1px;" type="hidden" placeholder="Doswiadczenie do"
                  @if(Session::get('form_doswiadczenie_do') !='' ) value="{{Session::get('form_doswiadczenie_do')}}"
                  @else value="{{old('doswiadczenie_do')}}" @endif name="doswiadczenie_do" type="number" min="0"
                  class="form-control-input validate" />
              </div>
            </div>
      
            {{-- <select name="poziom" required>
              <option value="" disabled selected>Stanowisko (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
              <option value="{{$poziom->id_poziom}}" @if(($poziom->id_poziom == Session::get('form_poziom')) ||
                ($poziom->id_poziom == old('poziom')))
                selected="selected"
                @endif>{{$poziom->nazwa}}</option>
              @endforeach
            </select> --}}
            
          </div>
        </div>
        <div class="row">
          <div class="col s12 m6 l6">
            <!-- multiple -->
            <select name="typ_umowy" required style="padding-right: 10px">
              <option value="" disabled selected>Rodzaj umowy (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getRodzajUmowy(null) as $rodzaj)
              <option value="{{$rodzaj->id_rodzaj_umowy}}" @if(($rodzaj->id_rodzaj_umowy ==
                Session::get('form_typ_umowy')) || ($rodzaj->id_rodzaj_umowy == old('typ_umowy')))
                selected="selected"
                @endif>{{$rodzaj->nazwa}}</option>
              @endforeach
            </select>
          </div>
          <div class="col s12 m6 l6">
              <select name="wymiar_pracy" required>
                <option value="" disabled selected>Wymiar pracy (wymagane)</option>
                @foreach(App\Http\Controllers\Controller::getWymiarPracy(null) as $wymiar)
                <option value="{{$wymiar->id_wymiar_pracy}}" @if(($wymiar->id_wymiar_pracy ==
                  Session::get('form_wymiar_pracy')) || ($wymiar->id_wymiar_pracy == old('wymiar_pracy')))
                  selected="selected"
                  @endif>{{$wymiar->nazwa}}</option>
                @endforeach
              </select>
            </div>
        </div>
        <div class="row">
          <div class="col s12 m6 l3" style="padding-right: 15px;">
            <input required placeholder="Pensja od" @if(Session::get('form_wynagrodzenie_od') !='' )
              value="{{Session::get('form_wynagrodzenie_od')}}" @else value="{{old('wynagrodzenie_od')}}" @endif
              name="wynagrodzenie_od" step="0.01" type="number" min="0" class="form-control-input validate"  />
          </div>
          <div class="col s12 m6 l3" style="padding-right: 15px;">
            <input required placeholder="Pensja do" @if(Session::get('form_wynagrodzenie_do') !='' )
              value="{{Session::get('form_wynagrodzenie_do')}}" @else value="{{old('wynagrodzenie_do')}}" @endif
              name="wynagrodzenie_do" step="0.01" type="number" min="0" class="form-control-input validate" />
          </div>
          <div class="col s12 m6 l3">
            <select name="typ_wynagrodzenia" required>
              <option value="" disabled selected>Typ wynagrodzenia (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getTypWynagrodzenia(null) as $typ)
              <option value="{{$typ->id_typ_wynagrodzenia}}" @if(($typ->id_typ_wynagrodzenia ==
                Session::get('form_typ_wynagrodzenia')) || ($typ->id_typ_wynagrodzenia == old('typ_wynagrodzenia')))
                selected="selected"
                @endif>{{$typ->nazwa}}</option>
              @endforeach
            </select>
          </div>
          <div class="col s12 m6 l3">
            <select name="waluta_wynagrodzenia" required>
              <option value="" disabled selected>Waluta (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getWalutaWynagrodzenia(null) as $waluta)
              <option value="{{$waluta->id_waluta_wynagrodzenia}}" @if(($waluta->id_waluta_wynagrodzenia ==
                Session::get('form_waluta_wynagrodzenia')) || ($waluta->id_waluta_wynagrodzenia ==
                old('waluta_wynagrodzenia')))
                selected="selected"
                @endif>{{$waluta->nazwa}}</option>
              @endforeach
            </select>
</div>
        </div>
        {{-- <div class="row">
          <div class="col s12">
            @if(Session::get('form_tresc') != '')
            <textarea name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
              class="form-control-area materialize-textarea validate">{{Session::get('form_tresc')}}</textarea>
            @else
            <textarea name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
              class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
            @endif
          </div>
        </div> --}}
        <a href="{{route('oglosznia_dodaj_krok1')}}"
          class="btn-grey-gradient m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
        <button type="submit" class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
      </div>


    </form>
  </div>
  </div>
</div>

@endsection