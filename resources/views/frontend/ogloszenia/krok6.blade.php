@extends('layouts.medbrain')

@section('content')
{{-- <div class="row center ogloszenia_confirmation padding-box krok_5">
    <div class="container col s12 m6 l6 offset-l4 offset-m4 text-left">
        <h1 class="bold green-text">Gratulacje!</h1>

        <h6 class="bold m-t50 m-d30 gray-text">Właśnie założyłeś swoje konto na {{config('app.name')}}. Od teraz możesz
            publikować ogłoszenia oraz założyć swoj profil lekarski.
        </h6>
        <p class="gray-text">
            Potwierdź swoje konto oraz hasło linkiem
            dostarczonym na podany adres e-mail
        </p>
        <a href="{{route('profil_dane')}}" class="btn-green-gradient green-gradient btn waves-effect waves-light btn-large border-white">Przejdź do
            profilu</a>
    </div>
</div> --}}
<div class="row center ogloszenia_confirmation padding-box krok_5">
    <div class="container col s12 m6 l6 offset-l4 offset-m4 text-left">
        <h1 class="bold green-text">Gratulacje!</h1>

        <h6 class="bold m-t50 m-d30 gray-text">Właśnie dodałeś ogłoszenie na {{config('app.name')}}.
        </h6>
        <p class="gray-text">
            Twoje ogłoszenie będzie widoczne w seriwisie.
        </p>
        <a href="{{route('ogloszenia_zobacz',['id'=>$id,'nazwa'=>str_slug($nazwa)])}}"
            class="btn-green-gradient green-gradient btn waves-effect waves-light btn-large border-white">Przejdź do
            ogłoszenia</a>
    </div>
</div>
@endsection