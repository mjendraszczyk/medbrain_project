@extends('layouts.medbrain')

@section('content')
<div class="row  center ogloszenia_create krok4 padding-box">
  <div class="container">
    <h6 class="bold gray-text">Nie czekaj</h6>
    <h1 class="green-text bold center">dodaj ogłoszenie</h1>
    <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

    <div class="row">
      <ul class="steeps col s8 offset-s2">
        <li class="col s20 steep">
        </li>
        <li class="col s20  steep">

        </li>
        <li class="col s20 steep">
        </li>
        <li class="col s20  steep current">
          <span class="steep-legend">
            Krok 4
          </span>
        </li>
        <li class="col s20  steep">

        </li>
      </ul>
    </div>

    <div class="row white card padding-50 radius-5">
      <h2 class="gray-text bold rem1-75">Wybierz swoją placówkę</h2>

      <form method="POST" id="app" action="{{route('krok4bRequest')}}">
        @include('backend/_main/message')
        @csrf
        <div class="row">
         @foreach($lokalizacje as $key => $lokalizacja)
         <div class="col s12 m6 l6">
         <div class="panel" style="margin: 25px 0;box-shadow: 0px 0px 10px rgba(0,0,0,0.1);padding: 20px;"> 
           <label>
              <input class="form-check-input" type="radio" @if($key == 0) checked="checked" @endif value="{{$lokalizacja->id_lokalizacja}}" name="id_lokalizacja" id="id_lokalizacja" {{ old('id_lokalizacja') ? 'radio' : '' }}>
            
              <span>
              <h6>{{$lokalizacja->alias}}</h6>
              <p>
                {{$lokalizacja->ulica}}
                <br>
                {{App\Http\Controllers\Controller::getMiasto($lokalizacja->id_miasta)}} <br />
                {{App\Http\Controllers\Controller::getKrajByIdMiasto($lokalizacja->id_miasta)}}
              </span>
            
            </label>
         </div>
         </div>
         
         @endforeach
         <a href="{{route('profil_lokalizacje_create')}}?status=krok4" class="btn btn-primary green">Dodaj lokalizację</a>
         </div>
          <a href="{{route('oglosznia_dodaj_krok3')}}"
            class="btn-grey-gradient m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
          <button type="submit"
            class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
        </div>


      </form>
    </div>
  </div>
</div>

@endsection