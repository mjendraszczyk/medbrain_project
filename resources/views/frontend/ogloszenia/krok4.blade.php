@extends('layouts.medbrain')

@section('content')
<div class="row  center ogloszenia_create krok4 padding-box">
  <div class="container">
    <h6 class="bold gray-text">Nie czekaj</h6>
    <h1 class="green-text bold center">dodaj ogłoszenie</h1>
    <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

    <div class="row">
      <ul class="steeps col s8 offset-s2">
        <li class="col s20 steep">
        </li>
        <li class="col s20  steep">

        </li>
        <li class="col s20 steep">
        </li>
        <li class="col s20  steep current">
          <span class="steep-legend">
            Krok 4
          </span>
        </li>
        <li class="col s20  steep">

        </li>
      </ul>
    </div>

    <div class="row white card padding-50 radius-5">
      <h2 class="gray-text bold rem1-75">Uzupełnij dane kontaktowe</h2>
{{-- {{route('krok4Request')}} --}}
      <form method="POST" id="app" enctype="multipart/form-data" action="{{route('krok4Request')}}">
        @include('backend/_main/message')
        @csrf
        <div class="input-field col s8 offset-s2">
          <div class="row">
            <div class="col s12 m12 l12">
              <input placeholder="Nazwa firmy" name="podmiot" @if(Session::get('form_podmiot') !='' )
                value="{{Session::get('form_podmiot')}}" @else value="{{old('podmiot')}}" @endif type="text"
                class="form-control-input validate">
            </div>
            <div class="col s12 m12 l12">
              <input placeholder="NIP" name="nip" @if(Session::get('form_nip') !='' )
                value="{{Session::get('form_nip')}}" @else value="{{old('nip')}}" @endif type="hidden"
                class="form-control-input validate">
            </div>

            <div class="col s12 m12 l12">
              <input placeholder="Imię i nazwisko" name="imie_nazwisko" @if(Session::get('form_imie_nazwisko') !='' )
                value="{{Session::get('form_imie_nazwisko')}}" @else value="{{old('imie_nazwisko')}}" @endif type="hidden"
                class="form-control-input validate">
            </div>

            <div class="col s12 m6 l6">
              <input placeholder="Ulica" name="ulica" @if(Session::get('form_ulica') !='' )
                value="{{Session::get('form_ulica')}}" @else value="{{old('ulica')}}" @endif type="text"
                class="form-control-input validate">

              {{-- <input placeholder="Adres e-mail" @if(Session::get('form_adres_email') !='' )
                value="{{Session::get('form_email')}}" @else value="{{old('adres_email')}}" @endif name="adres_email"
                type="email" class="form-control-input validate"> --}}

              <select name="id_rodzaj_placowki">
                <option value="" disabled selected>Rodzaj placówki</option>
                @foreach ($rodzaje_placowki as $placowka)
                <option value="{{$placowka->id_rodzaj_placowki}}" @if(($placowka->id_rodzaj_placowki ==
                  Session::get('form_rodzaj_placowki')) || ($placowka->id_rodzaj_placowki == old('id_rodzaj_placowki')))
                  selected="selected"
                  @endif>{{$placowka->nazwa}}</option>
                @endforeach
              </select>

              <input placeholder="E-mail" @if(Session::get('form_email') !='' ) value="{{Session::get('form_email')}}" @else
                value="{{old('kontakt_email')}}" @endif name="kontakt_email" type="text" class="form-control-input validate">
            </div>

            <div class="col s12 m6 l6">
              <select name="id_wojewodztwa" class="zmien_miasta form-control-input validate">
                @foreach (App\Http\Controllers\Controller::getWojewodztwo(null) as $wojewodztwo)
                <option value="{{$wojewodztwo->id_wojewodztwa}}" @if(old('id_wojewodztwa')==$wojewodztwo->
                  id_wojewodztwa)
                  selected="selected" @endif>
                  {{$wojewodztwo->nazwa}}
                </option>
                @endforeach
              </select>
              @if(old('id_wojewodztwa') != '' )
              <select name="miasto" class="form-control-input validate"
                data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(old('miasto'))}}">
                <option value="" disabled>Miasto</option>

                @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(old('miasto')))
                as
                $miasto)
                <option @if(old('miasto')==$miasto->id_miasta) selected="selected" @endif
                  value="{{$miasto->id_miasta}}">{{$miasto->nazwa}}</option>
                @endforeach
              </select>
              @else
              <select name="miasto" class="form-control-input validate"
                data-wojewodztwo="{{App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1)}}">
                <option value="" disabled>Miasto</option>

                @foreach(App\Http\Controllers\Controller::getMiastaByWojewodztwo(App\Http\Controllers\Controller::getWojewodztwoIdByMiasto(1))
                as
                $miasto)
                <option @if(old('miasto')==$miasto->id_miasta) selected="selected" @endif
                  value="{{$miasto->id_miasta}}">{{$miasto->nazwa}}</option>
                @endforeach
              </select>
              @endif
              <input placeholder="Telefon" @if(Session::get('form_telefon') !='' )
                value="{{Session::get('form_telefon')}}" @else value="{{old('telefon')}}" @endif name="telefon"
                type="text" class="form-control-input validate">


            </div>
            
          </div>
          <div class="row">
            <label>Logo</label>
            <div class="file-field input-field">
              <div class="btn">
                <span>Plik</span>
                <input type="file" name="avatar_upload">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
          </div>
          <input type="hidden" name="lat" id="lat" @if(Session::get('form_lat') !='' )
            value="{{Session::get('form_lat')}}" @else value="{{old('lat')}}" @endif />
          <input type="hidden" name="lon" id="lon" @if(Session::get('form_lon') !='' )
            value="{{Session::get('form_lon')}}" @else value="{{old('lon')}}" @endif />
          <div class="clearfix"></div>
          <div>
            <div style="display:none;">
            <label for="mapLatLon">Wybierz lokalizację</label>
            <div class="alert alert-danger" style="margin: 25px 0;">
              Wybierz lokalizację klikająć punkt na mapie
            </div>
            
            <div id="mapLatLon"></div>
            </div>
          </div>
          <a href="{{route('oglosznia_dodaj_krok3')}}"
            class="btn-grey-gradient m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
          <button type="submit"
            class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
        </div>


      </form>
    </div>
  </div>
</div>

@endsection