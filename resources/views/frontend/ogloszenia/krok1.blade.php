@extends('layouts.medbrain')

@section('content')
<div class="row  center ogloszenia_create krok1 padding-box">
  <div class="container">
  <h6 class="bold gray-text">Nie czekaj</h6>
  <h1 class="green-text bold center">dodaj ogłoszenie</h1>
  <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

  <div class="row">
    <ul class="steeps col s8 offset-s2">
      <li class="col s20 steep current">
        <span class="steep-legend">
          Krok 1
        </span>
      </li>
      <li class="col s20  steep">

      </li>
      <li class="col s20  steep">

      </li>
      <li class="col s20  steep">

      </li>
      <li class="col s20  steep">

      </li>
    </ul>
  </div>

  @include('auth.login_form')

  {{-- <div class="row white card ogloszenie-krok1 padding-50 radius-5">
    <h2 class="gray-text bold rem1-75">Wybierz specjalizację</h2>
    <h4 class="gray-text rem1-25">z listy poniżej</h4>



    <form method="POST" id="app" novalidate @submit="checkForm" action="{{route('krok1Request')}}">
      @include('backend/_main/message')
      @csrf
      <div class="input-field col l6 s12 offset-l3">
        <select name="specjalizacja" required>
          
          <option value="" disabled selected>np. dermatologia (wymagane)</option>
          @foreach(App\Http\Controllers\Controller::getSpecjalizacje(null) as $specjalizacja)
          <option value="{{$specjalizacja->id_specjalizacje}}" @if(($specjalizacja->id_specjalizacje ==
            Session::get('form_specjalizacja')) || ($specjalizacja->id_specjalizacje == old('specjalizacja')))
            selected="selected"
            @endif>{{$specjalizacja->nazwa}}</option>
          @endforeach
        </select>
        <button type="submit" class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
      </div>


    </form>
  </div> --}}
  </div>
</div>

@endsection