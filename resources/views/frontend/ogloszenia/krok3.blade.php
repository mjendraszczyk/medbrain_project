@extends('layouts.medbrain')

@section('content')
<div class="row center ogloszenia_create krok3 padding-box">
  <div class="container">
    <h6 class="bold gray-text">Nie czekaj</h6>
    <h1 class="green-text bold center">dodaj ogłoszenie</h1>
    <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

    <div class="row">
      <ul class="steeps col s8 offset-s2">
        <li class="col s20 steep">
        </li>
        <li class="col s20  steep">
        </li>
        <li class="col s20  steep current">
      <span class="steep-legend">
            Krok 3
          </span>
        </li>
        <li class="col s20  steep">

        </li>
        <li class="col s20  steep">

        </li>
      </ul>
    </div>

    <div class="row white card padding-50 radius-5">
      <img src="{{asset('img')}}/krok3_icon.png" alt="">
      <h2 class="gray-text bold rem1-75">Uzupełnij tresc ogłoszenia</h2>
      <form method="POST" id="app" action="{{route('krok3Request')}}" novalidate>
        @include('backend/_main/message')
        @csrf
        <div class="input-field col l8 offset-l2">
          <div class="row">
            <div class="col s12 m12 l12">
               
               <select name="poziom" required>
              <option value="" disabled selected>Stanowisko (wymagane)</option>
              @foreach(App\Http\Controllers\Controller::getPoziom(null) as $poziom)
              <option value="{{$poziom->id_poziom}}" @if(($poziom->id_poziom == Session::get('form_poziom')) ||
              ($poziom->id_poziom == old('poziom')))
              selected="selected"
              @endif>{{$poziom->nazwa}}</option>
              @endforeach
              </select>  

            </div>
          </div>

          <div class="row">
            <div class="col s12">
              @if(Session::get('form_tresc') != '')
              <textarea class="editor" name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
                class="form-control-area materialize-textarea validate">{{Session::get('form_tresc')}}</textarea>
              @else
              <textarea class="editor" name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
                class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
              @endif
            </div>
          </div>

          <div style="margin-top:15px;">
            <input placeholder="Link do systemu rekrutacyjnego" name="rekruter" @if(Session::get('form_rekruter') !='' )
              value="{{Session::get('form_rekruter')}}" @else value="{{old('rekruter')}}" @endif type="text"
              class="form-control-input validate">
          </div>
          
          {{-- <div class="row">
          <div class="col s12">
            @if(Session::get('form_tresc') != '')
            <textarea name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
              class="form-control-area materialize-textarea validate">{{Session::get('form_tresc')}}</textarea>
          @else
          <textarea name="tresc" required placeholder="Wpisz treść ogloszenia (wymagane)"
            class="form-control-area materialize-textarea validate">{{old('tresc')}}</textarea>
          @endif
        </div>
    </div> --}}
    <a href="{{route('oglosznia_dodaj_krok2')}}"
      class="btn-grey-gradient m-t50 btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">Powrót</a>
    <button type="submit"
      class="btn-green-gradient m-t50 btn waves-effect waves-light btn-large green min-200">Dalej</button>
  </div>


  </form>
</div>
</div>
</div>

@endsection