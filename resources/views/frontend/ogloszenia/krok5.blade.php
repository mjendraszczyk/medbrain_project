@extends('layouts.medbrain')

@section('content')
<div class="row  center ogloszenia_create krok5 padding-box">
    <div class="container">
        <h6 class="bold gray-text">Nie czekaj</h6>
        <h1 class="green-text bold center">dodaj ogłoszenie</h1>
        <h3 class="gray-text center rem1-25 m20" style="">w <span class="rem1-75">5</span> prostych krokach</h3>

        <div class="row">
            <ul class="steeps col s8 offset-s2">
                <li class="col s20 steep">
                </li>
                <li class="col s20  steep">
                </li>
                <li class="col s20 steep">
                </li>
                <li class="col s20  steep ">
                    
                </li>
                <li class="col s20 steep current">
                    <span class="steep-legend">
                        Krok 5
                    </span>
                </li>
            </ul>
        </div>

        <div class="row white card padding-50 radius-5">
            <h2 class="gray-text bold rem1-75">Podsumowanie</h2>

            @include('backend/_main/message')

            <div class="input-field col s10 offset-s1">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row">
                            <div class="col s12 c6 l6 text-left">
                                Szukam
                            </div>
                            <div class="col s12 c6 l6 text-left">
                                <strong>{{$szukam}}</strong>
                            </div>

                        </div>
                        <hr />
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="row">
                            <div class="col s12 c6 l6 text-left">
                                Specjalizacja
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$specjalizacja}}</strong>
                            </div>
                        </div>
                        <hr />
                    </div>

                </div>

                <div class="row">
                    <div class="col s12 m6 l6 text-left">
                        <h6 class="uppercase full-width rem08 m-15">Wymagania</h6>
                        <div class="row">
                            <div style="display: none;">
                            <div class="col s12 c6 l6">
                                Doświadczenie
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$doswiadczenie_od}}-{{$doswiadczenie_do}}</strong>
                            </div>
                        </div>
                            <div class="col s12 c6 l6">
                                Stanowisko
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$poziom}}</strong>
                            </div>
                        </div>
                        <h6 class="uppercase full-width rem08 m-15">Informacje o pracy</h6>
                        <div class="row">
                            <div class="col s12 c6 l6">
                                Wymiar pracy
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$wymiar_pracy}}</strong>
                            </div>
                            <div class="col s12 c6 l6">
                                Rodzaj umowy
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>
                                    <strong>{{$typ_umowy}}</strong>
                                </strong>
                            </div>
                            <div class="col s12 c6 l6">
                                Wynagrodzenie
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>
                                    @if($wynagrodzenie_od == 0)
                                    -
                                    @else
                                    {!!
                                    App\Http\Controllers\Controller::parseMoneyFormat($wynagrodzenie_od, '') !!}
                                    <br>
                                    @if($wynagrodzenie_do != null)
                                    -
                                    {!!
                                    App\Http\Controllers\Controller::parseMoneyFormat($wynagrodzenie_do, '') !!}
                                    @endif
                                    @endif
                                </strong>
                            </div>
                        </div>
                        <h6 class="uppercase full-width rem08 m-15">Dane lokalizacyjne</h6>
                        <div class="row">
                            <div class="col s12 c6 l6">
                                Miasto
                            </div>
                            <div class="col s12 c6 l6">
                                {{$miasto}}
                            </div>
                            <div class="col s12 c6 l6">
                                Rodzaj placówki
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$rodzaj_placowki}}</strong>
                            </div>
                        </div>
                    </div>


                    <div class="col s12 m6 l6 text-left">
                        <h6 class="uppercase full-width rem08 m-15">Dane kontaktowe</h6>
                        <div class="row">
                            <div class="col s12 c6 l6">
                                Podmiot
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$imie_nazwisko}}</strong>
                            </div>
                            <div class="col s12 c6 l6">
                                E-mail
                            </div>
                            <div class="col s12 c6 l6">
                                <strong>{{$email}}</strong>
                            </div>
                        </div>

                        <h6 class="uppercase full-width rem08 m-15">Treść ogłoszenia</h6>
                        <div class="row">
                            <div class="col s12 c12 l12">
                                {{str_limit($tresc,150)}}
                                {{-- <a href="#" class="btn-default table text-gray bold">Czytaj więcej >></a> --}}
                            </div>

                        </div>
                    </div>




                </div>
                {{-- {{route('krok5Request')}} --}}
                <form method="POST" id="app" action="{{route('krok5Request')}}" style="display: block;"
                    class="m-t50 white-text pointer">
                    <label style="display:block;">
                        <input class="form-check-input" required type="checkbox" name="accept">
                        <span style="font-size: 11px;line-height: initial;margin-bottom: 25px;">
                            Zapoznałem się i akceptuję postanowienia <a href="{{route('regulamin')}}"
                                style="font-size: 11px;color:#9e9e9e;">regulaminu</a>.</span></span></label>
                    <div class="row">
                        <p class="col l6 m12 offset-l3" style="font-size: 11px;line-height: initial;color:#9e9e9e;">
                            Jednym z Naszym
                            priorytetów
                            jest dbanie o Państwa dane osobowe, a o tym
                            jak to robimy, a przede wszystkim do czego te
                            dane wykorzystujemy możecie Państwo przeczytać na naszej stronie medbra.in, w dedykowanej do
                            tego celu zakładce:
                            Polityka prywatności</p>
                    </div>
                    @csrf
                    @if(Auth::check() == false)
                    <a href="{{route('ogloszenia_dodaj_krok4')}}"
                        class="btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">
                        @else
                        <a href="{{route('ogloszenia_dodaj_krok4')}}"
                            class="btn-grey-gradient btn waves-effect waves-light btn-large gray-text border-btn min-200 pointer">
                            @endif
                            Powrót</a>

                        <button type="submit"
                            class="btn-green-gradient pointer white-text btn waves-effect waves-light btn-large green pointer">
                            Dodaj ogłoszenie
                        </button>
                </form>


            </div>
        </div>
    </div>
</div>


@endsection