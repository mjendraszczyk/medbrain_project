@extends('layouts.medbrain')

@section('content')

@foreach($blog as $b)

<div class="blog-single row center onas-section">

</div>
<div class="blog row white">
    <div class="container">
        <div class="blog_image"
            style="position: relative;top:-250px;background-image:url({{asset('img/blog/'.$b->image)}});">
        </div>
        <div class="row" style="top:-200px; position:relative;">
            <h1 class="black-text bold">{{$b->tytul}}</h1>
            {!! $b->tresc !!}
        </div>
    </div>
</div>
@endforeach
</div>
@endsection