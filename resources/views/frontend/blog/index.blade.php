@extends('layouts.medbrain')

@section('content')

<div class="row center onas-section">
    <h1 class="gray-text bold">Blog</h1>
</div>
<div class="blog row white">
    <div class="container">
        <div class="row">
            @foreach($items as $blog)
            <div class="col l6 blog-detail">
                <div class="blog_image" style="background-image:url({{asset('img/blog/'.$blog->image)}});">
                </div>
                <h4 class="bold">{{$blog->tytul}}</h4>
                <p>{!! str_limit($blog->tresc, 250) !!}</p>
                <a href="{{route('blog_show',['id'=>$blog->id_blog])}}" class="btn btn-primary green">Czytaj więcej</a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection