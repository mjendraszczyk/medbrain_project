﻿@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($routes_array as $key => $route)
    <url>
        <loc>{{$route}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>
    @endforeach
    @foreach($getOgloszenia as $ogloszenie)
    <url>
        <loc>{{route('ogloszenia_zobacz',['id'=>$ogloszenie->id_ogloszenia, 'nazwa'=>str_slug($ogloszenie->nazwa)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    @endforeach

    @foreach($getSpecjalizacje as $specjalizacja)
    <url>
        <loc>{{route('ogloszenia_specjalizacja_index',['id'=>$specjalizacja->id_specjalizacje, 'nazwa'=>str_slug($specjalizacja->nazwa)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    @endforeach

@foreach($getWojewodztwa as $wojewodztwo)
<url>
    <loc>
        {{route('ogloszenia_specjalizacja_wojewodztwa_index',['id_specjalizacje'=>0,'name'=>'wszystkie', 'id_wojewodztwa'=>$wojewodztwo->id_wojewodztwa, 'nazwa'=>str_slug($wojewodztwo->nazwa)])}}
    </loc>
    <lastmod>{{date('Y-m-d')}}</lastmod>
    <changefreq>daily</changefreq>
    <priority>0.5</priority>
</url>
@endforeach

{{-- 
    @foreach($getMiasta as $miasto)
    <url>
        <loc>
            {{route('eksperts_by_filters',['wojewodztwo'=>$miasto->id_province, 'wojewodztwo_name' => str_slug(\App\Http\Controllers\Controller::getProvinceName($miasto->id_province)), 'miasto' => $miasto->id_city, 'miasto_name' => str_slug($miasto->name),'filtr' => '0'])}}
        </loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    @endforeach --}}


    @foreach($getBlog as $blog)
    <url>
        <loc>{{route('blog_show',['id'=>$blog->id_blog])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    @endforeach

    {{-- @foreach($getCms as $cms)
    <url>
        <loc>{{route('cms_show',['id'=>$cms->id_cms, 'title'=>str_slug($cms->title)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    @endforeach --}}
</urlset>