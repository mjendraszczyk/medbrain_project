 <!-- Show logo -->
@if($o->logo_upload != '')
 <img src=\'{{asset('img/logo')}}/{{$o->logo_upload}}\' class='block m-auto' /> 
 @else 
 <img src='{{asset('img/logo-header.png')}}' class='block m-auto' /> 
@endif

 <!--Osoby -->
{{-- @if((Request::route()->getPrefix() == '/osoby'))
@if(count(App\Http\Controllers\Controller::getOsobyDependsMiasto($o->id_miasta)) > 0) <ul class='oferty-pracy lista'>
    @foreach (App\Http\Controllers\Controller::getOsobyDependsMiasto($o->id_miasta)
    as $o) <li class='row pointer'><a href=\"{{route('profil_zobacz',['id'=>$o->id_user])}}\"><span
                class='icon-specjalizacja' style='background-color:#{{$o->kolor}};'>{{$o->symbol}}</span>
            <div class='col s12 m12 l12'>
                <h6>{{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h6>
                <h3>{{$o->name}}
                    {{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h3>
            </div>
            <div class='col s12 m12 l12'>

                <span class="btn btn-default green" style="
                                            align-items: center;
                                            justify-content: center;
                                        ">Szukam pracy</span>

            </div>
        </a></li>@endforeach </ul> @endif

@else --}}
{{-- Ogloszenia --}}
{{-- W widoku specjalizacji --}}
@if((Route::currentRouteName() == 'ogloszenia_specjalizacja_index') || (Route::currentRouteName() == 'ogloszenia_zobacz'))
@if(count(App\Http\Controllers\Controller::getOgloszeniaDependsSpecjalizacja($o->id_specjalizacje, $o->id_podmiot)) > 0)
<ul class='oferty-pracy lista'>
    @foreach (App\Http\Controllers\Controller::getOgloszeniaDependsSpecjalizacja($o->id_specjalizacje, $o->id_podmiot)
    as $o) <li class='row pointer'><a
            href=\"{{route('ogloszenia_zobacz',['id'=>$o->id_ogloszenia,'nazwa'=>str_slug($o->nazwa)])}}\"><span
                class='icon-specjalizacja' style='background-color:#{{$o->kolor}};'>{{$o->symbol}}</span>
            <div class='col s12 m12 l12'>
                <h6>{{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h6>
                <h3>{{App\Http\Controllers\Controller::getPoziom($o->id_poziom)->nazwa}}
                    {{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h3>
                @if(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia))
                <div class='oferta-pracy_adres'>{{$o->nazwa}}, <span class="material-icons">
                        place
                    </span>{{App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->ulica}},
                    {{App\Http\Controllers\Controller::getMiasto(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->id_miasta)}}
                </div>
                @else
                <div class='oferta-pracy_adres'>{{$o->nazwa}}, <span class="material-icons">
                        place
                    </span> {{$o->kod_pocztowy}},{{$o->ulica}},
                    {{App\Http\Controllers\Controller::getMiasto($o->id_miasta)}}</div>
                @endif
            </div>
            <div class='col s12 m12 l12'>
                <div class='praca-wynagrodzenie'>
                    @if(($o->wynagrodzenie_od == '0') || ($o->wynagrodzenie_od == ''))
                    @else
                    {!!App\Http\Controllers\Controller::parseMoneyFormat($o->wynagrodzenie_od, $o->id_ogloszenia)!!}
                    @endif
                    @if($o->wynagrodzenie_do > $o->wynagrodzenie_od)
                    -
                    {!!App\Http\Controllers\Controller::parseMoneyFormat($o->wynagrodzenie_do, $o->id_ogloszenia)!!}
                    @endif
                    @if($o->wynagrodzenie_od > 0)
                    <small>
                        {{App\Http\Controllers\Controller::getTypWynagrodzenia($o->typ_wynagrodzenia)}}
                    </small>
                    @endif
                </div>
            </div>
        </a></li>@endforeach </ul> @endif
@else
{{-- Ogloszenia wg podmiotu --}}
@if(count(App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot)) > 0) 
<ul
    class='oferty-pracy lista'> 
    @foreach (App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot)
    as $o) 
    <li class='row pointer'><a
            href=\"{{route('ogloszenia_zobacz',['id'=>$o->id_ogloszenia,'nazwa'=>str_slug($o->nazwa)])}}\"><span
                class='icon-specjalizacja' style='background-color:#{{$o->kolor}};'>{{$o->symbol}}</span>
            <div class='col s12 m12 l12'>
                <h6>{{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h6>
                <h3>{{App\Http\Controllers\Controller::getPoziom($o->id_poziom)->nazwa}}
                    {{App\Http\Controllers\Controller::getSpecjalizacje($o->id_specjalizacje)}}</h3>
                    
                @if(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia))
                
                <div class='oferta-pracy_adres'>{{$o->nazwa}}, <span class="material-icons">
                        place
                    </span>
                    
                    {{App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->ulica}},
                    {{--{{App\Http\Controllers\Controller::getMiasto(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->id_miasta)}}
                </div>--}}
                @else

                <div class='oferta-pracy_adres'>{{$o->nazwa}}, <span class="material-icons">
                        place
                    </span> {{$o->kod_pocztowy}},{{$o->ulica}},
                    {{--
                    {{App\Http\Controllers\Controller::getMiasto($o->id_miasta)}}</div>
                    --}}
                @endif
            </div>
            
            <div class='col s12 m12 l12'>
                <div class='praca-wynagrodzenie'>
                    @if(($o->wynagrodzenie_od == '0') || ($o->wynagrodzenie_od == ''))
                    @else
                    {!!App\Http\Controllers\Controller::parseMoneyFormat($o->wynagrodzenie_od, $o->id_ogloszenia)!!}
                    @endif
                    @if($o->wynagrodzenie_do > $o->wynagrodzenie_od)
                    -
                    {!!App\Http\Controllers\Controller::parseMoneyFormat($o->wynagrodzenie_do, $o->id_ogloszenia)!!}
                    @endif
                    @if($o->wynagrodzenie_od > 0)
                    <small>
                        {{App\Http\Controllers\Controller::getTypWynagrodzenia($o->typ_wynagrodzenia)}}
                    </small>
                    @endif
                </div>
            </div>
        </a>
        
    </li>
        @endforeach 
    </ul> 
@endif
@endif
{{-- @endif --}}