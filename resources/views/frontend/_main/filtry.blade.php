<div class="row flex">
    @if(trim(Request::route()->getPrefix(),"/") == "osoby")
    <div class="col s12 m6 bar-green-left typy-ogloszen green" data-typy="lekarze">
        <h2>Szukaj pracownika <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>
    <div class="col s12 m6 bar-gray-right typy-ogloszen dgrey" data-typy="ogloszenia">
        <h2>Oferty pracy <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>
    @else
    <div class="col s12 m6 bar-green-left typy-ogloszen dgrey" data-typy="lekarze">
        <h2>Szukaj pracownika <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>
    <div class="col s12 m6 bar-gray-right typy-ogloszen dgrey green" data-typy="ogloszenia">
        <h2>Oferty pracy <i class="material-icons">
                arrow_drop_down
            </i></h2>
    </div>
    @endif
</div>