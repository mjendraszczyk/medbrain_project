﻿@if(count($showOgloszenia) > 0)
@foreach ($showOgloszenia as $ogloszenie)
<li class="row pointer" data-id="{{$ogloszenie->id_ogloszenia}}" style="border-left:5px solid #{{$ogloszenie->kolor}};">
    <a href="{{route('ogloszenia_zobacz',['id'=>$ogloszenie->id_ogloszenia, 'nazwa'=>str_slug($ogloszenie->nazwa)])}}">
        {{-- <span class="icon-specjalizacja"
            style="background-color:#{{$ogloszenie->kolor}};">{{$ogloszenie->symbol}}</span> --}}
        <div class="col s12 m12 l8">
            <h6 style="color:#{{$ogloszenie->kolor}};">
                {{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie->id_specjalizacje)}}

            </h6>
            <h3>
                @if($ogloszenie->id_lokalizacja != null &&
                \App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($ogloszenie->id_ogloszenia) != null)
                {{\App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($ogloszenie->id_ogloszenia)->alias}}
                @else
                {{$ogloszenie->nazwa}}
                @endif
            </h3>
            @if(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($ogloszenie->id_ogloszenia) != null)
            <div class="oferta-pracy_adres">{{$ogloszenie->nazwa}}, <span class="material-icons">
                    place
                </span>
                {{App\Http\Controllers\Controller::getMiasto(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($ogloszenie->id_ogloszenia)->id_miasta)}}
                {{App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($ogloszenie->id_ogloszenia)->ulica}}
            </div>
            @else
            <div class="oferta-pracy_adres">{{$ogloszenie->nazwa}}, <span class="material-icons">
                    place
                </span>
                {{$ogloszenie->kod_pocztowy}},
                {{App\Http\Controllers\Controller::getMiasto($ogloszenie->id_miasta)}}
                {{$ogloszenie->ulica}}
            </div>
            @endif
        </div>
        <div class="col s12 m12 l4">
            <div class="praca-wynagrodzenie">
                <i class="material-icons fixed_price">
                    tune
                </i>
                @if((($ogloszenie->wynagrodzenie_od) == 0) || ($ogloszenie->wynagrodzenie_od == ''))
                @else
                {!!
                App\Http\Controllers\Controller::parseMoneyFormat($ogloszenie->wynagrodzenie_od,
                $ogloszenie->id_ogloszenia)
                !!}
                @endif
                <br>
                @if(($ogloszenie->wynagrodzenie_do) >
                ($ogloszenie->wynagrodzenie_od))
                {!!
                App\Http\Controllers\Controller::parseMoneyFormat($ogloszenie->wynagrodzenie_do,
                $ogloszenie->id_ogloszenia)
                !!}
                @endif
                @if($ogloszenie->wynagrodzenie_od > 0)
                <h6 style="text-transform: none">
                    {{App\Http\Controllers\Controller::getTypWynagrodzenia($ogloszenie->typ_wynagrodzenia, $ogloszenie->id_ogloszenia)}}
                </h6>
                @endif
            </div>
        </div>
    </a>
</li>
@endforeach
@else
{{-- <li class="row pointer alert alert-danger">
    Brak ogłoszeń
</li> --}}
@endif