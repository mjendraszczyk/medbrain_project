@include('frontend/_main/head')

<body>
  <nav class="white lighten-1" role="navigation">
    <div class="nav-wrapper"><a id="logo-container" href="{{route('home')}}" class="brand-logo"><img
          src="{{asset('img/logo-header.png')}}" /></a>
      @include('frontend/_main/nav')
      <ul id="nav-mobile" class="sidenav">
        <div class="center slideNavContent">
          <a href="{{route('home')}}"><img src="{{asset('img/logo-header.png')}}" /></a>
          @if(Auth::guest())
          <li><a href="{{route('konto')}}" class="gray-text">Zaloguj się</a></li>
          @endif
          @include('frontend/_main/menu')
        </div>
      </ul>

    </div>
  </nav>