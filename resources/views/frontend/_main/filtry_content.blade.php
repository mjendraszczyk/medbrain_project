<div class="row white">
    <div class="container">
<h5 class="green-title">Specjalizacje</h5>
    <div class="row panel white filtry collapse" id="collapseExample" style="height: 200px; overflow: hidden;">
        {{-- @if(trim(Request::route()->getPrefix(),"/") != "osoby") --}}
        {{-- @if(trim(Request::route()->getPrefix(),"/") != "osoby")
        <div class="price_filter_slider">
            @else
            <div class="price_filter_slider hidden">
                @endif
                <h5>Wynagrodzenie</h5>
                <div data-role="main" class="ui-content" style="height:auto;">
                    <form method="post" action="{{route('getOgloszeniaFilter')}}">
                        @csrf
                        <div class="row">
                            <div class="col s12 m10 l11">
                                <input type="text" name="wynagrodzenie_od" id="input-with-keypress-0"
                                    class="col s12 m2 l1">
                                <div id="steps-slider"
                                    class="noUi-target noUi-ltr noUi-horizontal noUi-txt-dir-ltr col s12 m8 l10">
                                </div>
                                <input type="text" name="wynagrodzenie_do" id="input-with-keypress-1"
                                    class="col s12 m2 l1">
                            </div>
                            <div class="col s12 m2 l1" style="text-align:right;">
                                <input type="submit" class="btn btn-primary green" data-inline="true" value="Filtruj">
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            {{-- @endif --}}
            <div class="col s12 m12 l12">
                {{-- Lista ofert pracy wg specjalizacji --}}
                @if(trim(Request::route()->getPrefix(),"/") == "osoby")
                <div class="ogloszenia_specjaliacje hidden" data-typy="ogloszenia">
                    @else
                    <div class="ogloszenia_specjaliacje" data-typy="ogloszenia">
                        @endif
                        <ul class="inline-list row">
                            @foreach($specjalizacje as $specjalizacja)
                            <li
                                class="col s12 m6 l4 xl2 @if(Route::currentRouteName() == 'ogloszenia_specjalizacja_index') @if($specjalizacja->id_specjalizacje == $id_specjalizacje) active @endif @endif">
                                <a
                                    href="{{route('ogloszenia_specjalizacja_index',['id_specjalizacje'=>$specjalizacja->id_specjalizacje,'name'=>str_slug($specjalizacja->nazwa)])}}">
                                    <span class="icon-specjalizacja"
                                        style="background:#{{$specjalizacja->kolor}};">{{$specjalizacja->symbol}}</span>
                                    {{$specjalizacja->nazwa}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                     
{{-- <span class="btn btn-primary more green"><i class="material-icons" style="vertical-align: middle;">
        arrow_drop_down
    </i>więcej</span> --}}
                    {{-- Lista pracowników wg specjalizacji --}}
                    {{-- @if(trim(Request::route()->getPrefix(),"/") == "osoby")
                    <div class="lekarze_specjalizacje" data-typy="lekarze">
                        @else
                        <div class="lekarze_specjalizacje hidden" data-typy="lekarze">
                            @endif
                            <ul class="inline-list row">
                                @foreach($specjalizacje as $specjalizacja)
                                <li
                                    class="col s12 m6 l4 xl2 @if(Route::currentRouteName() == 'pracownicy_specjalizacja_index') @if($specjalizacja->id_specjalizacje == $id_specjalizacje) active @endif @endif">
                                    <a
                                        href="{{route('pracownicy_specjalizacja_index',['id_specjalizacje'=>$specjalizacja->id_specjalizacje])}}">
                                        <span class="icon-specjalizacja"
                                            style="background:#{{$specjalizacja->kolor}};">{{$specjalizacja->symbol}}</span>
                                        {{$specjalizacja->nazwa}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div> --}}
                  

                    {{-- Lista ofert pracy wg województw --}}
                    {{-- <div class="col s12 m12 l3 collapse">
                        <h5>Województwa</h5>
                      
                        @if(trim(Request::route()->getPrefix(),"/") == "osoby")
                        <div class="ogloszenia_wojewodztwa hidden" data-typy="ogloszenia">
                            @else
                            <div class="ogloszenia_wojewodztwa" data-typy="ogloszenia">
                                @endif
                                <ul class="inline-list">
                                    <li class="gray white-text center radius-15 indent-0">
                                        <a href="{{route('home')}}">Wszystkie</a>
                                    </li>
                                    @foreach($wojewodztwa as $wojewodztwo)
                                    <li class="gray white-text center radius-15 indent-0"><a
                                            href="{{route('ogloszenia_wojewodztwa_index',['id_wojewodztwa'=>$wojewodztwo->id_wojewodztwa])}}">{{$wojewodztwo->nazwa}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div> --}}
                            {{-- Lista pracowników wg województw --}}
                            {{-- @if(trim(Request::route()->getPrefix(),"/") == "osoby")
                            <div class="lekarze_wojewodztwa" data-typy="lekarze">
                                @else
                                <div class="lekarze_wojewodztwa hidden" data-typy="lekarze">
                                    @endif
                                    <ul class="inline-list">
                                        <li class="gray white-text center radius-15 indent-0">
                                            <a
                                                href="{{route('pracownicy_wojewodztwa_index',['id_wojewodztwa'=>'wszystkie'])}}">Wszystkie</a>
                                        </li>
                                        @foreach($wojewodztwa as $wojewodztwo)
                                        <li class="gray white-text center radius-15 indent-0"><a
                                                href="{{route('pracownicy_wojewodztwa_index',['id_wojewodztwa'=>$wojewodztwo->id_wojewodztwa])}}">{{$wojewodztwo->nazwa}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div> --}}
                            {{-- <span class="btn btn-primary more green"><i class="material-icons"
                                    style="vertical-align: middle;">
                                    arrow_drop_down
                                </i>więcej</span>
                            <span class="btn btn-primary more-single green"><i class="material-icons"
                                    style="vertical-align: middle;">
                                    arrow_drop_down
                                </i>więcej</span> --}}
                        </div>
                    </div>
                     <span class="btn btn-primary more" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    <i class="material-icons" style="vertical-align: middle;">
    arrow_drop_down
    </i>więcej
  </span>
  </div>