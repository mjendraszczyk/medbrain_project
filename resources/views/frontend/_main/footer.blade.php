<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l3 s12">
        <a href="{{route('home')}}"><img src="{{asset('img/logo-header.png')}}" /></a>
      </div>
      <div class="col l3 s12">
        <h4>{{(App\Http\Controllers\Controller::getUstawienia()['firma'])}} </h4>
        <div class="footer-address">
          {{(App\Http\Controllers\Controller::getUstawienia()['adres'])}} <br />
        </div>
        <div class="footer-contact">
          <i class="material-icons">phone</i>
          tel. {{(App\Http\Controllers\Controller::getUstawienia()['telefon'])}} <br />
          <span class="green-text">
            <i class="material-icons">email</i>
            email:
            {{(App\Http\Controllers\Controller::getUstawienia()['email'])}}
          </span>
        </div>
      </div>
      <div class="col l3 s12">
        <h4>Portal</h4>
        <ul class="footer-list-item">
          @include('frontend/_main/menu')
        </ul>

      </div>
      <div class="col l3 s12">
        <h4>Praca</h4>
        <ul class="footer-list-item">
          @include('frontend/_main/menu_praca')
        </ul>
      </div>
      <div class="copy center">
        &copy; {{date('Y')}} {{(App\Http\Controllers\Controller::getUstawienia()['nazwa'])}} Wszelkie Prawa
        Zastrzeżone
      </div>
    </div>
  </div>

</footer>


<!--  Scripts-->

{{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}

<script src="https://code.jquery.com/jquery-2.1.1.min.js?v=2"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js?v=2"></script>

<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js?v=2"></script>

<!-- Tinymce init -->
<script type="text/javascript">
  var editor_config = {
  selector: '.editor',
  height: 300,
   branding: false,
  menubar: true,
   menu: {
     edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
    insert: {title: 'Insert', items: 'link media | template hr'},
    view: {title: 'View', items: 'visualaid'},
    format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
    // table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
    tools: {title: 'Tools', items: 'spellchecker code'}
  },
  font_formats: 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace;Georgia=georgia,sans; Lato=Lato,sans;Verdana=verdana,sans; AkrutiKndPadmini=Akpdmi-n',
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'
  ],
  fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px 60px 72px',
  toolbar: 'spellchecker code | insert | undo redo | fontsizeselect | formatselect | bold italic backcolor forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Roboto:400,700',
    '//www.tinymce.com/css/codepen.min.css'],
relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      // var cmsURL = '{{URL::to("/")}}/laravel-filemanager?field_name=' + field_name;
      // if (type == 'image') {
      //   cmsURL = cmsURL + "&type=Images";
      // } else {
      //   cmsURL = cmsURL + "&type=Files";
      // }

      // tinyMCE.activeEditor.windowManager.open({
      //   file : cmsURL,
      //   title : 'Filemanager',
      //   width : x * 0.8,
      //   height : y * 0.8,
      //   resizable : "yes",
      //   close_previous : "no"
      // });
    }
};
tinymce.init(editor_config);
</script>

<script src="{{asset('js/materialize.js')}}?v=2"></script>

<script src="{{asset('js/init.js')}}?v=2"></script>


<script type="text/javascript">
  /**
 * Ustaw style z ui google dla selectów
 */
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
  });

  /**
  * Ustaw wyszukiwarkę w selectach
  */
  $(document).ready(function(){
    $('select').select2({
      width: "100%",
      });

      $('select[name=id_miasta], select[name=miasto]').select2({
      width: "100%",
     minimumInputLength: 2,
     page: 20
    });
    M.updateTextFields();
  });

  /* Przy zmianie wojewodztwa pobierz miasta z województwa*/
$("select[name=id_wojewodztwa]").change(function() {
  // alert("G");
  

  if($(this).hasClass("zmien_miasta")) {
    // for(var dz=0;dz>dat) {
      //location.protocol+
      $.get(window.location.origin+'/api/wojewodztwo/'+$(this).val()+'/miasta/', function(data){
        console.log(data);
        // var json = JSON.parse(data);
        console.log(data[0].id_miasta);
        
        $("select[name=id_miasta], select[name=miasto]").each(function() {
        $(this).html('');
        for(var di=0;di<data.length;di++) { $(this).append("<option value="+data[di].id_miasta+">"+data[di].nazwa+"</option>");
          }
          });
          });
    // }
  }
});

  
   document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
  });
  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
</script>

<script>
  /**
   * Pobierz LatLon z podanego adresu 
   */ 

  function getLanLon() {
  console.log("Test");
  console.log("ULICA "+$("input[name=ulica]").val());
  console.log("BLABLA");
  //$("input[name=ulica]").val()+" "+
  if($("input[name=ulica]").val() != '') {
    var address_full = $("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text()+" "+$("input[name=ulica]").val();
  } else {
  var address_full = $("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text();
  }

  var address_min = $("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text();
  console.log("ADRES: "+address_full);

  $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+address_full, function(data){
    //console.log("FF");
       console.log(data);
       M.updateTextFields();
      
     // alert("A");
       if(data.length == 0) {
        // alert("B");
         console.log("AAA");
         var address = $("select[name=miasto] :selected").text()+" "+$("select[name=id_miasta] :selected").text()+" "+$("input[name=miasto]").val();
         $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+address_min, function(data){
            $("#lat").val(data[0].lat);
            $("#lon").val(data[0].lon);
         });
       } else {
         //alert("C");
         console.log("BBB");
         console.log(data[0].lat);
         console.log(data[0].lon);
      $("#lat").val(data[0].lat);
      $("#lon").val(data[0].lon);
       }

      $("#mapLatLon .leaflet-marker-icon").remove();
$("#mapLatLon .leaflet-popup").remove();
$("#mapLatLon .leaflet-shadow-pane").remove();
var marker = L.marker([$("#lat").val(), $("#lon").val()]).addTo(mymap2);
      //}
      //var mymap2 = L.map('mapLatLon').setView([52.3009024,19.7999369], 6);
      //var marker = L.marker([data[0].lat, data[0].lon]).addTo(mymap2);
});

}
</script>
@if(
(Route::currentRouteName() =='ogloszenia_dodaj_krok4') ||
(Route::currentRouteName() =='krok4Request') ||
(Route::currentRouteName() =='profil_lokalizacje_edit') ||
(Route::currentRouteName() =='profil_lokalizacje_create') ||
(Route::currentRouteName() =='backend_ogloszenia_edit'))
<script>

 
   
  var mymap2 = L.map('mapLatLon').setView([52.3009024,19.7999369], 6);
    
    var gl = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',{
    // accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg',
    attribution: '',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    minZoom: 6,
    maxZoom: 18,
    scrollWheelZoom: false,
    accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg'
    }).addTo(mymap2);
    
    
    var dx=0;
    var marker;
    
    if(($("#lat").val() != '') && ($("#lon").val() != '')) {
    marker = L.marker([$("#lat").val(), $("#lon").val()]).addTo(mymap2);
    } else { 
      marker = L.marker([52.3009024, 19.7999369]).addTo(mymap2);
    }
    
    
    mymap2.on('click', function(e) {
    //if(dx > 0) {
 
 //alert("F");
    
    $(".leaflet-marker-icon").remove();
    $(".leaflet-popup").remove();
    $(".leaflet-shadow-pane").remove();
    //}
    
    marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap2);
    $("#lat").val(e.latlng.lat);
    $("#lon").val(e.latlng.lng);
    
    dx++;
    });

    // $("input[name=ulica],input[name=miasto], select[name=miasto],select[name=id_wojewodztwa],select[name=id_miasta]").change(function() {
      
    //   $(".leaflet-marker-icon").remove();
    //   $(".leaflet-popup").remove();
    //   $(".leaflet-shadow-pane").remove();
    //   //}
      
    //   marker = L.marker([$("#lat").val(), $("#lon").val()]).addTo(mymap2);

    // });
</script>
@endif
@if((Route::currentRouteName() == 'home') || (Route::currentRouteName() == 'ogloszenia_zobacz') ||
(Route::currentRouteName() == 'profil_zobacz') || (Route::currentRouteName() == 'ogloszenia_specjalizacja_index') ||
(Route::currentRouteName() == 'ogloszenia_wojewodztwa_index') || (Route::currentRouteName() ==
'ogloszenia_wynagrodzenie_index') || Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index' || (Route::currentRouteName() ==
'pracownicy_wojewodztwa_index') || (Route::currentRouteName() =='pracownicy_specjalizacja_index'))

  {{-- 
  ############## 
  Początek markerów dla mapy 
  ##############
  --}}

<script>
  /**
   * Pobierz i wyrenderuj mapę 
   */
  function getMap() {
    @if(Route::currentRouteName() == 'ogloszenia_zobacz')
      @foreach($ogloszenie as $key => $o)
        @if($key == 0)
          @if($lokalizacja != null)
            var mymap = L.map('map').setView([{{$lokalizacja->lat}},{{$lokalizacja->lon}}], 16);
          @else
            var mymap = L.map('map').setView([{{$o->lat}},{{$o->lon}}], 16);
          @endif
        @endif
      @endforeach
      @elseif(Route::currentRouteName() == 'ogloszenia_wojewodztwa_index')
        @foreach($ogloszenia as $key => $o)
          @if($key == 0)
            var mymap = L.map('map').setView([{{$o->lat}},{{$o->lon}}], 9);
          @endif
        @endforeach
        @if(count($ogloszenia) == 0) 
            var mymap = L.map('map').setView([52.3009024,19.7999369], 6);
            @endif
      @else
        var mymap = L.map('map').setView([52.3009024,19.7999369], 6);
      @endif

  var gl = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',{
  attribution: '',
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  minZoom: 6,
  maxZoom: 18,
  scrollWheelZoom: false,
  accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg'
  }).addTo(mymap);
 

  @foreach($ogloszenia as $o)
    var icon = L.divIcon({
    iconSize:null,
    html:`<div class="btn-floating pulse icon-specjalizacja" style="background:#{{$o->kolor}};">{{$o->symbol}} @if(count(App\Http\Controllers\Controller::getOgloszeniaDependsPodmiot($o->id_podmiot)) > 0) 
    @if((Route::currentRouteName() == 'ogloszenia_specjalizacja_index') || (Route::currentRouteName() == 'ogloszenia_zobacz'))
    <sup class="counterMap">{{count(App\Http\Controllers\Controller::getOgloszeniaDependsSpecjalizacja($o->id_specjalizacje ,$o->id_podmiot))}}</sup>
    @elseif(Route::currentRouteName() == 'pracownicy_specjalizacja_index')
    <sup class="counterMap">{{count(App\Http\Controllers\Controller::getOsobyDependsSpecjalizacja($o->id_specjalizacje))}}</sup>
    @elseif((Route::currentRouteName() == 'profil_zobacz') ||(Route::currentRouteName() == 'pracownicy_wojewodztwa_index'))
    <sup class="counterMap">{{count(App\Http\Controllers\Controller::getOsobyDependsMiasto($o->id_miasta))}}</sup>
    @else
    <sup class="counterMap">{{count(App\Http\Controllers\Controller::getOgloszeniaDependsMiasto($o->id_miasta))}}</sup> 
    @endif
    @endif</div>`});
  
 //alert("ustawiam punkt z dodatkowej lokalizacji")
  @if(App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia))
      var marker = L.marker([{{App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->lat}}, {{App\Http\Controllers\Controller::checkLokalizacjaFromOgloszenie($o->id_ogloszenia)->lon}}],{icon: icon}).addTo(mymap);
  @else
      var marker = L.marker([{{$o->lat}}, {{$o->lon}}],{icon: icon}).addTo(mymap);
  @endif
  
    marker.bindPopup(`@include('frontend._main.footer_marker_bind')`);
    @if(Route::currentRouteName() == 'ogloszenia_zobacz')
      @foreach($ogloszenie as $item)
        @if($item->id_ogloszenia == $o->id_ogloszenia)
          marker.openPopup();
        @endif
      @endforeach
    @endif
  @endforeach
  
// init Infinite Scroll
var $container = $('.lista').infiniteScroll({
path: 'a.page-link[rel=next]',
append: '.row.pointer',
status: '.page-load-status',
hideNav: '.pagination',
button: '.view-more-button',
scrollThreshold: false,
});

$container.on( 'append.infiniteScroll', function( event, response, path, items ) {
console.log( items.length + ' items appended' );
console.log(items);
for(var dx=0;dx<items.length;dx++) {
  $.get(window.location.origin + '/api/ogloszenie/'+items[dx].getAttribute('data-id'), function(data){
// console.log(data['id_ogloszenia']);
console.log(data.id_ogloszenia);

var podmiot = data.id_podmiot;

function getLiczbaOgloszen(podmiot) {
  if(typeof podmiot === undefined) {
    $('.counterMap').html('');
  } else { 
  $.get(window.location.origin + '/api/count/ogloszenia/podmiot/'+podmiot, function(liczba){
  console.log("PP");
  console.log(data);
  console.log(liczba.length);
  
$('.counterMap.item'+podmiot).html(liczba.length);
  
});
  }
}

var icon = L.divIcon({
    iconSize:null,
    html:`<div class="btn-floating pulse icon-specjalizacja" style="background:#`+data. kolor+`;">`+data.symbol+`
      <sup class="counterMap item`+data.id_podmiot+`">`
      +setTimeout(function() { getLiczbaOgloszen(podmiot); }, 100)+`
      </sup>
    </div>`});
console.log("==TEST==");
    console.log(data);
var marker = L.marker([data.lat, data.lon],{icon:
icon}).addTo(mymap);


var contentBindPopup = '';
if(data.logo_upload != null) {
contentBindPopup += `<img src=\'{{asset('img/logo')}}/`+data.logo_upload+`\' class='block m-auto' />`
} else {
contentBindPopup += `<img src='{{asset('img/logo-header.png')}}' class='block m-auto' />`
}

contentBindPopup += `
 <ul
    class='oferty-pracy lista'>`;

  $.get(window.location.origin + '/api/ogloszenia/podmiot/'+data.id_podmiot, function(response){
    console.log("ttt");
    console.log(response);
    for(var di=0;di<response.length;di++) {
      contentBindPopup += `<li class='row pointer'><a href=\"ogloszenie/`+response[di].id_ogloszenia+`/`+response[di].nazwa.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'')+`\"><span
                class='icon-specjalizacja' style='background-color:#`+response[di].kolor+`;'>`+response[di].symbol+`</span>
                
            <div class='col s12 m12 l12'>
                <h6>
                `+response[di].specjalizacja_nazwa+`</h6>
                <h3>
                  `+response[di].poziom_nazwa+` `+response[di].specjalizacja_nazwa+`</h3>
                    <div class='oferta-pracy_adres'>`+response[di].nazwa+`, <span class="material-icons">
                          place
                        </span> `+response[di].kod_pocztowy+`,`+response[di].ulica+`,
                        `+response[di].miasto_nazwa+`
                         </div>
                         <div class=''>
                          <div class='praca-wynagrodzenie'>`;
                          if(response[di].wynagrodzenie_od != null) {
                            contentBindPopup += response[di].wynagrodzenie_od+` `+response[di].waluta_wynagrodzenia;
                          }
                          if(response[di].wynagrodzenie_do != null) {
                          contentBindPopup += ` - `+response[di].wynagrodzenie_do+` `+response[di].waluta_wynagrodzenia;
                          }

                          if(response[di].wynagrodzenie_od > 0) {
                          contentBindPopup += `<small>`;
                             if(response[di].typ_wynagrodzenia == 1) {
                                contentBindPopup += ` za miesiąc`;
                             } else {
                                contentBindPopup += ` roboczogodzina`;
                             }
                          contentBindPopup += `</small>`;
                          }
                          contentBindPopup += `
                          </div>
                          </div>
                    </li>`;
    }
   contentBindPopup += '</ul>';


marker.bindPopup(contentBindPopup);
});
  });
}
});
// end getMap()
  }

  $(document).ready(function(){
    getMap();
});
</script>
@endif

{{-- 
  ############## 
  Koniec markerów dla mapy 
  ##############
  --}}
<script>
  /**
   * Więcej / mniej specjalizacji - województw 
   */

//button
  $("input[name=ulica],input[name=miasto], select[name=miasto],select[name=id_wojewodztwa],select[name=id_miasta]").change(function(e) { 
    getLanLon();
    console.log("zmiana markera");
    }).ready(function(e) {
      if(($('input[name=ulica]').length > 0)
      // || ($('input[name=miasto]').length > 0)
      // || ($('select[name=miasto]').length > 0)
      // || ($('select[name=id_wojewodztwa]').length > 0)
      // || ($('select[name=id_miasta]').length > 0)
      )
       {
        getLanLon();
      }
  }).blur(function(e) {
    getLanLon();
  }).hover(function(e) {
  });
  var j=0;
  $(".more-single").click(function(){
    if (j%2 == 0) {
      $(this).parent().children('.collapse').css('height','auto');
      $(this).parent().children('.collapse').css('overflow','visible');
    } else {
      $(this).parent().children('.collapse').css('height','200px');
      $(this).parent().children('.collapse').css('overflow','hidden');
    }
    j++;
  });
  var i = 0;
  $(".more").click(function(){
    if (i%2 == 0) {
      $(this).prev('.collapse').css('height','auto');
      $(this).prev('.collapse').css('overflow','visible');
      $(this).html("<i class='material-icons'>arrow_drop_up</i>Mniej");
    } else {
      $(this).prev('.collapse').css('height','200px');
      $(this).prev('.collapse').css('overflow','hidden');
      $(this).html("<i class='material-icons'>arrow_drop_down</i>Więcej");
    }
    i++;
    });
/**
 * Ogłoszenia pracownika / oferty pracy przełączanie
 */

  $(".typy-ogloszen").click(function() {
  var getTyp = $(this).attr('data-typy');
  $('.typy-ogloszen').removeClass('green');
  $('.typy-ogloszen').addClass('dgrey');

    $(this).removeClass('dgray');
    $(this).addClass('green');
  if(getTyp == 'lekarze') {
    $('.filtry [data-typy="ogloszenia"]').addClass('hidden');
    $('.filtry [data-typy="lekarze"]').removeClass('hidden');
    $(".price_filter_slider").css('display','none');
  } else {
    $('.filtry [data-typy="ogloszenia"]').removeClass('hidden');
    $('.filtry [data-typy="lekarze"]').addClass('hidden');
    $(".price_filter_slider").css('display','block');
  }
})
/**
 * Potwierdzenie akcji 
 */
$(document).ready(function() {
$('.js-alert').click(function() {
  var confirm_button = confirm("Czy napewno chcesz to zrobic?");
if(confirm_button == true) {
  //$('#usun_konto').submit();
  //document.usun_konto.submit()
  document.getElementById("usun_konto").submit();
} else {
  return false;
}
});
});
/**
 * Przełączanie faq
 */
  $(".faq-section .list li").click(function(){
$(this).children().children('.content_faq_box').toggle();
  });
/**
 * Dodawanie nowych pozycji
 */

  @if(Route::currentRouteName() == 'profil_profil-lekarski')
 $(document).on('click','.add_row',function(){
  var getContent = $(this).next().children('.content').html();
if($(this).hasClass('edukacja')) {
  var extra_content = '<div class="col s12"><select required name="id_tytul[]"><option value="" disabled selected>Tytuł</option>@foreach (App\Http\Controllers\Controller::getTytuly(null) as $tytul) <option value="{{$tytul->id_tytuly}}" @if($tytul->id_tytuly == $edukacja->id_tytul) selected="selected" @endif>{{$tytul->nazwa}}</option> @endforeach </select></div>';
} else {
  var extra_content = '';
}

  $(this).parent().append('<div class="row edukacja_block adding_row">'+extra_content+'<div class="col m10">'+getContent+'</div><div class="col m2"><span class="remove_row btn btn-default">Usuń</span></div></div>');
 

  console.log("g"+getContent);
  
  
  $('select').select2({
  width: "100%",
  });

  $('select[name=id_miasta]').select2({
  width: "100%",
  minimumInputLength: 3
  });
  M.updateTextFields();
})
@endif
/**
 * Usuwanie pozycji
 */ 
$(document).on('click','.remove_row',function(){
  //alert('gdgdfg');
  $(this).parent().parent().remove();
});

/**
 * Dodatkowe adresy
 */
// $(".dodaj_lokalizacje").click(function() {
//   $(this).next('.dodatkowe_lokalizacje').append(`
//   <div>
//     <label>Adres</label>
//     <input @if(Route::currentRouteName()=='profil_ogloszenia_edit' ) value="" @else value="" @endif type="text"
//       class="form-control-input">
//     </div>
//   `);
// });

/**
 * Scroll to div
 */
$(".scrollto").click(function(e) {
  e.preventDefault();
  $('.praca-section > div').animate({ scrollTop: $($(this).attr('href')).offset().top }, 1000);
});



// var mymap = L.map('map').setView([52.3009024,19.7999369], 6);

// var gl = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',{
// // accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg',
// attribution: '',
// maxZoom: 18,
// id: 'mapbox/streets-v11',
// tileSize: 512,
// zoomOffset: -1,
// accessToken: 'pk.eyJ1IjoibWplbmRyYXN6Y3p5ayIsImEiOiJjazMzYjF5OXAwb2YzM2JwOW1xcHRmenAwIn0.xJ_cIRov3PlsgkxVKLryBg'
// }).addTo(mymap);



</script>

<script src="{{asset('js/nouislider.min.js')}}?v=2"></script>
<script src="{{asset('js/wNumb.min.js')}}?v=2"></script>
{{-- 
<script>
  var stepsSlider = document.getElementById('steps-slider');
var input0 = document.getElementById('input-with-keypress-0');
var input1 = document.getElementById('input-with-keypress-1');
var inputs = [input0, input1];

noUiSlider.create(stepsSlider, {
start: [{{Session::get('filter_wynagrodzenie_od')}}, {{Session::get('filter_wynagrodzenie_do')}}],
connect: true,
tooltips: [false, wNumb({decimals: 1})],
range: {
'min': [0],
'max': {{Session::get('filter_wynagrodzenie_max')}}
}
});

stepsSlider.noUiSlider.on('update', function (values, handle) {
inputs[handle].value = values[handle];
});
 
inputs.forEach(function (input, handle) {

input.addEventListener('change', function () {
stepsSlider.noUiSlider.setHandle(handle, this.value);
});

input.addEventListener('keydown', function (e) {

var values = stepsSlider.noUiSlider.get();
var value = Number(values[handle]);

// [[handle0_down, handle0_up], [handle1_down, handle1_up]]
var steps = stepsSlider.noUiSlider.steps();

// [down, up]
var step = steps[handle];

var position;

// 13 is enter,
// 38 is key up,
// 40 is key down.
switch (e.which) {

case 13:
stepsSlider.noUiSlider.setHandle(handle, this.value);
break;

case 38:

// Get step to go increase slider value (up)
position = step[1];

// false = no step is set
if (position === false) {
position = 1;
}

// null = edge of slider
if (position !== null) {
stepsSlider.noUiSlider.setHandle(handle, value + position);
}

break;

case 40:

position = step[0];

if (position === false) {
position = 1;
}

if (position !== null) {
stepsSlider.noUiSlider.setHandle(handle, value - position);
}

break;
}
});
});




</script> --}}
</body>

</html>