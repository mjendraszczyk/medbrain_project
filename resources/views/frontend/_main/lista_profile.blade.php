@if(count($ogloszenia) > 0)
@foreach ($ogloszenia as $ogloszenie)
<li class="row pointer" style="border-left:5px solid #{{$ogloszenie->kolor}};">
    <a href="{{route('profil_zobacz',['id'=>$ogloszenie->id_profil])}}">
        {{-- <span class="icon-specjalizacja"
            style="background-color:#{{$ogloszenie->kolor}};">{{$ogloszenie->symbol}}</span> --}}
        <div class="col s12 m12 l8">
            <h6 style="color:#{{$ogloszenie->kolor}};">
                {{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie->id_specjalizacje)}}

            </h6>
            <h3>
                {{$ogloszenie->name}}
            </h3>
            <div class="oferta-pracy_adres">{{$ogloszenie->imie_nazwisko}}
                {{(App\Http\Controllers\Controller::getWojewodztwoByMiasto($ogloszenie->id_miasta))}},
                {{App\Http\Controllers\Controller::getMiasto($ogloszenie->id_miasta)}}
            </div>
        </div>
        <div class="col s12 m12 l4">
            <span class="btn btn-default green" style="
                        align-items: center;
                        justify-content: center;
                    ">Szukam pracy</span>
        </div>
    </a>
</li>
@endforeach
@else
<li class="row pointer alert alert-danger">
    Brak ogłoszeń
</li>
@endif