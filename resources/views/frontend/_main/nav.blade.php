<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
<ul class="right hide-on-med-and-down">
    <li><a href="{{route('oglosznia_dodaj_krok1')}}">
            <i class="material-icons">
                add
            </i>
            Dodaj ogłoszenie</a></li>
    @if (Auth::guest())
    <li><a href="{{route('login')}}" class="gray-text">
            <i class="material-icons">
                person
            </i>
            Zaloguj się</a></li>
    {{-- <li><a href="{{route('konto')}}">
    <i class="material-icons">
        person_add
    </i>
    Zarejestruj się</a></li> --}}
    @else
    <li><a href="{{route('profil_dane')}}">
            <i class="material-icons">
                person
            </i>
            Profil</a></li>
    @if(Auth::user()->id_rola == '3')
    <li><a href="{{route('backend_index')}}">
            <i class="material-icons">
                dashboard
            </i>
            Admin</a></li>
    @endif
    @endif
</ul>