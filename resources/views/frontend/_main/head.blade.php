<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  @if(Route::currentRouteName() == 'ogloszenia_zobacz')
  <title>
     {{App\Http\Controllers\Controller::getPoziom($ogloszenie[0]->id_poziom)->nazwa}}
    -
     {{App\Http\Controllers\Controller::getSpecjalizacje($ogloszenie[0]->id_specjalizacje)}} - 
     {{$ogloszenie[0]->nazwa}} - {{App\Http\Controllers\Controller::getMiasto($ogloszenie[0]->id_miasta)}}
      - {{ config('app.name') }}
  </title>
  @elseif(Route::currentRouteName() == 'ogloszenia_specjalizacja_index')
  <title>
  Oferty pracy -
      {{App\Http\Controllers\Controller::getSpecjalizacje(Request::route('id_specjalizacje'))}}
      - {{ config('app.name') }}
  </title>
  @elseif(Route::currentRouteName() == 'ogloszenia_specjalizacja_wojewodztwa_index')
  <title>
Oferty pracy -
{{strtolower(App\Http\Controllers\Controller::getWojewodztwo(Request::route('id_wojewodztwa')))}}
- {{ config('app.name') }}
  </title>
  @else
  <title>
    {{ config('app.name') }} - portal pracy branży medycznej
  </title>
  @endif
  <meta name="description" content="Portal pracy branży medycznej. Aktualne ogłoszenia pracy dla zawodów medycznych i nie tylko. Dodaj darmowe ogłoszenie."/>
  <!-- CSS  -->

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{asset('css/nouislider.min.css')}}?v=2" type="text/css" rel="stylesheet" />
  <link href="{{asset('css/materialize.css')}}?v=2" type="text/css" rel="stylesheet" />
  <link href="{{asset('css/style.css')}}?v=2" type="text/css" rel="stylesheet" />
  <link href="{{asset('css/medbrain.css')}}?v=2" type="text/css" rel="stylesheet" />

  <!-- Leaflet -->
  {{-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" /> --}}
  {{-- <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script> --}}
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css?v=2"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin="" />
    
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}?v=2"/>
    <script defer src="{{asset('js/leaflet.js')}}?v=2"></script>
  {{-- <script async defer src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js?v=2"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script> --}}
  <!-- Mapbox GL -->
  {{-- <link
    href="https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css?key=pk.eyJ1IjoibWFnZXMiLCJhIjoiY2s5Mmk2OThpMDRrNjNob3Ryb2xsNWtuaCJ9.88b1zbK2Cy3xe-Lg8AEYmA"
    rel='stylesheet' />
  <script
    src="https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js?key=pk.eyJ1IjoibWFnZXMiLCJhIjoiY2s5Mmk2OThpMDRrNjNob3Ryb2xsNWtuaCJ9.88b1zbK2Cy3xe-Lg8AEYmA">
  </script> --}}
  {{-- <script src="{{asset('/js/mapbox-gl.js')}}">
  </script> --}}
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css?v=2" rel="stylesheet" />
  <script src="{{asset('js/tinymce/tinymce.min.js')}}?v=2"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async defer src="https://www.googletagmanager.com/gtag/js?id=UA-139233983-21"></script>
  <script async defer>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
  
    gtag('config', 'UA-139233983-21');
  </script>

  <!-- Facebook Pixel Code -->
  <script async defer>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '241524937075732');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=241524937075732&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->
</head>
{{-- {{dd($ogloszenie)}} --}}